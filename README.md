# Diandao

Diandao is a platform game made with unity by 6 students for the class of "Video game design and programming".

## Getting Started

Install Unity and open this folder from Unity. The version used for the last release was 2017.2.0f3.

The name of the features are the ones in the trello taskboard.

### Prerequisites

The installer of unity is on the site of Unity3D engine (unity3d.com).

The task are in the following taskboard: https://trello.com/b/tnaDFJnJ/taskboard


## Running the tests

If you play and there exceptions in the console, if you can't finish a walkthrough, 

you have a bug. 


### Coding style tests

The conventions about the coding style are inside the GDD in the "pipeline overview" paragraph.

Here the link of the GDD:

https://docs.google.com/document/d/1jA7GUcsl3rbqKKq-v9u8RAND9W4O32oElaXzkAJxQTw/edit?usp=sharing



## Versioning

We use git for the versioning

## Authors

* **Francesco Scarlata**    - *Lead Programmer, project management* - [BitBucket](https://bitbucket.org/ReaperScarl/) -  [GitHub](https://github.com/FrancescoScarlata) - [LinkedIn](https://www.linkedin.com/in/francescoscarlata/)
* **Giada Passerini**       - *Design, Narrative, Team leader*      - [LinkedIn](https://www.linkedin.com/in/giada-passerini-2a999975/)
* **Simone Lazzaretti**     - *programmer*                          - [BitBucket](https://bitbucket.org/Principe94Simone/)
* **Chang Lin**             - *programmer*                          - [BitBucket](https://bitbucket.org/lc1995/)
* **Chris Malfasi**        - *Sound design*                        - [BitBucket](https://bitbucket.org/Chrix19/)
* **Silvia Monti**          - *Graphic Artist*                      - [BitBucket](https://bitbucket.org/sissysissy/)


## License

This project is licensed under the Copyright License - see the [LICENSE.md](LICENSE.md) file for details