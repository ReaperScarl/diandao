﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is responsible for the jump action of the player.
/// </summary>
[RequireComponent(typeof(Rigidbody2D), typeof(CapsuleCollider2D))]
[RequireComponent(typeof(Animator))]
public class CharacterJump : MonoBehaviour {

	// ------	Jump Public Variable	------
	[Header("Jump Height")]
	[Range(10.0f, 100.0f)]
	public float jumpHeight = 10.0f;		// Jump Speed

	[Header("Is Activated")]
	public bool activated = true;

	[Header("Jump Key")]
	public KeyCode jumpKey = KeyCode.Space;

	[Header("Jump Sound Effect")]
	public AudioClip jumpEffect;

	// ------	Shared Variable
	public bool isGrounded {get; private set;}

	// ------	Required Components		------
	private Rigidbody2D rb2d;
	private CapsuleCollider2D cc2d;
    private Animator anim; // animator variable

	// ------	Variables for Ground Detection	------
	private Vector2 center;		// Center of the BoxCollider2D
	private Vector2 pointA;
	private Vector2 pointB;
	private Collider2D[] collider2Ds;

	// ------	Jump Sound Effect	------
	private AudioClip landClip;
	private bool lastIsGrounded;

	// Use this for initialization
	void Start () 
	{
		rb2d = GetComponent<Rigidbody2D>();
		cc2d = GetComponent<CapsuleCollider2D>();
        anim = GetComponent<Animator>();

		center = cc2d.bounds.center;
		isGrounded = DetectGrounded();
		lastIsGrounded = isGrounded;
	}

	/// <summary>
	/// This method checks whether the player is grounded by detecting a rectangular region.
	/// If grounded, two conditions should be satisfied:
	/// - A collider2D should be detected.
	/// - The collider2D should not be a trigger.
	/// </summary>
	bool DetectGrounded()
	{
		bool onGround = false;

		center = cc2d.bounds.center;
		pointA = center - new Vector2(-cc2d.bounds.size.x * 0.32f, 
			cc2d.bounds.size.y * 1.01f * rb2d.transform.up.y * 0.5f);
		pointB = center - new Vector2(cc2d.bounds.size.x * 0.32f, 
			cc2d.bounds.size.y * 1.1f * rb2d.transform.up.y * 0.5f);

		collider2Ds = Physics2D.OverlapAreaAll(pointA, pointB);
		foreach(Collider2D collider2D in collider2Ds)
			if(!collider2D.isTrigger && collider2D.gameObject.tag != "player")
				onGround = true;

		return onGround;
	}
	
	// Update is called once per frame
	void Update () {
		lastIsGrounded = isGrounded;
		isGrounded = DetectGrounded();
		// Get key "Space" and check if it's on the ground
		if (activated) {
			if (isGrounded && Input.GetKeyDown (jumpKey)) {
				rb2d.velocity = new Vector2 (rb2d.velocity.x, jumpHeight * rb2d.transform.up.y);
				isGrounded = false;

				// Jump sound effect (Ignored in Beta)
				//			if (SoundEffectManager.Instance && jumpEffect)
				//				SoundEffectManager.Instance.PlayClip (jumpEffect);
			} else if (Input.GetKeyUp (jumpKey)) {
				if (rb2d.velocity.y > 0)
					rb2d.velocity = new Vector2(rb2d.velocity.x, rb2d.velocity.y * 0.5f);
			}
		}


		// Land sound effect （Ignored in Beta)
//		if (!lastIsGrounded && isGrounded)
//			SoundEffectManager.Instance.PlayClip (landClip);

        // animator variables
        anim.SetBool("grounded", isGrounded);
        anim.SetFloat("vSpeed", rb2d.velocity.y); 
	}

	/// <summary>
	/// Set jump clip.
	/// </summary>
	/// <param name="clip">Clip</param>
	public void SetClip(AudioClip clip){
		landClip = clip;
	}
}
