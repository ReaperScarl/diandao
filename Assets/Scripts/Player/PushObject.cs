﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is responsible for pushing object.
/// </summary>
[RequireComponent(typeof(Rigidbody2D), typeof(CapsuleCollider2D))]
[RequireComponent(typeof(CharacterMovement), typeof(CharacterJump))]
[RequireComponent(typeof(Animator))]
public class PushObject : MonoBehaviour {

	// ------	Push Object Public Viriable	------

	[Header("Push Speed")]
	[Range(1.0f, 10.0f)]
	public float pushSpeed = 1.0f;		// Player's speed when pushing an object

	[Header("Pull Object Speed")]
	[Range(1.0f, 10.0f)]
	public float pullSpeed = 1.0f;

	[Header("KeyCode for Catch Object")]
	public KeyCode catchKey = KeyCode.LeftShift;

	[Header("Push Clip")]
	public AudioClip pushClip;
	[Header("Pull Clip")]
	public AudioClip pullClip;

	// ------	Shared Viriable	------
	public bool activated = true;
	public State state = State.None;

	// ------	Public Enumerator	------
	public enum State :int{
		None,
		Idle,
		Pull,
		Push,
		MeetWall
	}

	// ------	Required Component	------

	private CharacterMovement characterMovement;
	private CharacterJump characterJump;
	private Rigidbody2D rb2d;
	private CapsuleCollider2D cc2d;
    private Animator anim;
	private GravityValues gravity;

	// ------	Variables for object detection

	private Collider2D[] collider2Ds;
	private Vector2 center;		// Center of the cc2d
	private Vector2 pointA;
	private Vector2 pointB;

	// ------	Viriables for push object control
	private Collider2D contactedCollider2D;
	private bool isFacedRight;		// True if player faces right
	private Vector2 detectedPoint;
	private bool isGrounded;
	private float idleSpeed;	// Player's origin speed
	private HingeJoint2D hj2d;
	private State lastState;

	// Use this for initialization
	void Start () 
	{
		characterMovement = GetComponent<CharacterMovement>();
		cc2d = GetComponent<CapsuleCollider2D>();
		characterJump = GetComponent<CharacterJump>();
		rb2d = GetComponent<Rigidbody2D>();
		gravity = GetComponent<GravityValues> ();
		hj2d = gameObject.AddComponent<HingeJoint2D> ();
		hj2d.enableCollision = true;
		hj2d.enabled = false;

        anim = GetComponent<Animator>();

		idleSpeed = characterMovement.speed;
	}

	void OnDisable(){
		state = State.None;
		lastState = state;
		characterMovement.flipEnabled = true;
		hj2d.enabled = false;
        characterMovement.speed = idleSpeed;
	}

	/// <summary>
	/// This method checks whether there is a collider2D at specific position according to the bound center.
	/// If detected, the following conditions should be satisfied:
	/// - A collider2D should be detected.
	/// - The collider2D should not be a trigger.
	/// </summary>
	/// <param name="posAhead">Relative position to the center of the player.</param>
	Collider2D DetectObjectAtPosition(Vector2 posAhead)
	{
		isFacedRight = characterMovement.GetFaceDirection();
		int isFacedRightInt = isFacedRight? 1: -1;

		center = (Vector2)cc2d.bounds.center;	// Get cc2d's center
		Vector2 pointA = center + new Vector2(posAhead.x * isFacedRightInt, posAhead.y);
		Vector2 pointB = center + new Vector2(posAhead.x * isFacedRightInt * 1.01f, -posAhead.y);

		collider2Ds = Physics2D.OverlapAreaAll(pointA, pointB);
		foreach(Collider2D collider2D in collider2Ds)
			if(!collider2D.isTrigger)
			{
				Rigidbody2D cRb2d;
				if((cRb2d = collider2D.GetComponent<Rigidbody2D>()) &&
					cRb2d.bodyType == RigidbodyType2D.Dynamic)
				return collider2D;
			}
				
		return null;
	}
	
	// Update is called once per frame
	/// <summary>
	/// The player will only push an object when an object is detected and the player is grounded.
	/// If the player isn't grounded and touches something, he should stop.
	/// Otherwise, the player keeps its idle speed.
	/// </summary>
	void Update () {
		isGrounded = characterJump.isGrounded;		// Get whether the player is grounded
		detectedPoint = new Vector2(cc2d.bounds.size.x * 0.75f, Mathf.Epsilon);
		lastState = state;
		state = State.None;

		if (contactedCollider2D = DetectObjectAtPosition (detectedPoint)) 
		{
			if (activated && isGrounded && Input.GetKey (catchKey)) 
			{
				state = State.Idle;
				if (lastState != State.Idle) {
					characterMovement.flipEnabled = false;
					hj2d.enabled = true;
					hj2d.connectedBody = contactedCollider2D.GetComponent<Rigidbody2D> ();
					characterJump.activated = false;
				}

				float hor = Input.GetAxis ("Horizontal");
				float isFacedRightFloat = characterMovement.GetFaceDirection () ? 1f : -1f;
				Rigidbody2D contactedRb2d = contactedCollider2D.GetComponent<Rigidbody2D>();
				if (hor * isFacedRightFloat > 0) 
				{
					state = State.Push;
					//if (lastState != State.Push)
					//	SoundEffectManager.Instance.PlayClip (pushClip);

					// Detect second forward object
					detectedPoint = new Vector2(cc2d.bounds.size.x * 0.75f
						+ contactedCollider2D.bounds.size.x * 1.1f, Mathf.Epsilon);
					if(DetectObjectAtPosition(detectedPoint))
					{
						characterMovement.speed = 0;
						return;
					}

					characterMovement.speed = pushSpeed;
					contactedRb2d.velocity = new Vector2 (pushSpeed * isFacedRightFloat, contactedRb2d.velocity.y);
				} 
				else if (hor * isFacedRightFloat < 0) 
				{
					state = State.Pull;
					//if (lastState != State.Pull)
						//SoundEffectManager.Instance.PlayClip (pullClip);

					// Detect backward object
					detectedPoint = new Vector2(-cc2d.bounds.size.x * 0.55f, Mathf.Epsilon);
					if(DetectObjectAtPosition(detectedPoint))
					{
						characterMovement.speed = 0;
						return;
					}

					characterMovement.speed = pullSpeed;
					contactedRb2d.velocity = new Vector2 (-pullSpeed * isFacedRightFloat, contactedRb2d.velocity.y);
				} 
				else 
				{
					state = State.Idle;
				}
			}
			else 
			{
				state = State.MeetWall;
				if (lastState != State.MeetWall) {
					hj2d.enabled = false;
					hj2d.connectedBody = null;
					characterMovement.flipEnabled = true;
					characterMovement.speed = 0;
					if (rb2d.gravityScale == gravity.oneRock || rb2d.gravityScale == gravity.twoRocks)
						characterJump.activated = true;
				}
			}
		} 
		else 
		{
			state = State.None;
			if (lastState != State.None) {
				hj2d.enabled = false;
				hj2d.connectedBody = null;
				characterMovement.flipEnabled = true;
				characterMovement.speed = idleSpeed;
				if (rb2d.gravityScale == gravity.oneRock || rb2d.gravityScale == gravity.twoRocks)
					characterJump.activated = true;
			}
		}
			
		if(state == State.MeetWall)
			anim.SetInteger ("push&Pull", (int)State.None);
		else
			anim.SetInteger ("push&Pull", (int)state);
    }
}
