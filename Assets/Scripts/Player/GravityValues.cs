﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// this is script with the Informations of the gravity. the values are the gravity value for each phase.
/// when you are with the dress, the only values are "zeroRocks" and "twoRocks"
/// </summary>
public class GravityValues : MonoBehaviour {

	public int zeroRocks=-15;
	public int oneRock=-5;
	public int twoRocks=+5;
	public int threeRocks=+15;


    private int currentGravity;


    public void SetCurrentGravity(int gravity)
    {
        currentGravity = gravity;
    }

    public int GetCurrentGravity()
    {
        return currentGravity;
    }
}
