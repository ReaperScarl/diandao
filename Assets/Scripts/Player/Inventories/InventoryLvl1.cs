﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Class that manages the inventory for the 1rst lvl
/// </summary>
[RequireComponent(typeof(EnableController))]
public class InventoryLvl1 : InventoryController
 {

    [Header("     Elements of the first level    ")]
    [Space(5)]
    [Header("FatherDocument elements:")]
    [Space(10)]

    [Header("Door to open in the junkyard:")]
    public _Action doorGear;

    [Header("dialogue of the father:")]
    public _Action fatherTalkAction;

    [Header("Door to unlock in the gearRoom :")]
    public _Action doorLab;

    [Header("The button to disable")]
    public ButtonPipeController pipeButton;

    public override void Take(ItemInfo element)
    {
        base.Take(element);

        switch (element.ID) // important things when taking objects in the lvl 1
        {
            case ItemID.FatherDocument:
                /// When taking the father document what should happen is:
                /// - The father starts to talk
                /// - the door opens
                /// - the dress is disabled
                /// - the door in the library

                fatherTalkAction.DoAnAction();
                doorGear.DoAnAction();
                doorLab.DoAnAction();
                pipeButton.DeactivateThePipe();
                break;
        }
    }
}