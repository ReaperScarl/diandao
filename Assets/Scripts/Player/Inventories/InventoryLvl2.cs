﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class the manages the invenroty for the 2rst lvl
/// </summary>
[RequireComponent(typeof(EnableController))]
public class InventoryLvl2 : InventoryController 
{
	// ------	Public Variables
	[Header("Elements of the second level")]
	[Space(5)]

	[Header("Key elements:")]
	[Space(5)]

	[Header("Puddle to open")]
	public GameObject puddle;
	[Header("Dialog after grabing the key")]
	public GameObject dialogKey;
	[Header("Reset Component")]
	public ResetGrabbableObject resetScript;

	// Use this for initialization
	protected override void Start () 
	{
		base.Start ();

	}

	/// <summary>
	/// Method called by the player when it grabs something
	///   - Key: Start a dialogue
	/// </summary>
	/// <param name="element">the informations about the element</param>
	public override void Take(ItemInfo element)
	{
		base.Take (element);

		switch (element.ID) 
		{
		case ItemID.Key:
			dialogKey.GetComponent<_Action> ().DoAnAction ();
			resetScript.isTaken = true;
			break;
		}
	}
}
