﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

/// <summary>
/// This is the script that stores the elements of the Game.
/// The elements are stores by some fundamental information like:
///  - ID of the element 
///  - Description of the element
///  - GameObject of the element (if the element has to drop a prefab in the scene)
/// </summary>
public class InventoryController : MonoBehaviour {

    LinkedList<ItemInfo> elements=new LinkedList<ItemInfo>(); // IDs of the objects

    protected Text elementText;
    protected InventoryGUIManager gUIManager;
    protected DelayedDisappearScript disappScript;


	// Use this for initialization
	protected virtual void  Start () {
       
        gUIManager = GameCanvasManager.Instance.inventoryGUI;
        elementText = GameCanvasManager.Instance.elementText;
        disappScript = elementText.GetComponent<DelayedDisappearScript>();
    }

    /// <summary>
    /// Method called by the player when it grabs something
    /// </summary>
    /// <param name="element"> the informations about the element</param>
    public virtual void Take(ItemInfo element)
    {
        elements.AddLast(element);
        gUIManager.AddItem(element.ID, element.interfaceElement);
        elementText.text = element.description;
        disappScript.Disappear();
    }

    /// <summary>
    /// Method called by the player when it drops something
    /// </summary>
    /// <param name="IDelement">   The id of the element we need to drop     </param>
    /// <param name="withoutShow">   False if The drop will leave an element in the level   </param>
    public void Drop(ItemID IDelement)
    {
        // Removes the element
        ItemInfo element = elements.FirstOrDefault(x => x.ID == IDelement);
        if (element != null)
            elements.Remove(element);

    }


    /// <summary>
    /// Checks if the item requested is in the inventory or not
    /// </summary>
    /// <param name="ID">the ID of the item</param>
    /// <returns></returns>
    public bool IsItemInInventory(ItemID ID)
    {
        //Debug.Log(elements.FirstOrDefault(x => x.ID == ID));
        if((elements.FirstOrDefault(x => x.ID == ID))!=null)
            return true;

        return false;
    }

}
