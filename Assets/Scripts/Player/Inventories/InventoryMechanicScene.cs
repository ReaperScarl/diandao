﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Inventory for the grab scene mechanic
/// </summary>
public class InventoryMechanicScene : InventoryController {

    public DoorLevelInteractionScript door;


    /// <summary>
    /// Overrided the method.
    /// If the element is Other, than you can unlock the door
    /// </summary>
    /// <param name="element"></param>
    public override void Take(ItemInfo element)
    {
        base.Take(element);
        switch(element.ID)
        {
            case ItemID.Other:
                door.isLocked = false;
                break;
        }
    }
}
