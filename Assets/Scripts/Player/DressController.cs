﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the script that controls the dress element
/// </summary>
public class DressController : MonoBehaviour {

    [Header("The clip to play when it hits the ground")]
    public AudioClip clipWhenTouchesTheGround;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "player")
        {
            collision.GetComponent<PlayerController>().DressInRange(true,this.gameObject);
            //Debug.Log("is inside");
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "player")
        {
            collision.GetComponent<PlayerController>().DressNotInRange();
            //Debug.Log("is outside");
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag!="player"&& clipWhenTouchesTheGround)
            SoundEffectManager.Instance.PlayClip(clipWhenTouchesTheGround);
    }
}
