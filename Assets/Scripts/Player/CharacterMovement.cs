﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  This class manages the movement of the player and the flip of the sprite when changing direction
/// </summary>

[RequireComponent(typeof(Animator), typeof(CharacterJump))]
public class CharacterMovement : MonoBehaviour {

    [Header("Player Speed")]
    [Range(5.0f, 50.0f)]
    public float speed;                 //speed of our character       

    public bool movementEnabled = true;     // variable to stop only the input but not all the script
    public bool flipEnabled = true;

    [Space(10)]
    [Header(" Offset Head for the detection")]
    [Range(-1,1)]
    public float headOffset=0.885f;

    [Header(" Offset feet for the detection")]
    [Range(-1.5f, 0)]
    public float feetOffset = -0.902f;

    [Header(" Offset from the center of the character to the horizontal extremis (x axis)")]
    [Range(0.1f, 1.5f)]
    public float horizontalOffSet = 0.4f;

    [Header(" Time between a footstep sound effect and the following one")]
    [Range(0.1F, 0.8F)]
    public float distanceBetweenFootsteps;

    [Header("How much the pitch can vary (as upper and down boundaries)")]
    [Range(0F, 1F)]
    public float changePitch=0.05f;

    [Header("Variable make last of the footstep sound effect longer. Used for push and pull")]
    [Range(0.01F, 1.00F)]
    public float pushNpullMultiplier = 0.50f;

    [Header("If true, the player starts facing right. False otherwise")]
    public bool startingFacing;
    [Header("If true, the player starts on the ground. False otherwise")]
    public bool startingGround;

    private float m_horizontal;         //horizontal axis for our character

    private bool rightFace = true;
    private bool onCeiling = false;
    public bool isAlreadyMoving = false; 

    private Rigidbody2D rb2d;           //rigidbody attached to the player 

    private CharacterJump cJ;
    private PushObject pO;
    private Animator anim;

    private Vector3 vectorOffsetHead;
    private Vector3 vectorOffsetFeet;

    private AudioClip[] currentFootSteps;

    private float timeBetweenChecksInSoundEffect = 0.02f;
    private WaitForSeconds delayInSoundEffect;

    // Use this for initialization
    void Start()
    {
        //Get and store a reference to the Rigidbody2D component so that we can access it.
        rb2d = GetComponent<Rigidbody2D>();
        m_horizontal = 0.0f;

        anim = GetComponent<Animator>();
        cJ = GetComponent<CharacterJump>();
        pO = GetComponent<PushObject>();
        vectorOffsetHead = new Vector3(0,headOffset,0);
        vectorOffsetFeet = new Vector3(0, feetOffset, 0);

        delayInSoundEffect = new WaitForSeconds(timeBetweenChecksInSoundEffect);
    }

    //FixedUpdate is called at a fixed interval and is independent of frame rate. Put physics code here.
    void FixedUpdate()
    {
        m_horizontal = 0; // starts with 0
        if (movementEnabled)
            m_horizontal = Input.GetAxis("Horizontal");

        anim.SetFloat("speed", Mathf.Abs(m_horizontal)); //animator speed part

        
		if(!onCeiling && ( (m_horizontal > 0 && !rightFace) || (m_horizontal < 0 && rightFace) ) ) // on the floor 
        {
            Flip();
        }
		else if (onCeiling && ( (m_horizontal < 0 && rightFace) || (m_horizontal > 0 && !rightFace) ) ) // on the ceiling
        {
            Flip();
        }
        if(!cJ.isGrounded&& DetectWall())
        {
            //Debug.Log("Wall!");
            m_horizontal = 0;
        }

        if(movementEnabled)
            rb2d.velocity = new Vector2(m_horizontal * speed, rb2d.velocity.y);

        if (!isAlreadyMoving && m_horizontal != 0 && cJ.isGrounded)
        {
            isAlreadyMoving = true;
            StartCoroutine(SoundEffect(currentFootSteps));
        }
    }

    /// <summary>
    /// this method flips the direction of the character while moving
    /// </summary>
    public void Flip()
    {
        if (flipEnabled)
        {
            rightFace = !rightFace;
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
    }

    /// <summary>
    /// This method updates the
    /// footSteps sound effect
    /// </summary>
    /// <param name="fSteps">Set of sound effects used for the footsteps</param>
    public void StartingSoundEffect(AudioClip[] fSteps)
    {
        currentFootSteps = fSteps;      
    }

   

    /// <summary>
    /// This method asks the SoundManager
    /// to play a certain clip 
    /// if the player is moving 
    /// and is on the ground at a
    /// certain frequence
    /// </summary>
    /// <returns></returns>
    private IEnumerator SoundEffect(AudioClip[] fSteps)
    {
        float timeTofinish;
        if (pO.state == PushObject.State.Pull || pO.state == PushObject.State.Push)
        {
            timeTofinish = Time.time + distanceBetweenFootsteps/pushNpullMultiplier;
        }
        else timeTofinish = Time.time + distanceBetweenFootsteps;
        while (m_horizontal != 0 && cJ.isGrounded && isAlreadyMoving&& currentFootSteps==fSteps) // maybe redundant, but just to be sure
        {          
            int choice = UnityEngine.Random.Range(0, currentFootSteps.Length);
            float randomPitch = UnityEngine.Random.Range(0 - changePitch, 0 + changePitch);
            SoundEffectManager.Instance.PlayClip(currentFootSteps[choice], 1 + randomPitch);
            while (timeTofinish >= Time.time)
            {
                if (!cJ.isGrounded)
                    break;
                yield return delayInSoundEffect;
            }
            if (pO.state == PushObject.State.Pull || pO.state == PushObject.State.Push)
            {
                timeTofinish = Time.time + distanceBetweenFootsteps / pushNpullMultiplier;
            }
            else timeTofinish = Time.time + distanceBetweenFootsteps;
        }
        isAlreadyMoving = false;
    }

    public bool GetFaceDirection()
    {
        return this.rightFace;
    }


    /// <summary>
    /// This method detects the walls when the player is moving not in the ground.
    /// With this method, if the player is going towards a wall, it goes down (or up)
    /// </summary>
    /// <returns> returns true if there is an object detected with no rigid body or with a rigid body not dynamic</returns>
    private bool DetectWall()
    {
        // maybe these variables can be declared before and then use it here.
        Vector3 myPosition = transform.position;
        Vector3 rayDirection;

        if ((!onCeiling && rightFace) || (onCeiling && !rightFace))
            rayDirection = Vector3.right;
        else
            rayDirection = Vector3.left;

        int ceilingcorrector = (onCeiling) ? -1 : 1;

        Collider2D[] colliders;
        /// Overlap area will check if there is a collider in the rectangle defined by those 2 points
        /// The points are given by using 
        /// 1: the position of the center of the player
        /// 2: (the ray direction * the horizontal Offset) -> this gives us the left/right offset
        /// 3:  head /feet offset -> so we have all the area from head to feet
        /// 4: this overall offset is multiplied by 1/-1 depending on the ceiling
        /// 5: now the corrected offsets are added to the position to give the area needed
        colliders = Physics2D.OverlapAreaAll(myPosition + (rayDirection * horizontalOffSet + vectorOffsetHead) * ceilingcorrector,
                    myPosition + (rayDirection * (horizontalOffSet - 0.1f) + vectorOffsetFeet) * ceilingcorrector);
        {
            //Debug.Log("Is there a wall?");
            for(int i=0; i<colliders.Length; i++)
            {
                Rigidbody2D rigidbody2D = colliders[i].GetComponent<Rigidbody2D>();
                //Debug.Log("collider name: " + colliders[i].name);
                if (colliders[i].tag!="player" && ( 
                    (rigidbody2D && colliders[i].isTrigger != true)
                    || (!rigidbody2D && colliders[i].isTrigger!=true))
                    )
                    return true;
            }
        }
        return false;
    }

	/// <summary>
	/// This method changes the "onCeiling" variable. When it's called, the gravity is changed as the input.
	/// It changes the right face once when it chages gravity
	/// </summary>
	/// <param name="gravityUp"> gravity up is true if the current gravity is negative</param>
	public void ChangeGravity(bool gravityUp){
		onCeiling = gravityUp;
		rightFace = !rightFace;
	}


    /// <summary>
    /// This method gives the right setting 
    /// when the player restart from the checkpoint
    /// </summary>
    public void OnEnable()
    {
        if (rightFace != startingFacing)
        {
            Flip();
        }
        onCeiling = !startingGround;
    }

    /// <summary>
    /// Method called by the enable Controller to stop the velocity of the player
    /// </summary>
    public void Stop()
    {
        Vector2 vel = rb2d.velocity;
        rb2d.velocity = new Vector2(0,vel.y);
    }
}