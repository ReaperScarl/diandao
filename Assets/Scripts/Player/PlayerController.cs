﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class is responsible for the abilities of the player. 
/// This script manages:
/// - The gravity change (dress ability and rocks)
/// - The interaction ability
/// - The grab ability
/// - The talk ability
/// </summary>
[RequireComponent(typeof(Rigidbody2D), typeof(CharacterMovement), typeof(GravityValues))]
[RequireComponent(typeof(CharacterMovement), typeof(InventoryController))]
[RequireComponent(typeof(Animator),typeof(CharacterJump))]

public class PlayerController : MonoBehaviour {

    // -------              Gravity public variables       ------
    [Header("          Gravity variables")]
    [Header("True if the dress is enabled and the rock is not (and viceversa)")]
    public bool isDressActive=true;

    [Header("Minimum amount in seconds to completely invert the body")]
    public float minTimeToInvert = 0.5f;

    [Header("Camera of the level")]
    public Camera mainCamera;

    [Space(10)]
    // -------          Dress public variables          ----------
    [Header("          Dress Variables (when isDressActive is true)")]
   
  
    [Header("Does Xia has the dress at the start of the level? y/n")]
    public bool hasTheDress = true;

    [Header("The dress prefab")]
    public GameObject dressPrefab;

    [Header("Is the dress enabled while the dress is active")]
    public bool dressEnabled = false;
    [Space(10)]

    // -------          Rock public variables          ----------
    [Header("          Rock Variables (when isDressActive is false)")]
    [Header("The number of rocks of the player")]
    [Range(0,4)]
    public int numberOfRocks;

    [Header("The rock prefab when the rock has to be thrown")]
    public GameObject rockPrefab;

    [Header ("Is the rock mech enabled")]
    public bool rockEnabled = true;



    // ---------                Key nodes         ----------

    [Space(10)]
    [Header("        Key Codes         ")]
    [Space(10)]

    [Header("Key to take the dress")]
    public KeyCode dressOnKey = KeyCode.E;

    [Header("Key to take out the dress")]
    public KeyCode dressOffKey = KeyCode.X;

    [Header("Key for the interaction ability")]
    public KeyCode interactionKey = KeyCode.E;

    [Header("Key for the grab ability")]
    public KeyCode grabKey = KeyCode.E;

    [Header ("KeyCode to talk with NPCs")]
    public KeyCode talkKey = KeyCode.E;


    // -----------------              PRIVATE            -----------------

    // ---------            Gravity variables      ----
    private bool gravityUp;

    private Rigidbody2D rigidBody;
    private const int feetDown = 0;
    private const int feetUp = 180;

    private GravityValues gravity; // enumerator that says the value of the gravity

    //    dress variables
    private bool isDressInRange=false;
    private GameObject dressInRange;
    private WaitForSeconds delay = new WaitForSeconds(0.5f);
    private WaitForSeconds delayForSpawn = new WaitForSeconds(0.1f);

    //    rock variables
    private bool isRockInRange = false;
    private GameObject rockInRange;

    private bool dressInCooldown=false;
    private CharacterJump cJ;
    private bool alreadyUsed1Action;

    // -------          Interaction variables   ------------
    [HideInInspector]
    public Text interactionText;
    public bool isInteractionEnabled = true;

    private _Interactions interactionObject;
    private bool canInteract = false;

    // -------          Grab variables          ------------

    private bool canGrab = false;
    private ItemController grabObject;
    private InventoryController inventory;

    // -------          Talk  variables          ------------

    private bool canTalk = false;
    private GameObject NPCObject;

    // -------     External scripts     -------

    private CameraFollowing camFoll;
    private Animator anim;
    private CharacterMovement cm;
    private bool inizialised=false; // to not make the use the enable before the start

    void Start()
    {
        interactionText = GameCanvasManager.Instance.interactionText;
        inizialised = true;
        camFoll = mainCamera.GetComponent<CameraFollowing>();
        anim = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody2D>();
		cm = GetComponent<CharacterMovement> ();
        gravity = GetComponent<GravityValues>();
        cJ = GetComponent<CharacterJump>();
        if (isDressActive)
        {
            if (hasTheDress)
                gravity.SetCurrentGravity(gravity.twoRocks);
            else
                gravity.SetCurrentGravity(gravity.zeroRocks);   
        }
        else // Rock Mechanic
        {
            switch (numberOfRocks)
            {
                case 0:
                    gravity.SetCurrentGravity(gravity.zeroRocks);
                    break;
                case 1:
                    gravity.SetCurrentGravity(gravity.oneRock);
                    break;
                case 2:
                    gravity.SetCurrentGravity(gravity.twoRocks);
                    break;
                case 3:
                    gravity.SetCurrentGravity(gravity.threeRocks);
                    break;
            }
        }
  
        ChangeGravity(gravity.GetCurrentGravity());
        inventory = GetComponent<InventoryController>();
        // this works only if the player is not grounded when it starts
        if(isDressActive)
                anim.SetBool("hasDress", hasTheDress);
    }

    // Update is called once per frame
    void Update()
    {
        alreadyUsed1Action = false;
        // Dress ability
        if (isDressActive)
        {
            if (dressEnabled)
                alreadyUsed1Action = DressCheck();
        }
        else // rock check
        {
            if(rockEnabled)
                alreadyUsed1Action= RockCheck();
        }        

        // interaction ability while in the trigger
        if (!alreadyUsed1Action&& isInteractionEnabled && canInteract && Input.GetKeyDown(interactionKey))
        {
            alreadyUsed1Action = true;
            Interact();
        }
        if (!alreadyUsed1Action && canGrab && Input.GetKeyDown(grabKey))
        {
            alreadyUsed1Action = true;
            Grab();
        }

        if (!alreadyUsed1Action &&  canTalk && Input.GetKeyDown(talkKey))
            Talk();
    }

    //------                    GRAVITY methods           ------

    /// <summary>
    /// This method checks in the update when it is pressed the button of the dress.
    /// 2 cases:
    /// 1- she has the dress, then she takes it off
    /// 2 - she hasn't the dress, then se has to check if there is a dress nearby and she can put it on.
    /// </summary>
    private bool DressCheck ( )
        {
		if (!dressInCooldown) 
		{
			if (Input.GetKeyDown(dressOffKey)&&hasTheDress) // dress off
			{
                gravity.SetCurrentGravity(gravity.zeroRocks);
                hasTheDress = false;
                anim.SetBool("hasDress", hasTheDress);
				ChangeGravity(gravity.GetCurrentGravity());
                StartCoroutine(DressDelaySpawn(transform.position));
				StartCoroutine (DressDelay ());
                return true;
			}
			if (Input.GetKeyDown(dressOnKey) && !hasTheDress&& isDressInRange) // dress on
			{
                gravity.SetCurrentGravity(gravity.twoRocks);
                ChangeGravity(gravity.GetCurrentGravity());
                Destroy(dressInRange); // destroy the dress
                hasTheDress = true;
                anim.SetBool("hasDress", hasTheDress);
                StartCoroutine (DressDelay ());
                interactionText.text = "";
                return true;
            }
		}
        return false;
    }

    /// <summary>
    /// This method checks in the updtare when it is pressed the button of the rock (dress)
    /// - when there is a rock in range and the character has less than 3 rocks, than she can take it
    /// </summary>
    /// <returns> if the check has taken a rock or not</returns>
    private bool RockCheck()
    {
        if (Input.GetKeyDown(dressOnKey)&& isRockInRange && rockInRange && numberOfRocks<3) // Rock to add
        {
            AddRock();
            Destroy(rockInRange);
            interactionText.text = "";
            numberOfRocks++;
            GameCanvasManager.Instance.numberOfRockText.text = "" + numberOfRocks;
            return true;
        }
        return false;
    }

    /// <summary>
    /// method call externally when the internal gravity is changed (like dress on/off etc)
    /// </summary>
    /// <param name="gravityPhase"> the gravity value</param>
    public void ChangeGravity(int gravityPhase)
    {
        rigidBody.gravityScale = gravityPhase;
        gravity.SetCurrentGravity(gravityPhase);

        if (gravityPhase < 0) // gravity up
        {
            if (gravityPhase == gravity.zeroRocks) // also dress off
            {
                cJ.activated = false;
                if (isDressActive)
                {
                    gravityUp = true;
                    anim.SetBool("hasDress", hasTheDress);
                    cm.ChangeGravity(gravityUp);
                    camFoll.ChangeRect(gravity);
                }     
            }
            if (gravityPhase == gravity.oneRock)
            {
                cJ.activated = true;
                if (!gravityUp && gravityPhase < 0)
                {
                    gravityUp = true;
                    cm.ChangeGravity(gravityUp);
                    camFoll.ChangeRect(gravityUp);
                }
                if(!isDressActive) //the animator to go upside down?
                {
                    anim.SetBool("isGravityDown", false);
                }
            }
            if (!gameObject.activeInHierarchy)
                transform.rotation= Quaternion.Euler(0, 0, 180);
        }
        else // gravity down
        {
            if (gravityPhase == gravity.twoRocks) // also dress on
            {
                if(gravityUp && gravityPhase > 0)
                {
                    gravityUp = false;
                    cm.ChangeGravity(gravityUp);
                    camFoll.ChangeRect(gravityUp);
                }
                if (isDressActive)
                    anim.SetBool("hasDress", hasTheDress);
                else //the animator for the gravity down
                {
                    anim.SetBool("isGravityDown", true);
                }
                cJ.activated = true;
            }
            if (gravityPhase == gravity.threeRocks)
            {
                cJ.activated = false;
            }
            if (!gameObject.activeInHierarchy)
                transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        if (gameObject.activeInHierarchy)
            StartCoroutine(RotateFromGravity(!gravityUp));       
    }

    /// <summary>
    /// Method called in the respawn. Maybe temporary
    /// </summary>
    /// <param name="isGravityUp"></param>
    /// <param name="gravityPhase"></param>
    /// <param name="playerHasDress"></param>
    public void ResetGravity(bool isGravityUp,int gravityPhase )
    {
        dressInCooldown = false;

        if (gravityUp != isGravityUp)
        {
            gravityUp = isGravityUp;
            cm.ChangeGravity(gravityUp);
            camFoll.ChangeRect(gravityUp);
        }
            
        ChangeGravity(gravityPhase);

    }

    private void OnEnable()
    {
        if (inizialised)
        {
            if (isDressActive)
            {
                if (anim.GetBool("hasDress") != hasTheDress)
                    anim.SetBool("hasDress", hasTheDress);
            }
            else
            {
                if (gravity.GetCurrentGravity() > 0)
                    anim.SetBool("isGravityDown", true);
                else
                    anim.SetBool("isGravityDown", false);
            }
        }
    }

    /// <summary>
    ///     This creates the rotation of the character when the gravity is changed
    /// </summary>
    private IEnumerator RotateFromGravity(bool heavierThanAir)
    {
      //  float velocity =  feetUp/minTimeToInvert;

        float startingRotation = this.transform.rotation.eulerAngles.z;
        float threshold = 45;
        float tempRotation;
        
        for (float timeToInvert=0;  
            heavierThanAir != gravityUp && timeToInvert <= 1; 
            timeToInvert+=0.1f/minTimeToInvert ) //if there is a change in gravity, this should to finish
        {  
            if (heavierThanAir) // towards the ground
            {
                if (Mathf.Abs(startingRotation - feetDown) < 0.02f)
                    break;
                tempRotation = Mathf.Lerp(startingRotation, feetDown, timeToInvert);
            }
            else
            {
                if (Mathf.Abs(startingRotation - feetUp) < 0.02f)
                    break;
                tempRotation = Mathf.Lerp(startingRotation, feetUp, timeToInvert);
            }

            //Debug.Log("temp rot: " + tempRotation);
            transform.rotation=Quaternion.Euler(0,0, tempRotation);
            yield return null;
        }

        float finalRotation = transform.rotation.eulerAngles.z % 360;
        //Debug.Log(" final rotation: " + transform.rotation.eulerAngles);

        if (finalRotation > feetDown-threshold && finalRotation < feetDown+threshold)
            transform.rotation = Quaternion.Euler(0, 0, feetDown);
        if (feetUp-threshold<finalRotation && finalRotation < feetUp+threshold)
            transform.rotation = Quaternion.Euler(0, 0, feetUp);
      //  hasPassedTheThreshold = false;
    }

    /// <summary>
    /// This method is called when the trigger of the dress is triggered.
    /// This will send this method to say that the dress is in range and it can be picked up
    /// </summary>
    /// <param name="isEntering"></param>
    public void DressInRange(bool isEntering,GameObject dress)
    {
        isDressInRange = isEntering;
        interactionText.text = "Press '" + dressOnKey + "' to take the Skirt";
        if (isEntering)
            dressInRange = dress;
    }

    /// <summary>
    /// This is the script called by the dress controller to remove the dress
    /// </summary>
    public void DressNotInRange()
    {
        isDressInRange = false;
        interactionText.text = "";
        dressInRange = null;
    }

    /// <summary>
    /// This method is called when the trigger of the rock is triggered.
    /// This will send this method to say that the rock is in range and it can be picked up
    /// </summary>
    /// <param name="isEntering"> true if the player is entering the </param>
    public void RockInRange(bool isEntering, GameObject rock)
    {
        isRockInRange = isEntering;
        interactionText.text = "Press '" + dressOnKey + "' to take the rock";
        if (isEntering)
            rockInRange = rock;
    }

    /// <summary>
    /// This is the script called by the rock controller to remove the rock
    /// </summary>
    public void RockNotInRange()
    {
        isRockInRange = false;
        interactionText.text = "";
        rockInRange = null;
    }

    /// <summary>
    /// This method is called by the rock check and it takes a rock.
    /// </summary>
    private void AddRock()
    {
        switch (numberOfRocks)
        {
            case 0:
                gravity.SetCurrentGravity(gravity.oneRock);
                break;
            case 1:
                gravity.SetCurrentGravity(gravity.twoRocks);
                break;
            case 2:
                gravity.SetCurrentGravity(gravity.threeRocks);
                break;
        }
        ChangeGravity(gravity.GetCurrentGravity());
    }

    /// <summary>
    /// The method called to drop the rock. This is called by other scripts that will instantiate the rock as they want
    /// </summary>
    /// <returns> the gameobject of the rock prefab</returns>
    public GameObject DropRock()
    {
        if (numberOfRocks > 0)
        {
            numberOfRocks--;
            GameCanvasManager.Instance.numberOfRockText.text = "" + numberOfRocks;
            switch (numberOfRocks)
            {
                case 2:
                    gravity.SetCurrentGravity(gravity.twoRocks);
                    break;
                case 1:
                    gravity.SetCurrentGravity(gravity.oneRock);
                    break;
                case 0:
                    gravity.SetCurrentGravity(gravity.zeroRocks);
                    break;
            }
            ChangeGravity(gravity.GetCurrentGravity());
            return rockPrefab;
        }
        else
            return null;
        
    }

	/// <summary>
	/// This enumerator waits 1 sec before enable again the dress ability
	/// </summary>
	/// <returns>The delay.</returns>
	IEnumerator DressDelay(){
		dressInCooldown = true;
		yield return delay;
        dressInCooldown = false;
	}

    /// <summary>
    /// This enumerator waits 1 sec before enable again the dress ability
    /// </summary>
    /// <returns>The delay.</returns>
    IEnumerator DressDelaySpawn(Vector3 position)
    {
        yield return delayForSpawn;
        Instantiate(dressPrefab, position, dressPrefab.transform.rotation);
        //  Debug.Log("cooldown");
    }


    //------                    INTERACTION methods         ------

    /// <summary>
    ///  This method is called by and interactible object when the player enters the trigger collider.
    ///  It saves the interaction object to be called after
    /// </summary>
    /// <param name="interactibleObject"> the gameobject that called the method</param>
    public void AddInteraction(_Interactions interactibleObject)
    {
        canInteract = true;
        interactionObject = interactibleObject;
        interactionText.text = "Press '" + interactionKey + "'\n to "+ interactibleObject.messageToSayWhenInteracting;
    }

    /// <summary>
    /// This method is called by the interactible object when the player exits its range.
    /// </summary>
    public void DeleteInteraction()
    {
        canInteract = false;
        interactionObject = null;
        interactionText.text = "";
    }

    /// <summary>
    /// Action made by the player when the object is interactible.
    /// It calls the action inside the element.
    /// </summary>
    private void Interact()
    {
        if(interactionObject)
            interactionObject.Action();
    }

    //------                GRAB Methods                -------

    /// <summary>
    /// Called by the grabbable object when the player enters the range
    /// </summary>
    /// <param name="grabbableObject">the object to grab</param>
    public void AddToGrab(ItemController grabbableObject)
    {
        canGrab = true;
        grabObject = grabbableObject;
        interactionText.text = "Press '" + grabKey + "'\n to Grab "+grabObject.description;
    }

    /// <summary>
    /// This method is called by the interactible object when the player exits its range.
    /// </summary>
    public void ExitFromGrabRange()
    {
        canGrab = false;
        grabObject = null;
        interactionText.text = "";
    }

    /// <summary>
    /// Called in the update when the player wants to grab
    /// </summary>
    public void Grab()
    {
        if (grabObject)
        {
            inventory.Take(grabObject.GetComponent<ItemController>().GetInfo());
            // grabObject.gameObject.SetActive(false);
			Destroy(grabObject.gameObject);
        }
        ExitFromGrabRange();
    }

    //------               TALK Methods                -------

    /// <summary>
    /// Method called when the player enters inside the range of an NPC that talks
    /// </summary>
    /// <param name="talkableObject">the talkable object</param>
    public void AddToTalk(GameObject talkableObject)
    {
        canTalk = true;
        NPCObject = talkableObject;
        interactionText.text = "Press '" + talkKey + "'\n to Talk";
    }

    /// <summary>
    /// This method is called by the NPC object when the player exits its range.
    /// </summary>
    public void ExitFromTalkRange()
    {
        canTalk = false;
        NPCObject = null;
        interactionText.text = "";
    }

    /// <summary>
    ///  Starts the dialog with the NPC
    /// </summary>
    public void Talk()
    {
        if (NPCObject)
            NPCObject.GetComponent<_TalkNPC>().Talk(this.gameObject);
    }
}