﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class is repsonsible for death mechanic
/// This script should be attached to the canvas contains the following objects:
///   - A died image
///   - A resume button
/// </summary>
public class DeathMechanic : MonoBehaviour {

	// ------	Public Variable	------
	[Header("Is player dead")]
	public bool isDead = false;

	[Header("Time of Fade Out (seconds)")]
	public float timeFadeOut = 5.0f;

	[Header("Time of Stay")]
	public float timeStay = 1.0f;

	[Header("Time of Fade In when respawn (seconds)")]
	public float timeFadeOutRespawn = 5.0f;

	[Header("Canvas Elements")]
	public GameObject diedImage;
	public GameObject respawnImage;

	[Header("Checkpoint Manager")]
	public CheckPointManager cpm;

	[Header("InGameMenu Manager")]
	public InGameMenuManager igmm;

	// ------	Private Variable	------
	private GameObject player;
	private Image _image;
	private RawImage _imageRespawn;

	private float alphaInterval;	// Each time how many alpha value is changed
	private float alphaIntervalRespawn;	// Each time how many alpha value is changed when respawn

	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindGameObjectWithTag ("player");
		_image = diedImage.GetComponent<Image>();
		_imageRespawn = respawnImage.GetComponent<RawImage> ();

		alphaInterval = 1.0f / timeFadeOut;
		alphaIntervalRespawn = 1.0f / timeFadeOutRespawn;

		InitCanvas ();
	}

	/// <summary>
	/// Inits the canvas.
	/// </summary>
	void InitCanvas()
	{
		// resumeButton.SetActive (false);

		diedImage.SetActive (false);
		Color _alpha = _image.color;
		_alpha.a = 0f;
		_image.color = _alpha;

		respawnImage.SetActive (false);
		Color _alphaReset = _imageRespawn.color;
		_alphaReset.a = 1f;
		_imageRespawn.color = _alphaReset;
	}

	/// <summary>
	/// Public function for player die. (Called by other death script)
	/// </summary>
	public void PlayerDie()
	{
		StartCoroutine(PlayerDieAndRespawn());
	}

	/// <summary>
	/// Public function for player respawn. (Called by CheckPointManager)
	/// </summary>
	/// <param name="_player">Player Transform</param>
	public void PlayerRespawn(Transform _player)
	{
		// DestroyImmediate (player);
		player = _player.gameObject;

		// InitCanvas ();

		// GetComponent<DeathInHeight> ().PlayerInit (player);

		// StartCoroutine (PlayerResapwnEffect ());
	}

	/// <summary>
	/// Players the die and respawn, with fade out, stay and fade in.
	/// </summary>
	private IEnumerator PlayerDieAndRespawn()
	{
		igmm.pauseAllowed = false;

		// Disable player control
		player.GetComponent<EnableController> ().DisableAll ();
		isDead = true;

		// Fade out
		diedImage.SetActive(true);

		while(_image.color.a < 1.0f)
		{
			Color _alpha = _image.color;
			_alpha.a += alphaInterval * Time.deltaTime;
			_image.color = _alpha;

			yield return null;
		}

		// Stay
		yield return new WaitForSeconds (timeStay);
		player.GetComponent<RopePlayer>().DestroyRope();
		cpm.RespawnInTheCheckpoint();
		isDead = false;
		foreach (_Death death in GetComponents<_Death>())
			death.ResetIsDead ();

		// Fade in
		diedImage.SetActive (false);
		respawnImage.SetActive (true);

		while (_imageRespawn.color.a > 0.0f) 
		{
			Color _alpha = _imageRespawn.color;
			_alpha.a -= alphaIntervalRespawn * Time.deltaTime;
			_imageRespawn.color = _alpha;

			yield return null;
		}

		// Reset Canvas
		InitCanvas ();

		igmm.pauseAllowed = true;
	}
}
