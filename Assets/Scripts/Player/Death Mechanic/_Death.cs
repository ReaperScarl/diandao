﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Basic death class.
/// This class should be inheritted by any other classes with death, except TestDeath.
/// It realizes a basic function for player init, which can be overrided in subclass which needs more functionality.
/// </summary>
public class _Death : MonoBehaviour
{
	// ------	Public Variables	------
	public GameObject player;

	// ------	Protected Variables	------
	protected bool isDead;

	protected virtual void Start()
	{
		ResetIsDead ();
	}

	public void ResetIsDead()
	{
		isDead = false;
	}
}

