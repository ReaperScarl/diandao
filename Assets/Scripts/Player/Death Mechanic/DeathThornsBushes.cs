﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is responsible for death when player touches a thorns bush.
/// This script should be attached to the throns bushes object.
/// </summary>
public class DeathThornsBushes : MonoBehaviour {

	// ------	Public Variables	------
	[Header("Death Manager")]
	public DeathMechanic deathManager;

	[Header("Death Clip by Xia")]
	public AudioClip deathClip;

	[Header("Touch Clip")]
	public AudioClip touchClip;

	// Use this for initialization
	void Start () 
	{
		
	}

	/// <summary>
	/// Trigger when player touches the thorns bush
	/// </summary>
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "player" && !deathManager.isDead) {
			deathManager.PlayerDie ();
			SoundEffectManager.Instance.PlayClip (deathClip);
			SoundEffectManager.Instance.PlayClip (touchClip);
		}
	}
}
