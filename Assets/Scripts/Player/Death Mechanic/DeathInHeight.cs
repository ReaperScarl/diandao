﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is responsible for death passing a defined height, inheritted from _Death.
/// This script should be attached to the death canvas.
/// </summary>
public class DeathInHeight : _Death {

	// ------	Public Variable	------
	[Header("Maximum height for death")]
	public float deathHeight;

	[Header("Death sound effect")]
	public AudioClip deathClip;

	// ------	Required Component	------
	private CapsuleCollider2D cc2d;

	// ------	Private Variable	------

	// Use this for initialization
	protected override void Start () 
	{
		base.Start ();
		cc2d = player.GetComponent<CapsuleCollider2D>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		// Check isDead & height
		if(!isDead && cc2d.bounds.center.y > deathHeight)
		{
			GetComponent<DeathMechanic>().PlayerDie();
			SoundEffectManager.Instance.PlayClip (deathClip);
			isDead = true;
		}			
	}


}
