﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the script that manages the rock interaction
/// </summary>
[RequireComponent(typeof(CircleCollider2D),typeof(CircleCollider2D), typeof(Rigidbody2D))]
public class RockController : MonoBehaviour {

    [Header ("Clip when the rock touches the ground")]
    public AudioClip clipWhenTouchesTheGround;

    // we'll assume that the first collider is the not trigger one and the trigger one the second
    CircleCollider2D[] colliders;

    private Rigidbody2D rb2D;

    private void Start()
    {
       colliders =GetComponents<CircleCollider2D>();
        rb2D = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "player")
        {
            collision.GetComponent<PlayerController>().RockInRange(true, this.gameObject);
            //Debug.Log("is inside");
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "player")
        {
            collision.GetComponent<PlayerController>().RockNotInRange();
            //Debug.Log("is outside");
        }
    }

    /// <summary>
    /// Method called by the hole controller when a rock in put in a hole
    /// </summary>
    public void DeactivateTheRock()
    {
        if(!rb2D) // in case it is activated before the start
            rb2D = GetComponent<Rigidbody2D>();
        rb2D.bodyType = RigidbodyType2D.Static;
        if(colliders==null) // in case it is activated before the start
            colliders = GetComponents<CircleCollider2D>();
        foreach (Collider2D coll in colliders)
            coll.enabled = false;
    }

    /// <summary>
    /// Method called by the hole controller when a rock has to be free from a hole
    /// </summary>
    public void ReactivateTheRock()
    {
        rb2D.bodyType = RigidbodyType2D.Dynamic;
        foreach (Collider2D coll in colliders)
            coll.enabled = true;
    }


    /// <summary>
    /// called when the object is destroyed
    /// </summary>
    private void OnDestroy()
    {
        if (transform.parent)
        {
            ResetGrabbableObject reset = transform.parent.GetComponent<ResetGrabbableObject>();
            if (reset)
                reset.isTaken = true;
        }      
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag != "player" && clipWhenTouchesTheGround)
            SoundEffectManager.Instance.PlayClip(clipWhenTouchesTheGround);
    }

}
