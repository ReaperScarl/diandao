﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This class will be responsable for the enables and disables when there are multiple activations
/// </summary>
[RequireComponent(typeof(PlayerController),typeof(CharacterMovement),typeof(CharacterJump))]
[RequireComponent(typeof(RopePlayer), typeof(PushObject))]
public class EnableController : MonoBehaviour {

    // All the variables to enable, disable
    private PlayerController pC;
    private CharacterMovement cM;
    private CharacterJump cJ;
    private RopePlayer rP;
    private PushObject pO;

	void Start () {
        pC=GetComponent<PlayerController>();
        cM= GetComponent<CharacterMovement>();
        cJ= GetComponent<CharacterJump>();
        rP= GetComponent<RopePlayer>();
        pO= GetComponent<PushObject>();
    }

    /// <summary>
    /// Enables every interaction for the player
    /// </summary>
    public void EnableAll()
    {
        pC.dressEnabled = true;
        cM.movementEnabled = true;
        cJ.activated = true;
        rP.enabled = true;
        pO.activated = true;
        pC.isInteractionEnabled = true;
    }

    /// <summary>
    /// Disables every interaction for the player
    /// </summary>
    public void DisableAll()
    {
        pC.dressEnabled = false;
        cM.movementEnabled = false;
        cM.Stop();
        cJ.activated = false;
        rP.enabled = false;
        pO.activated = false;
        pC.isInteractionEnabled = false;
    }

}
