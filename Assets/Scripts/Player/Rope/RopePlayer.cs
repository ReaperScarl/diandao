﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is responsible for rope ability of the player
/// The script should be attached to the player
/// </summary>
[RequireComponent(typeof(Rigidbody2D), typeof(CapsuleCollider2D))]
[RequireComponent(typeof(CharacterMovement), typeof(CharacterJump), typeof(PushObject))]	// For disable these abilities
[RequireComponent(typeof(RopeNode))]
[RequireComponent(typeof(Animator))]
public class RopePlayer : MonoBehaviour {

	// ------	Rope Player Public Variables	------
	[Header("Length of the rope")]
	[Range(1.0f, 50.0f)]
	public float ropeLength = 8f;

	[Header("Speed of the rope when going")]
	[Range(1.0f, 30.0f)]
	public float speedGoing = 10f;

	[Header("Speed of the rope when backing")]
	[Range(1.0f, 50.0f)]
	public float speedBacking = 20f;

	[Header("Speed of the player when climbing the rope")]
	[Range(1.0f, 20.0f)]
	public float speedClimbing = 10f;

	[Header("Force of the player when pulling the captured object")]
	[Range(1.0f, 500.0f)]
	public float pullForce = 100f;

	[Header("Distance of the node")]
	[Range(0.1f, 1.0f)]
	public float nodeDistance = 0.2f;

	[Header("Hook prefab")]
	public GameObject hookPrefab;

	[Header("Node prefab")]
	public GameObject nodePrefab;

	[Header("Rope launch clip")]
	public AudioClip launchClip;

	[Header("Rope hit clip")]
	public AudioClip hitClip;

	// ------	Shared Variable
	public PlayerState state;
	public bool activated = true;

	// ------	Required Component	------
	private Rigidbody2D rb2d;
	private CapsuleCollider2D cc2d;
	private CharacterMovement cm;
	private CharacterJump cj;
	private PushObject cp;
	private RopeNode playerNode;
	private Animator animator;
	private GravityValues gv;
	private PlayerController pc;

	// ------	Private Variables	------
	private GameObject hook;
	private bool lastDressEnabled;
	private PlayerState lastState;

	// ------	Enumerator Player State	------
	public enum PlayerState
	{
		Idle,
		RopeMoving,
		Captured, 
		CapturedGrounded,
		CapturedRoofed
	}

	/// <summary>
	/// Raises the disable event.
	/// </summary>
	void OnDisable()
	{
		rb2d.freezeRotation = true;
		rb2d.bodyType = RigidbodyType2D.Dynamic;

		DestroyRope ();
		state = PlayerState.Idle;
        if (animator.isActiveAndEnabled)
        {
            animator.SetBool("isIdleClimbing", false);
            animator.SetBool("isClimbing", false);
        }
		
	}

	/// Public function to change player state.
	/// If state changes, some of the configuration will also change
	public void ChangeState(PlayerState _state)
	{
		lastState = state;
		state = _state;
		switch(state)
		{
			case PlayerState.Idle:
				cm.movementEnabled = true;
				if (rb2d.gravityScale == gv.oneRock || rb2d.gravityScale == gv.twoRocks)
					cj.activated = true;
				else
					cj.activated = false;
				cp.activated = true;
				pc.dressEnabled = lastDressEnabled;
				transform.rotation = rb2d.gravityScale > 0? Quaternion.Euler(0, 0, 0): Quaternion.Euler(0, 0, -180);
				rb2d.freezeRotation = true;
				rb2d.bodyType = RigidbodyType2D.Dynamic;
				animator.SetBool ("isIdleClimbing", false);
				animator.SetBool ("isClimbing", false);
				break;
			case PlayerState.RopeMoving:
				rb2d.velocity = Vector2.zero;
				cm.movementEnabled = false;
				cj.activated = false;
				cp.activated = false;
				if (lastState == PlayerState.Idle) {
					lastDressEnabled = pc.dressEnabled;
				}
				pc.dressEnabled = false;
				rb2d.freezeRotation = true;
				if(lastState == PlayerState.Idle)
					rb2d.bodyType = RigidbodyType2D.Kinematic;
				break;
			case PlayerState.Captured:
				SoundEffectManager.Instance.PlayClip (hitClip);
				_state = rb2d.gravityScale > 0? PlayerState.CapturedGrounded: PlayerState.CapturedRoofed;
				ChangeState(_state);
				break;
			case PlayerState.CapturedGrounded:
				cm.movementEnabled = false;
				cj.activated = false;
				cp.activated = false;
				pc.dressEnabled = false;
				rb2d.freezeRotation = true;
				rb2d.bodyType = RigidbodyType2D.Kinematic;
				break;
			case PlayerState.CapturedRoofed:
				cj.activated = false;
				cp.activated = false;
				pc.dressEnabled = lastDressEnabled;
					// rb2d.freezeRotation = false;
				rb2d.bodyType = RigidbodyType2D.Dynamic;
				break;
		}
	}

	/// Use this for initialization
	void Start () 
	{
		rb2d = GetComponent<Rigidbody2D>();
		cc2d = GetComponent<CapsuleCollider2D>();
		cm = GetComponent<CharacterMovement>();
		cj = GetComponent<CharacterJump>();
		cp = GetComponent<PushObject>();
		playerNode = GetComponent<RopeNode>();
		animator = GetComponent<Animator> ();
		gv = GetComponent<GravityValues> ();
		pc = GetComponent<PlayerController> ();

		playerNode.leftNode = null;
		lastDressEnabled = pc.dressEnabled;

		ChangeState(PlayerState.Idle);
		lastState = PlayerState.Idle;
	}

	public void DestroyRope()
	{
		if (!playerNode)
			return;
		GameObject rNode = playerNode.rightNode;
		while (rNode && (rNode.tag == "Node" || rNode.tag == "Hook")) 
		{
			if (rNode.tag == "Hook") 
			{
				Destroy (rNode);
				break;
			}
			GameObject _rNode = rNode.GetComponent<RopeNode> ().rightNode;
			Destroy (rNode);
			rNode = _rNode;
		}
	}
	
	/// Update is called once per frame
	/// In update, state will be checked and the player does specific tasks:
	/// Idle --> Get mouse input and shoot the rope
	/// RopeMoving --> Nothing
	/// RopeCaptured --> Intermediate state, nothing
	/// CapturedGrounded --> Get horizontal input and pull the object using the rope
	///					 --> Get mouse input and get back the rope
	/// CapturedRoofed --> Get vertical input and climb the rope
	///				   --> Get mouse input and get back the rope
	void Update () 
	{
		if(!activated)
			return;
		switch(state)
		{
			case PlayerState.Idle:
				// Check grounded first and get input from mouse
				if(cj.isGrounded && Input.GetMouseButtonDown(0))
				{
					// Check mouse position 
					// 1. Rope direction should be ahead of the player
					Vector2 end = Camera.main.ScreenToWorldPoint(Input.mousePosition);
//					if((end.x - cc2d.bounds.center.x + cc2d.bounds.size.x * 0.5f * isFaceRightFloat) 
//						* isFaceRightFloat < 0)
//						break;
					bool isFaceRight = cm.GetFaceDirection();
					if ((end.x < cc2d.bounds.center.x && isFaceRight) ||
					   end.x > cc2d.bounds.center.x && !isFaceRight) 
					{
						cm.Flip ();
						isFaceRight = !isFaceRight;
					}

					// 2. Mouse inside the player should be forbidden
					bool isInside = false;
					Collider2D[] collider2Ds = Physics2D.OverlapPointAll(end);
					foreach(Collider2D collider2D in collider2Ds)
						if(collider2D.gameObject.tag == "player")
							isInside = true;
					if(isInside)
						break;

					// Play sound effect
					SoundEffectManager.Instance.PlayClip (launchClip);

					// Initial the hook	
					float isFaceRightFloat = isFaceRight ? 1 : -1;
					Vector2 start;
					if(rb2d.gravityScale > 0 || Mathf.Abs(end.x - cc2d.bounds.center.x) > 2 * cc2d.bounds.size.x)
						start = cc2d.bounds.center + new Vector3(isFaceRightFloat * cc2d.bounds.size.x * 0.6f, 
						cc2d.bounds.size.y * 0.2f * Mathf.Sign(rb2d.gravityScale), 0);
					else
						start = cc2d.bounds.center + transform.up * cc2d.bounds.size.y * 0.65f;
						
					Vector2 direction = (end - start).normalized;
					hook = Instantiate(hookPrefab, start, Quaternion.identity);
					hook.GetComponent<RopeHook>().InitHook(
						gameObject, start, direction, ropeLength, speedGoing, speedBacking, nodeDistance);
					hook.GetComponent<RopeNode>().leftNode = transform.gameObject;
					playerNode.rightNode = hook;

					
					ChangeState(PlayerState.RopeMoving);
				}
				break;
			case PlayerState.RopeMoving:
				if(Input.GetMouseButtonUp(0))
					hook.GetComponent<RopeHook>().state = RopeHook.HookState.Backing;
				break;
			case PlayerState.Captured:
				break;
			case PlayerState.CapturedGrounded:
				GameObject handle = hook.GetComponent<RopeNode> ().rightNode;

				// Get mouse input and get back the rope
				if(Input.GetMouseButtonUp(0))
				{
					hook.GetComponent<RopeHook>().state = RopeHook.HookState.Backing;
					ChangeState(PlayerState.RopeMoving);
					if(handle)
						handle.transform.GetComponent<MovablePlatformControl>().ChangeState(MovablePlatformControl.State.Idle);

					return;
				}

				// Check capture an unfixed handle
				if(handle.tag == "UnfixedHandle")
				{
					// Get horizontal input and pull the object
					if(Input.GetAxis("Horizontal") * (cc2d.bounds.center.x - 
					hook.GetComponent<CircleCollider2D>().bounds.center.x) > 0)
					{
						// Change platform state
						if(Input.GetAxis("Horizontal") > 0)
							handle.transform.GetComponent<MovablePlatformControl>().ChangeState(MovablePlatformControl.State.PullRight);
						else if(Input.GetAxis("Horizontal") < 0)
							handle.transform.GetComponent<MovablePlatformControl>().ChangeState(MovablePlatformControl.State.PullLeft);
					
						// Shorten the rope whiling pulling the object
						GameObject _rightNode = playerNode.rightNode;	
						if((_rightNode.transform.position - cc2d.bounds.center).magnitude 
						< cc2d.bounds.size.x)
						{
							if(_rightNode != hook)
							{
								_rightNode.GetComponent<HingeJoint2D>().connectedBody = null;
								playerNode.rightNode = _rightNode.GetComponent<RopeNode>().rightNode;
								playerNode.rightNode.GetComponent<RopeNode>().leftNode = transform.gameObject;
								playerNode.rightNode.GetComponent<HingeJoint2D>().connectedBody = rb2d;
								Destroy(_rightNode);
								_rightNode = playerNode.rightNode;
							}
						}	

						// Apply force to the right node
						_rightNode.GetComponent<HingeJoint2D>().enabled = false;
						Vector2 rNodeForce = (cc2d.bounds.center - 
							_rightNode.transform.position).normalized * pullForce;
						_rightNode.GetComponent<Rigidbody2D>().AddForce(rNodeForce);
					}
					else
					{
						handle.transform.GetComponent<MovablePlatformControl>().ChangeState(MovablePlatformControl.State.Captured);
						playerNode.rightNode.GetComponent<HingeJoint2D>().enabled = true;
					}
				}
				
				break;
			case PlayerState.CapturedRoofed:
				// Simulate air force
				rb2d.velocity *= 0.98f;

				// If the player dresses up, turn back to Idle state
				if(rb2d.gravityScale > 0)
				{
					DestroyImmediate(hook);
					ChangeState(PlayerState.Idle);
					break;
				}

				// If grounded, the player should stand on the ground (not swing with the rope)
				if(cj.isGrounded)
				{
					transform.rotation = Quaternion.Euler(0, 0, 180);
					rb2d.freezeRotation = true;
				}
				else
				{
					rb2d.freezeRotation = false;
				}

				// Get mouse input and get back the rope
				if(Input.GetMouseButtonUp(0))
				{
					hook.GetComponent<RopeHook>().state = RopeHook.HookState.Backing;
					transform.rotation = rb2d.gravityScale > 0? Quaternion.Euler(0, 0, 0): Quaternion.Euler(0, 0, -180);
					rb2d.freezeRotation = true;
					ChangeState (PlayerState.RopeMoving);
				}					
				
				// Get vertical input and climb the rope
				float climbInput = -Input.GetAxis("Vertical");
				if(climbInput > 0 && playerNode.rightNode != hook)	// Climb up
				{
					// Move the player and set the angle pointed to right node
					GameObject _rightNode = playerNode.rightNode;
					_rightNode.GetComponent<HingeJoint2D> ().enabled = false;
					rb2d.position += (Vector2)(_rightNode.transform.position - cc2d.bounds.center).normalized 
						* speedClimbing * Time.deltaTime;
					Vector3 relativePos = playerNode.rightNode.transform.position - cc2d.bounds.center;
					float angle = Mathf.Atan2(relativePos.y, relativePos.x) * Mathf.Rad2Deg;
					if(angle >= 0)
						rb2d.rotation = angle + 90;
					else
						rb2d.rotation = angle - 90;
					_rightNode.GetComponent<HingeJoint2D> ().enabled = true;
					_rightNode.GetComponent<HingeJoint2D>().autoConfigureConnectedAnchor = true;
					

					// Shorten the rope while climbing up the rope
					if((_rightNode.transform.position - cc2d.bounds.center).magnitude 
						< cc2d.bounds.size.y * 0.52)
					{
						if(_rightNode != hook)
						{
							_rightNode.GetComponent<HingeJoint2D>().connectedBody = null;
							playerNode.rightNode = _rightNode.GetComponent<RopeNode>().rightNode;
							playerNode.rightNode.GetComponent<RopeNode>().leftNode = transform.gameObject;
							playerNode.rightNode.GetComponent<HingeJoint2D>().connectedBody = rb2d;
							playerNode.rightNode.GetComponent<HingeJoint2D>().connectedAnchor = 
								new Vector2(0, cc2d.bounds.size.y * 0.5f);

							Destroy(_rightNode);
						}
					}

					animator.SetBool ("isIdleClimbing", false);
					animator.SetBool ("isClimbing", true);
				}
				else if(climbInput < 0 && !cj.isGrounded)	// Climb down
				{
					// Move the player
					GameObject _rightNode = playerNode.rightNode;
					_rightNode.GetComponent<HingeJoint2D>().connectedBody = null;
					rb2d.position -= (Vector2)(_rightNode.transform.position - cc2d.bounds.center).normalized 
						* speedClimbing * Time.deltaTime;
					Vector3 relativePos = playerNode.rightNode.transform.position - cc2d.bounds.center;
					float angle = Mathf.Atan2(relativePos.y, relativePos.x) * Mathf.Rad2Deg;
					if(angle >= 0)
						rb2d.rotation = angle + 90;
					else
						rb2d.rotation = angle - 90;
					_rightNode.GetComponent<HingeJoint2D>().connectedBody = rb2d;

					// Extend the rope while climbing down the rope
					if((cc2d.bounds.center - _rightNode.transform.position).magnitude 
						> cc2d.bounds.size.y * 0.5 + nodeDistance)
					{
						Vector2 createPos = (_rightNode.transform.position - cc2d.bounds.center).normalized;
						createPos *= cc2d.bounds.size.y * 0.52f;
						createPos += (Vector2)cc2d.bounds.center;

						GameObject newNode = Instantiate(nodePrefab, createPos, Quaternion.identity);
						newNode.transform.SetParent(hook.transform);						
						newNode.transform.right = ((Vector2)_rightNode.transform.position - createPos).normalized;
						_rightNode.GetComponent<HingeJoint2D>().connectedBody = newNode.GetComponent<Rigidbody2D>();
						newNode.GetComponent<HingeJoint2D>().enabled = true;
						newNode.GetComponent<HingeJoint2D>().connectedBody = rb2d;
						newNode.GetComponent<HingeJoint2D>().connectedAnchor = 
							new Vector2(0, cc2d.bounds.size.y * 0.5f);
						_rightNode.GetComponent<RopeNode>().leftNode = newNode;
						newNode.GetComponent<RopeNode>().leftNode = transform.gameObject;
						newNode.GetComponent<RopeNode>().rightNode = _rightNode;
						playerNode.rightNode = newNode;
					}

					animator.SetBool ("isIdleClimbing", false);
					animator.SetBool ("isClimbing", true);
				}
				else
				{
					animator.SetBool ("isIdleClimbing", true);
					animator.SetBool ("isClimbing", false);
				}

				break;
		}		
	}
}
