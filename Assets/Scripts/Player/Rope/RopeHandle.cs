﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is responsible for handle.
/// This script should be attached to the handle
/// </summary>
public class RopeHandle : MonoBehaviour {

	// ------	Required Component	------
	private RopeNode handleNode;

	// Use this for initialization
	void Start () {
		handleNode = GetComponent<RopeNode>();
		handleNode.rightNode = null;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
