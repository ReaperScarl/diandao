﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is responsible for rope ability of hook
/// This script should be attached to the handle
/// </summary>
[RequireComponent(typeof(Rigidbody2D), typeof(CircleCollider2D), typeof(HingeJoint2D))]
[RequireComponent(typeof(RopeNode))]
public class RopeHook : MonoBehaviour {

	// ------	Rope Hook Public Variable	------
	[Header("Node Prefab")]
	public GameObject nodePrefab;

	// ------	Shared Variable
	public HookState state;

	// ------	Required Component	------
	private Rigidbody2D rb2d;
	private CircleCollider2D cc2d;
	private HingeJoint2D hinge;
	private RopeNode hookNode;

	// ------	Private Variable	------
	private Vector2 start;
	private Vector2 direction;
	private float maxDistance;
	private float goingSpeed;
	private float backingSpeed;
	private float nodeDistance;


	private GameObject player;
	private GameObject lastNode;

	private float timeCount;
	private float timeToBack;

	// ------	Enumerator Hook State	------
	public enum HookState
	{
		Idle,
		Going,
		Backing,
		Captured
	}

	/// Use this for initialization
	void Start () 
	{
		rb2d = GetComponent<Rigidbody2D>();
		cc2d = GetComponent<CircleCollider2D>();
		hinge = GetComponent<HingeJoint2D>();
		hookNode = GetComponent<RopeNode>();

		lastNode = transform.gameObject;
		state = HookState.Idle;
		hinge.enabled = false;

		timeToBack = Time.deltaTime / backingSpeed;
		timeCount = Time.time;
	}

	/// Set initial viriables
	public void InitHook(GameObject _player, Vector2 _start, Vector2 _direction, float _maxDistance, float _gSpeed, float _bSpeed, float _nodeDistance)
	{
		player = _player;
		start = _start;
		direction = _direction;
		maxDistance = _maxDistance;
		goingSpeed = _gSpeed;
		backingSpeed = _bSpeed;
		nodeDistance = _nodeDistance;
	}
	
	/// Update is called once per frame
	/// State will be checked and hook does the specific tasks:
	/// Idle --> Intermediate state, and changed to Going
	/// Going --> The hook moves to target position and creates the rope node
	///		  --> If the hook reaches rope maximum length or hits something not a handle, it should back
	///		  --> If the hook hits a handle, it should capture the handle
	/// Captured --> Nothing
	/// Backing --> The hook backs to player and deletes the rope node
	void Update () 
	{
		switch(state)
		{
			case HookState.Idle:			
				state = HookState.Going;
				break;
			case HookState.Going:
				// Check whether the hook reaches maximum length
				if(((Vector2)transform.position - start).magnitude > maxDistance)
				{
					lastNode.GetComponent<RopeNode>().leftNode = player;
					lastNode.GetComponent<HingeJoint2D>().connectedBody = player.GetComponent<Rigidbody2D>();
					lastNode.GetComponent<HingeJoint2D>().enabled = true;
					state = HookState.Backing;
					break;
				}
				transform.position += (Vector3)direction * goingSpeed * Time.deltaTime;
				if(((Vector2)lastNode.transform.position - start).magnitude > nodeDistance)
					CreateNode();
				break;
			case HookState.Captured:	
				break;
			case HookState.Backing:
				if(Time.time - timeCount > timeToBack)
				{
					timeCount = Time.time;
					HookBack();
				}	
				break;
		}
	}

	/// Create a new node
	void CreateNode()
	{
		Vector2 createPos = start - (Vector2)lastNode.transform.position;
		createPos.Normalize();
		createPos *= nodeDistance;
		createPos += (Vector2)lastNode.transform.position;

		GameObject newNode = Instantiate(nodePrefab, createPos, Quaternion.identity);
		newNode.transform.SetParent(transform);
		newNode.transform.right = ((Vector2) lastNode.transform.position - createPos).normalized;
		lastNode.GetComponent<HingeJoint2D>().connectedBody = newNode.GetComponent<Rigidbody2D>();
		lastNode.GetComponent<HingeJoint2D>().enabled = true;
		newNode.GetComponent<RopeNode>().rightNode = lastNode;
		newNode.GetComponent<RopeNode>().leftNode = player;
		player.GetComponent<RopeNode>().rightNode = newNode;

		if(lastNode != transform.gameObject)
			lastNode.GetComponent<RopeNode>().leftNode = newNode;
		else
			hookNode.leftNode = newNode;

		lastNode = newNode;
	}

	/// Hook back
	void HookBack()
	{
		// Remove the connection to the captured object, if captured
		if(hookNode.rightNode != null)
		{
			hookNode.rightNode.GetComponent<HingeJoint2D>().connectedBody = null;
			hookNode.rightNode.GetComponent<HingeJoint2D> ().enabled = false;

			if (hookNode.rightNode.tag == "UnfixedHandle")
				hookNode.rightNode.transform.GetComponent<MovablePlatformControl> ().ChangeState (MovablePlatformControl.State.Idle);

			hookNode.rightNode = null;
		}

		GameObject _leftNode = hookNode.leftNode;
		if (_leftNode == player) {
			if (player)
				player.GetComponent<RopePlayer> ().ChangeState (RopePlayer.PlayerState.Idle);
			DestroyImmediate (transform.gameObject);
		}
		else
		{
			hookNode.leftNode = _leftNode.GetComponent<RopeNode>().leftNode;
			if (hookNode.leftNode) 
			{
				hookNode.leftNode.GetComponent<RopeNode> ().rightNode = transform.gameObject;
				rb2d.position = _leftNode.transform.position;
				hinge.connectedBody = hookNode.leftNode.GetComponent<Rigidbody2D> ();
			}

			DestroyImmediate(_leftNode);
		}
	}
		
	/// When hook goes and hits a handle
	public void CaptureHandle(GameObject handle)
	{
		if(state == HookState.Going)
		{
			rb2d.velocity = Vector2.zero;

			hookNode.rightNode = handle;
			lastNode.GetComponent<HingeJoint2D>().connectedBody = player.GetComponent<Rigidbody2D>();
			lastNode.GetComponent<HingeJoint2D>().enabled = true;
			lastNode.GetComponent<RopeNode>().leftNode = player;
			handle.GetComponent<RopeNode>().leftNode = transform.gameObject;
			handle.GetComponent<HingeJoint2D>().enabled = true;
			handle.GetComponent<HingeJoint2D>().connectedBody = rb2d;

			if (handle.tag == "UnfixedHandle")
				handle.transform.GetComponent<MovablePlatformControl> ().ChangeState (MovablePlatformControl.State.Captured);

			player.GetComponent<RopePlayer>().ChangeState(RopePlayer.PlayerState.Captured);
			state = HookState.Captured;
		}
	}

	/// The collision enter checks when the hook hits a target
	/// If it's a handle, capture it
	/// If it's the player, ignore it
	/// If it's not, the hook backs
	void OnCollisionEnter2D(Collision2D other)
	{
		if(state == HookState.Going)
		{
			if((other.collider.GetType() == typeof(CircleCollider2D) &&  (other.gameObject.tag == "FixedHandle" || other.gameObject.tag == "UnfixedHandle"))
				|| other.gameObject.tag == "Dress")
			{
				// transform.position = other.gameObject.transform.position;
				rb2d.velocity = Vector2.zero;

				player.GetComponent<RopePlayer>().ChangeState(RopePlayer.PlayerState.Captured);

				hookNode.rightNode = other.gameObject;

				lastNode.GetComponent<HingeJoint2D>().connectedBody = player.GetComponent<Rigidbody2D>();
				lastNode.GetComponent<HingeJoint2D>().enabled = true;
				lastNode.GetComponent<RopeNode>().leftNode = player;
				other.gameObject.GetComponent<RopeNode>().leftNode = transform.gameObject;
				other.gameObject.GetComponent<HingeJoint2D>().enabled = true;
				other.gameObject.GetComponent<HingeJoint2D>().connectedBody = rb2d;

				state = HookState.Captured;
			}
			else if(other.gameObject.tag == "player")
			{
				return;
			}
			else
			{
				state = HookState.Backing;
				lastNode.GetComponent<RopeNode>().leftNode = player;
				lastNode.GetComponent<HingeJoint2D>().connectedBody = player.GetComponent<Rigidbody2D>();
				lastNode.GetComponent<HingeJoint2D>().enabled = true;
			}
		}
	}
}
