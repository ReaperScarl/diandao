﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is responsible for rope ability of the node.
/// This script should be attached to each node of the rope
/// 	including: player, node, hook
/// </summary>
public class RopeNode : MonoBehaviour {

	// ------	Public Viriable	------
	[HideInInspector]
	public GameObject leftNode;
	[HideInInspector]
	public GameObject rightNode;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
