﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that controls the cam following enable and disable
/// </summary>
public class ControllingCamFoll : MonoBehaviour {


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag=="player") //Checks if the Player is inside the trigger
        {
            Camera.main.GetComponent<CameraFollowing>().enabled = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "player") //Checks if the Player is inside the trigger
        {
            Camera.main.GetComponent<CameraFollowing>().enabled = true;
        }
    }
}
