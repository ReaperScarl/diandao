﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// This is the class that manages the in game menu.
/// This class has to: 
///  - Enable the pause Menu when called the key for the pause menu
///     - Also pause the game until the game is resumed
///  - Disable the pause menu and enable the option menu when pushed the option button
///  - Disable the pause menu and resume the game when pushed the resume button.
///  - Restart the level when pushed restart level button
///  - Restart from the last checkpoint when the pressed the restart from last checkpoint
/// </summary>
public class InGameMenuManager : MonoBehaviour {

    [Header("        Internal elements")]
    [Space(10)]
    [Header("The canvas of the pause menu")]
    public GameObject canvasPauseMenu;

    [Header("The button for the restart from a checkpoint in the pause menu")]
    public Button checkpointButton;

    [Header("Should the restart from a checkpoint be enabled")]
    public bool isCheckpointEnabled;   

    [Header("The canvas of the options menu")]
    public GameObject canvasOptionsMenu;

    [Header("ImageToFadeInOut")]
    public RawImage fadeImage;


    [Header("time to fade while restarting from a checkpoint")]
    [Range(0,5)]
    public float timeToFade=0.3f;

    [Header("time to fade while starting a scene")]
    [Range(0, 5)]
    public float timeToStartingFade=1;


    [Space(10)]
    [Header("The key used to pause the game")]
    public KeyCode pauseCode = KeyCode.Escape;

    [Header("Is the pause allowed")]
    public bool pauseAllowed = true;

    [Space(10)]
    [Header("       Elements to setup")]
    [Space(10)]
    [Header("The manager of the Checkpoints")]
    public CheckPointManager cpM;

    [Header("The index of the scene to restart (in the build setting)")]
    [Range(0,20)]
    public int sceneLevelToRestart=0;

    private bool pauseMenuCalled = false;
    private bool isOptionActive = false;
    public RopePlayer playerRope;
    private bool isRopeEnabled;

	void Start () {
        // just to be sure
        canvasPauseMenu.SetActive(false);
        canvasOptionsMenu.SetActive(false);
        if (!isCheckpointEnabled)
            checkpointButton.interactable = false;

        // the fadein/out initialized
        Color alpha = fadeImage.color;
        alpha.a = 0;
        fadeImage.color = alpha;
        fadeImage.enabled = false;
        Fade(timeToStartingFade);
        playerRope = GameObject.FindGameObjectWithTag("player").GetComponent<RopePlayer>();
    }
	
	void Update () {
        if (pauseAllowed && Input.GetKeyDown(pauseCode))
        {
            if(!pauseMenuCalled) // pause menu called
            {
                if (playerRope)
                {
                    playerRope.activated = false;
                }
               
                pauseMenuCalled = true;
                canvasPauseMenu.SetActive(true);
                Time.timeScale = 0;
            }
            else // Resume menu called
            {
                if (isOptionActive) //option menu called
                    BackToPauseMenu();
                else
                {
                    if (playerRope)
                        playerRope.activated = true;
                    Resume();
                }     
            }
        }
	}

    /// <summary>
    /// Method called by the button resume and also when using the key to pause while in pause menu
    /// </summary>
    public void Resume()
    {
        pauseMenuCalled = false;
        canvasPauseMenu.SetActive(false);
        Time.timeScale = 1;
    }

    /// <summary>
    /// Method called in the pause menu to restart from the checkpoint
    /// </summary>
    public void RestartFromCheckpoint()
    {
        fadeImage.enabled = true;
        if(playerRope)
            StartCoroutine(FadeOutFadeIn(playerRope.gameObject));
        else
            StartCoroutine(FadeOutFadeIn(GameObject.FindGameObjectWithTag("player")));
        Resume();
    }

    /// <summary>
    /// Method called by a button in the pause menu to restart the level
    /// if the sceneLevel to restart is not set, than the default scene to restart is the same
    /// </summary>
    public void RestartThisLevel()
    {
        Resume();
        if(playerRope)
        playerRope.GetComponent<EnableController>().DisableAll();
        else
            GameObject.FindGameObjectWithTag("player").GetComponent<EnableController>().DisableAll();
        if (sceneLevelToRestart == 0)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        else
            SceneManager.LoadScene(sceneLevelToRestart);
    }

    /// <summary>
    /// Method called by the the option button in the pause menu to call the options menu
    /// </summary>
    public void OptionMenu()
    {
        canvasPauseMenu.SetActive(false);
        canvasOptionsMenu.SetActive(true);
        isOptionActive = true;
    }

    /// <summary>
    /// Method called by the "main menu" button in the pause menu to go back in the main menu
    /// </summary>
    public void BackToMainMenu()
    {
        Resume();
        SceneManager.LoadScene(0);
    }

    /// <summary>
    /// Method called by the "back" button in the options menu to go back in the pause menu
    /// </summary>
    public void BackToPauseMenu()
    {
        canvasPauseMenu.SetActive(true);
        canvasOptionsMenu.SetActive(false);
        isOptionActive = false;
    }

    /// <summary>
    /// Fade in and fade out of an image
    /// </summary>
    /// <returns></returns>
    private IEnumerator FadeOutFadeIn(GameObject player)
    {
        player.GetComponent<EnableController>().DisableAll();
        Color alpha = fadeImage.color;
        float alphaInterval = (1 / timeToFade);
        while (fadeImage.color.a < 1.0f) //fade out
        {         
            alpha.a += alphaInterval * Time.deltaTime;
           fadeImage.color = alpha;
            yield return null;
        }

        // inside to not let see what happens to the object while respawning
        if(playerRope)
            playerRope.DestroyRope();
       
        cpM.RespawnInTheCheckpoint();

        yield return new WaitForSeconds(timeToFade);

        while (fadeImage.color.a > 0) //fade in
        {
            alpha.a -= alphaInterval * Time.deltaTime;
            fadeImage.color = alpha;
            yield return null;
        }
        fadeImage.enabled = false;
        playerRope = GameObject.FindGameObjectWithTag("player").GetComponent<RopePlayer>();
        //Debug.Log(playerRope);
    }

    /// <summary>
    ///  Called to deactivate the menu and also the player
    /// </summary>
    public void PauseTheMenu()
    {
        pauseAllowed = false;
        if(playerRope)
            playerRope.GetComponent<EnableController>().DisableAll();
        else
        {
            playerRope = GameObject.FindGameObjectWithTag("player").GetComponent<RopePlayer>();
            playerRope.GetComponent<EnableController>().DisableAll();
        }
    }

    /// <summary>
    ///  Called to reactivate the menu and also the player
    /// </summary>
    public void ResumeTheMenu()
    {
        pauseAllowed = true;
        playerRope.GetComponent<EnableController>().EnableAll();

    }

    /// <summary>
    /// Calles the ienumerator
    /// </summary>
    /// <param name="timeToFade"> the time of the fade halved</param>
    public void Fade(float timeToFadetmp)
    {
        StartCoroutine(FadeIn(timeToFadetmp));
    }

    /// <summary>
    /// Makes a fade of the fade image in timeToFade time
    /// </summary>
    /// <param name="timeToFade"> the time of the fade halved</param>
    public IEnumerator FadeIn(float timeToFadetmp)
    {
        fadeImage.enabled = true;
        Color alpha = fadeImage.color;
        alpha.a = 1;
        fadeImage.color = alpha;
        float alphaInterval = (1 / timeToFadetmp);

        while (fadeImage.color.a > 0) //fade in
        {
            alpha.a -= alphaInterval * Time.deltaTime;
            fadeImage.color = alpha;
            yield return null;
        }
        fadeImage.enabled = false;
    }

    /// <summary>
    /// Put the alpha to 1 for have a black screen while changing the scene
    /// </summary>
    public void KeepBlack()
    {
        fadeImage.enabled = true;
        Color alpha = fadeImage.color;
        alpha.a = 1;
        fadeImage.color = alpha;
    }

}
