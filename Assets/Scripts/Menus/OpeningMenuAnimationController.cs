﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpeningMenuAnimationController : MonoBehaviour {

    /// <summary>
    /// This class is used to check if the
    /// Opening is played only once
    /// This object has only one instance
    /// </summary>
    public static OpeningMenuAnimationController Instance;

    [Header("Was the opening already played?")]
    public bool neverPlayed;

    private void Start()
    {
        DontDestroyOnLoad(this);
    }

    void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);
        else
            Instance = this;
    }
}
