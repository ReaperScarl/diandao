﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

/// <summary>
/// This class is used to play
/// the Opening
/// </summary>
public class PlayOpeningAnimation : MonoBehaviour {

    [Header("The 'New Game' button fo the Main Menu to disable")]
    public GameObject newGameToDisable;
    [Header("The 'Resume Game' button fo the Main Menu to disable")]
    public GameObject resumeGameToDisable;
    [Header("The 'Options' button fo the Main Menu to disable")]
    public GameObject optionToDisable;
    [Header("The 'Credits' button fo the Main Menu to disable")]
    public GameObject creditsToDisable;
    [Header("The 'Quit' button fo the Main Menu to disable")]
    public GameObject quitToDisable;
    [Header("The GameObject used to manage the Opening")]
    public GameObject openingController;

    //Animator used for the opening
    private Animator animationOpening;
    
    // Use this for initialization
    void Start()
    {
        animationOpening = GetComponent<Animator>();
        StartCoroutine(PlayOpening());
    }

    /// <summary>
    /// This method avoid the user
    /// to interact with buttons of the main menu
    /// while the Opening is playing
    /// </summary>
    /// <returns></returns>
    public IEnumerator PlayOpening()
    {
        if (openingController.GetComponent<OpeningMenuAnimationController>().neverPlayed)
        {
            openingController.GetComponent<OpeningMenuAnimationController>().neverPlayed = false;

            newGameToDisable.GetComponent<Button>().enabled = false;
            resumeGameToDisable.GetComponent<Button>().enabled = false;
            optionToDisable.GetComponent<Button>().enabled = false;
            creditsToDisable.GetComponent<Button>().enabled = false;
            quitToDisable.GetComponent<Button>().enabled = false;

            while (!animationOpening.GetCurrentAnimatorStateInfo(0).IsName("Ending"))
            {
                yield return null;
            }

            newGameToDisable.GetComponent<Button>().enabled = true;
            resumeGameToDisable.GetComponent<Button>().enabled = true;
            optionToDisable.GetComponent<Button>().enabled = true;
            creditsToDisable.GetComponent<Button>().enabled = true;
            quitToDisable.GetComponent<Button>().enabled = true;
        }
        gameObject.GetComponent<Animator>().enabled = false;
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
    }
}
