﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// This class is responsible for the main menu manager
/// This script should be attached to the object MenuManager
/// </summary>
public class MainMenuManager : MonoBehaviour {

	// ------	Public Variable	------
	[Header("Menu")]
	public GameObject main;
	public GameObject options;
	public GameObject credits;

	public GameObject resumeBtn;

	// ------	Public Enumerator	------
	public enum Menu
	{
		Main,
		Options,
		Credits
	}

	// Use this for initialization
	void Start () 
	{
		int currentSceneIndex = 0;
		if (PlayerPrefs.HasKey ("Current Scene Index"))
			currentSceneIndex = PlayerPrefs.GetInt ("Current Scene Index");

		if (currentSceneIndex > 0)
			resumeBtn.GetComponent<Button> ().interactable = true;
	}

	// Diable all menu
	private void DisableAll()
	{
		main.SetActive(false);
		options.SetActive(false);
		credits.SetActive(false);
	}

	// Select (Enable) menu
	private void SelectMenu(Menu menu)
	{
		DisableAll();
		switch(menu)
		{
			case Menu.Main:
				main.SetActive(true);
				break;
			case Menu.Options:
				options.SetActive(true);
				break;
			case Menu.Credits:
				credits.SetActive(true);
				break;
		}
	}

	// Click button -- New Game
	public void OnClickNewGame()
	{
		SceneManager.LoadScene("Level1A");
	}

	// Click button -- Resume Game
	public void OnClickResumeGame()
	{
		SaveAndLoad.ResumeGame ();
	}

	// Cliick button -- Mechanics Menu
	public void OnClickMechanics()
	{
		SceneManager.LoadScene ("TestMechanicsMenu");
	}

	// Click button -- Options
	public void OnClickOptions()
	{
		SelectMenu(Menu.Options);
	}

	// Click button -- Credits
	public void OnClickCredits()
	{
		SelectMenu(Menu.Credits);
	}

	// Click button -- Quit
	public void OnClickQuit()
	{
		Debug.Log("Game Quit!");
		Application.Quit();
	}

	// Click button -- Back to menu
	public void OnClickBack()
	{
		SelectMenu(Menu.Main);
	}

}
