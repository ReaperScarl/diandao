﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This Class manages the checkpoints distribuited in the level
/// </summary>
public class CheckPointManager : MonoBehaviour {
    
    [Header("THe player object in the level")]
    public Transform player;

    [Header("The camera following used")]
    public CameraFollowing camFoll;

    
    [Header("The checkpoints of the level")]
    public GameObject[] checkpoints;

    [Header("The reset Element for the inventory (the GUI inventory)")]
    private ResetInventory resetForInventory;

    private PlayerController pC;
    // the checkpoint starts always from the checkpoint in position 0
    private int currentCheckpoint = 0;

    protected virtual void Start()
    {
        pC= player.GetComponent<PlayerController>();
        resetForInventory = GameCanvasManager.Instance.inventoryGUI.GetComponent<ResetInventory>();
    }
    /// <summary>
    /// This method is called by the checkpoints when their portals are activated
    /// </summary>
    /// <param name="checkpointValue"> the position of the checkpoint in the order</param>
    public void NewCheckpointPassed(int checkpointValue)
    {
        if (checkpointValue > currentCheckpoint)
        {
            currentCheckpoint = checkpointValue;
            resetForInventory.NewCheckpointReached();
        }         
        //Debug.Log("value: " + currentCheckpoint);
    }

    /// <summary>
    /// Method called by the death mechanic to restart from the checkpoint.
    /// </summary>
    public virtual void RespawnInTheCheckpoint()
    {
        player.gameObject.SetActive(false);
        // Delete the dress if died taking off one
        GameObject dressToDelete = GameObject.Find("Dress(Clone)");

        if (dressToDelete)
            Destroy(dressToDelete);

        // Delete all the rocks that were instantiated in the scene
        GameObject [] rocksToDelete = GameObject.FindGameObjectsWithTag("rock");
        //Debug.Log("rock:" + rockToDelete);
        for(int i=0; i<rocksToDelete.Length; i++) 
        {
            if(!rocksToDelete[i].transform.parent)
                Destroy(rocksToDelete[i]);
        }

        pC.interactionText.text = "";
        player = checkpoints[currentCheckpoint].GetComponent<CheckPointElement>().StartFromCheckPoint(player);
        resetForInventory.Reset();
        player.gameObject.SetActive(true);
        camFoll.enabled = true;
    }
    
}
