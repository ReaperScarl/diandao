﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script used to reset the inventory visible
/// </summary>
public class ResetInventory : _Reset {

    private Sprite[] lastImages;

    private ItemID[] IDs;
    private InventoryGUIManager inventory;
    
    protected override void Start()
    {
        inventory = GetComponent<InventoryGUIManager>();
        StartCoroutine(WaitASecBeforeTakeTheInventory());
    }

    /// <summary>
    /// It waits a second before taking the images needed. In this way, it is sure to have the right info
    /// </summary>
    private IEnumerator WaitASecBeforeTakeTheInventory()
    {
        yield return new WaitForSeconds(1);
        lastImages = inventory.GetImages();
        IDs = inventory.IDs;
    }

    /// <summary>
    /// Method called when reached a new checkpoint to know which elements have to be resetted when respawn is made
    /// </summary>
    public void NewCheckpointReached()
    {
        lastImages = inventory.GetImages();
        IDs = inventory.IDs;
    }

    /// <summary>
    /// Overrided method to reset the Inventory 
    /// </summary>
    public override void Reset()
    {
        inventory.ResetInventory(lastImages, IDs);
    }
}
