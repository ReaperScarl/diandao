﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Reset movable platform.
/// </summary>
public class ResetMovablePlatform : _Reset {

	private float zRotation;

	// Use this for initialization
	protected override void Start () {
		base.Start ();

		zRotation = transform.rotation.eulerAngles.z;
	}
	
	public override void Reset ()
	{
		base.Reset ();

		GetComponent<Rigidbody2D> ().MoveRotation (zRotation);
	}
}
