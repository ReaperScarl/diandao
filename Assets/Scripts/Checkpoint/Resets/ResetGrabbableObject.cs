﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This Script will manage the reset of a grabbable object
/// </summary>
public class ResetGrabbableObject : _Reset {

    [Header("Grabbable object to respawn when needed")]
    public GameObject grabbableObject;


    public bool isTaken=false;
    // Use this for initialization
    protected override void Start()
    {
        base.Start();
    }

    /// <summary>
    /// Override of the reset method
    ///  -if the element is taken, it has to respawn it
    /// </summary>
    public override void Reset()
    {
        if (isTaken)
        {
            Instantiate(grabbableObject, transform.position, transform.rotation, transform);
            isTaken = false;
        }
    }


}
