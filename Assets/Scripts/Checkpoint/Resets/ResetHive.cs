﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script manages the reset of the hive
/// </summary>
public class ResetHive : _Reset {

    private float zStartingRotation;

    protected override void Start()
    {
        base.Start();
        zStartingRotation = transform.rotation.eulerAngles.z;
    }

    /// <summary>
    /// Overrided method to reset the hive
    /// </summary>
    public override void Reset()
    {
        
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        GetComponent<Rigidbody2D>().angularVelocity = 0;
        base.Reset();
        transform.rotation = Quaternion.Euler(0, 0, zStartingRotation);
        GetComponent<CircleCollider2D>().enabled = true;
        GetComponent<InteractionHive>().isActive = true;

    }
}
