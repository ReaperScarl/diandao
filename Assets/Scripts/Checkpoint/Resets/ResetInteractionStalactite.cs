﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the reset script for the interaction of the stalactite.
/// </summary>
public class ResetInteractionStalactite : _Reset {

    InteractionStalactite stalactite;

	// Use this for initialization
	protected override void Start () {
        base.Start();
        stalactite = GetComponent<InteractionStalactite>();
	}

    /// <summary>
    /// override method. It will call the reset of  the Interaction stalactite
    /// </summary>
    public override void Reset()
    {
        base.Reset();
        stalactite.Reset();
    }

}
