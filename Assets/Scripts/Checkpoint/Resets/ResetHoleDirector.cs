﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the reset script for the hole director
/// This should:
/// - clear the puzzle
/// - make all the holes became empty again.
/// </summary>
public class ResetHoleDirector : _Reset {

    private HoleDirector director;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        director = GetComponent<HoleDirector>();
    }

    /// <summary>
    /// override method of The reset script
    /// this will:
    /// - reset the director counters
    /// - Reset the holes (removing their holes)
    /// </summary>
    public override void Reset()
    {
        base.Reset();
        director.Reset();
        foreach(HoleController hole in director.holes)
            hole.ResetHole();
    }
}
