﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that manages the reset of the Carriage element
/// It is added to the object with the interaction of the carriage
/// </summary>
public class ResetCarriage : _Reset {


    private float startingRotation;
    private Vector3 startingPosition;
    private CarriageInteraction cI;


	protected override void Start () {
        base.Start();
        cI=GetComponent<CarriageInteraction>();

        if (cI)
        {
            startingRotation = cI.carriage.transform.rotation.z;
            startingPosition = cI.carriage.transform.position;
        }
        else
            Debug.Log("Missing CarriageInteraction");
	}

    /// <summary>
    /// Ovverrided method to reset the element
    /// </summary>
    public override void Reset()
    {
        cI.isActive = true;
        cI.GetComponent<CircleCollider2D>().enabled = true;
        Vector3 rot = cI.carriage.transform.rotation.eulerAngles;
        cI.carriage.transform.rotation = Quaternion.Euler(rot.x, rot.y, startingRotation);
        cI.carriage.transform.position = startingPosition;
        cI.carriage.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
    }
}