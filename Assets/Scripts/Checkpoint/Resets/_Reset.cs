﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class responsible for resetting the element that needs to be reset.
/// The basic thing to do is to put the element that is changed and put it in the starting position 
/// </summary>
public class _Reset : MonoBehaviour {

    private Vector3 startingPosition;

	// Use this for initialization
	protected virtual void Start () {
        startingPosition = this.transform.position;
	}
	


    /// <summary>
    /// This is a method that is called by the checkpoint element to reset the element changed
    /// </summary>
	public virtual void Reset()
    {
        transform.position = startingPosition;
    }



}
