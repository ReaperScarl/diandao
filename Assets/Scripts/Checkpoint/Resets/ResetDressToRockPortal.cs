﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script will manager the reset of the player elements to the dress
/// </summary>
public class ResetDressToRockPortal : _Reset {

    [Header("The dress animator controller of Xia")]
    public RuntimeAnimatorController dressController;

    private DressToRockPortal portalToReset;

    protected override void Start()
    {
        base.Start();
        portalToReset = GetComponent<DressToRockPortal>();
    }


    public override void Reset()
    {
        portalToReset.Reset(dressController);
    }

}
