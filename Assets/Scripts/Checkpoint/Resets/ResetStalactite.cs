﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the reset script for the portal of the stalactites and the stalactites that the portal uses
/// </summary>
public class ResetStalactite : _Reset {

    private float[] yStartingPos;
    private PortalOfStalactite pS;

    // Use this for initialization
    protected override void Start () {
        pS = GetComponent<PortalOfStalactite>();

        yStartingPos = new float[pS.stalactites.Length];

        for (int i = 0; i < yStartingPos.Length; i++)
        {
            yStartingPos[i] = pS.stalactites[i].transform.position.y;
        }
    }

    /// <summary>
    /// This method reset the stalactites to their original position and setting
    /// </summary>
    public override void Reset()
    {
        pS.isActive = true;
        for (int i = 0; i < pS.stalactites.Length; i++)
        {
           
            pS.stalactites[i].GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
            pS.stalactites[i].GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            pS.stalactites[i].GetComponent<FallingStalactite>().isActive = false;
            pS.stalactites[i].transform.position = new Vector2(pS.stalactites[i].transform.position.x, yStartingPos[i]);
        }
    }
}
