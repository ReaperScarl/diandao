﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetDeadTree : _Reset {

    private float startingRotation;
    private DeadTreeMovement dTM;

    // Use this for initialization
    protected override void Start () {
        base.Start();
        dTM = GetComponent<DeadTreeMovement>();
        if (dTM)
        {
            startingRotation = dTM.gameObject.transform.rotation.z;
        }
        else
        {
            Debug.Log("Missing DeadTreeMovement");
        }
    }

    /// <summary>
    /// This method reset the dead tree to the starting setting
    /// </summary>
    public override void Reset()
    {
        base.Reset();
        dTM.toActiveEndFall = true;
        dTM.toActiveFall = true;
        Vector3 rot = dTM.gameObject.transform.rotation.eulerAngles;
        dTM.gameObject.transform.rotation = Quaternion.Euler(rot.x, rot.y, startingRotation);
        dTM.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
    }
}
