﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetShelter : _Reset {

    public GameObject imageToReset;

    private Color startingColor;

	// Use this for initialization
	protected override void Start () {
        base.Start();
        startingColor = imageToReset.GetComponent<SpriteRenderer>().material.color;
	}

    public override void Reset()
    {
        base.Reset();
        imageToReset.GetComponent<SpriteRenderer>().material.color = startingColor;
    }
}
