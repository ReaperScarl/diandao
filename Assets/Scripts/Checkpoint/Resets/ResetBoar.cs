﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script manages the reset of the boar
/// </summary>
public class ResetBoar : _Reset {

    private BoarAI boar;

	// Use this for initialization
	protected override void Start () {
        base.Start();
        boar = GetComponent<BoarAI>();
	}

    /// <summary>
    /// Overrided method to reset the boar.
    /// </summary>
    public override void Reset()
    {
        base.Reset();
        boar.Reset();
    }
}
