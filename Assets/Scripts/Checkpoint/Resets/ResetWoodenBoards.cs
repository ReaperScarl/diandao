﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that manages the Reset of the wooden boards
/// </summary>
public class ResetWoodenBoards : _Reset {


    private float[] yStartingPos;
    private PortalOfBoardFalling pBF;

    // Use this for initialization
	protected override void Start () {

        pBF = GetComponent<PortalOfBoardFalling>();

        yStartingPos = new float[pBF.woodenBoard.Length];

        for (int i=0; i<yStartingPos.Length; i++)
        {
            yStartingPos[i] = pBF.woodenBoard[i].transform.position.y;
        }
	}

    /// <summary>
    /// Overrided the reset. This will put the boards in the starting position after the velocity is put to 0
    /// </summary>
    public override void Reset()
    {
        pBF.isActive = true;
        for (int i = 0; i < pBF.woodenBoard.Length; i++)
        {
            pBF.woodenBoard[i].GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            pBF.woodenBoard[i].GetComponent<BoardFallingWithDelay>().isActive = false;
            pBF.woodenBoard[i].gameObject.SetActive(true);
            pBF.woodenBoard[i].transform.position = new Vector2(pBF.woodenBoard[i].transform.position.x, yStartingPos[i]);
        }     
    }
}
