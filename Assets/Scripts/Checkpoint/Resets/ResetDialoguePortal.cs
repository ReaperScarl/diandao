﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the reset of the dialogue portal and its action dialogue
/// </summary>
public class ResetDialoguePortal : _Reset {

    private DialoguePortal portal;
    private DialogueAction action;

    protected override void Start()
    {
        base.Start();
        portal = GetComponent<DialoguePortal>();
        action = GetComponent<DialogueAction>();
    }

    public override void Reset() {

        portal.Reset();
        action.Reset();
    }
}
