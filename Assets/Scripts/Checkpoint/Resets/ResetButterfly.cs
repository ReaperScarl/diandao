﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that manages the reset of the butterfly when the player is going back to a checkpoint
/// It is added to the element with the portal of the butterfly
/// </summary>
public class ResetButterfly : _Reset {

    private PortalForButterfly pB;
    private Vector2 bStartingPosition;

	protected override void Start () {

        pB = GetComponent<PortalForButterfly>();

        if (pB)
        {
            bStartingPosition=pB.butterfly.transform.position;
        }
        else
            Debug.Log("Mssing PortalForButterfly");
	}

    /// <summary>
    /// Overrided method to reset the elements when called
    /// </summary>
    public override void Reset()
    {
        pB.isActive = true;
        pB.butterfly.SetActive(true);
        pB.butterfly.GetComponent<ButterflyMovement>().isActive = false;
        pB.butterfly.GetComponent<Animator>().SetBool("isMoving", false);
        pB.butterfly.transform.position = bStartingPosition;
    }
}
