﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script will manage the reset of the sliding door.
/// </summary>
public class ResetSlidingDoor : _Reset {

    //The door sliding that should be resetted
    private DoorMovementScript doorToReset;
	
    protected override void Start()
    {
        doorToReset = GetComponent<DoorMovementScript>();
        base.Start();
    }

    public override void Reset()
    {
        doorToReset.ResetDoor();
    }
}
