﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The Script Responsible for the reset of the door interaction script
/// </summary>
public class ResetDoor : _Reset {

    private bool isStartingDoorClosed;
    
    private bool isStartingDoorLocked;
    private DoorInteractionScript doorToReset;

    // Use this for initialization
    override protected void Start () {
        doorToReset = GetComponent<DoorInteractionScript>();
        isStartingDoorClosed= doorToReset.GetDoorIsClosed();
        isStartingDoorLocked = doorToReset.isLocked;

	}

    /// <summary>
    /// This calls the reset method in the Door
    /// </summary>
    public override void Reset()
    {
        doorToReset.ResetDoor(isStartingDoorClosed, isStartingDoorLocked);
    }
}
