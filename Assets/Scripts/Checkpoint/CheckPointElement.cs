﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




/// <summary>
/// This is the script that manages which element is the last checkpoint.
///     - This has all the informations that are needed to restart from that point.
///     - Each one is ordered, and the highest checkpoint wins
/// </summary>
public class CheckPointElement : _PortalScript {

    [Header("Number in order of the checkpoint")]
    public int numberOfTheCheckPoint;

    [Header("Controller of all the Checkpoints")]
    public CheckPointManager checkPointManager;

    [Space(10)]
    [Header("     Elements to reset about the player   ")]

    [Header("The player restarts with the dress mechanic? (false is rock)")]
    public bool isDressActive=true;

    [Header("The player restarts with the dress?")]
    public bool hasDress;

    [Header("How many rocks should the player have in this checkpoint?")]
    public int numberOfRocks=0;

    [Header("The player restarts with the rope enabled?")]
    public bool ropeEnabled;

    [Header( "the starting ground to change in the char Move")]
    public bool isStartingGrounded=true;

    [Header("the starting right to change in the char Move")]
    public bool isStartingRight = true;

    [Header("The elements that should to be resetted by this checkpoint before the player")]
    public GameObject[] resetElementsPreRespawn;

    [Header("The elements that should to be resetted by this checkpoint after the player")]
    public GameObject[] resetElementsPostRespawn;

    private float cameraSize;
    private AreaManager area;

    private void Start()
    {
        area = this.GetComponent<AreaManager>();
    }


    /// <summary>
    /// Called to change the element from the script to realize the controller of the checkpoints
    /// </summary>
    protected override void Activate()
    {
        // Put this one as new checkpoint.
        checkPointManager.NewCheckpointPassed(numberOfTheCheckPoint);
        cameraSize=checkPointManager.camFoll.getSizeCamera();
        if(area)
            area.CallTheChange();
    }

    /// <summary>
    /// This method is called by the Checkpoint controller.
    /// This method is used to respawn the player etc with the elements said in the script
    /// this is mostly made by its children
    /// </summary>
    public virtual Transform StartFromCheckPoint(Transform player)
    {
        if(area)
            area.CallTheChange();
        if(cameraSize!=0)
            checkPointManager.camFoll.ChangeScale(cameraSize, 0.2f);

        for (int i = 0; i < resetElementsPreRespawn.Length; i++)
            resetElementsPreRespawn[i].GetComponent<_Reset>().Reset();

        player.position = transform.position;
        CharacterMovement cM = player.GetComponent<CharacterMovement>();

        cM.startingFacing = isStartingRight;
        cM.startingGround = isStartingGrounded;
        PlayerController pc =player.GetComponent<PlayerController>();
        player.GetComponent<EnableController>().EnableAll();
        pc.isDressActive = isDressActive;

        pc.numberOfRocks = numberOfRocks;
        GameCanvasManager.Instance.numberOfRockText.text = "" + numberOfRocks;

        if (isDressActive)
        {
            pc.hasTheDress = hasDress;
            if (hasDress)
                pc.ResetGravity(false, player.GetComponent<GravityValues>().twoRocks);
            else
                pc.ResetGravity(true, player.GetComponent<GravityValues>().zeroRocks);
        }
        else //For the rock
        {
            switch (numberOfRocks)
            {
                case 0:
                    pc.ResetGravity(true, player.GetComponent<GravityValues>().zeroRocks);
                    break;
                case 1:
                    pc.ResetGravity(true, player.GetComponent<GravityValues>().oneRock);
                    break;
                case 2:
                    pc.ResetGravity(false, player.GetComponent<GravityValues>().twoRocks);
                    break;
                case 3:
                    pc.ResetGravity(false, player.GetComponent<GravityValues>().threeRocks);
                    break;
            }
        }
        player.GetComponent<RopePlayer>().activated=ropeEnabled;

        for (int i = 0; i < resetElementsPostRespawn.Length; i++)
            resetElementsPostRespawn[i].GetComponent<_Reset>().Reset();

        return player;
    }
}
