﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manages the checkpoints in level2
/// </summary>
public class CheckPointManagerLvl2 : CheckPointManager {

	[Space(15)]

	[Header("      Elements for level 2")]
	[Space(5)]
	[Header("Puddle to open")]
	public GameObject puddle;
	[Header("Dialog after grabing the key")]
	public GameObject dialogKey;
	[Header("Reset Component")]
	public ResetGrabbableObject resetScript;

	public override void RespawnInTheCheckpoint()
	{
		base.RespawnInTheCheckpoint ();
       /*
		InventoryLvl2 i2 = GameObject.FindGameObjectWithTag ("player").GetComponent<InventoryLvl2> ();
        if (i2&&puddle&& dialogKey&& resetScript)
        {
            i2.puddle = puddle;
            i2.dialogKey = dialogKey;
            i2.resetScript = resetScript;
        }
        else
            Debug.Log("Missing lvl2 parts");
            */
	}
}
