﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is a helper for the button scripts to help them take the glow and not glow sprites instead to write there complicated checks for
/// a part not inherent for the script itself
/// </summary>
public class GlowForButtonHelper : MonoBehaviour {

    [Header("The Glowy Elements")]
    [Header("The sprite when the button is Up")]
    public Sprite glowUpButton;

    [Header("The sprite when the button is Down")]
    public Sprite glowDownButton;

    [Header("The not Glowy Elements")]

    [Header("The sprite when the button is Up")]
    public Sprite nGUpButton;

    [Header("The sprite when the button is Down")]
    public Sprite nGDownButton;

}
