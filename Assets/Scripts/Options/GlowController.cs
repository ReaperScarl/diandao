﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is the controller for the sprites with glow that are indipendent from other scripts
/// </summary>
public class GlowController : MonoBehaviour {

    public Sprite glowySprite;
    public Sprite noGlowySprite;


	// Use this for initialization
	void Start () {
        SpriteRenderer sRenderer = GetComponent<SpriteRenderer>();
        if (!PlayerPrefs.HasKey("glowyThings") || PlayerPrefs.GetInt("glowyThings") == 1) //glowy active
        {
            sRenderer.sprite = glowySprite;
        }
        else
        {
            sRenderer.sprite = noGlowySprite;
        }
	}
}
