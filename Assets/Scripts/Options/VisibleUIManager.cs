﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script should manage the hints texts and Interface Visible
/// </summary>
public class VisibleUIManager : MonoBehaviour {

    [Header("         Elements To set up")]
    public Text[] hintTexts;

    void Start () {
        if (PlayerPrefs.GetInt("visibleInterface") == 0) // part of the interface (inventory etc)
        {
            GameCanvasManager.Instance.inventoryGUI.InventoryNotVisible();
            HintsNotVisible();
        }
        else
        {
            if (PlayerPrefs.GetInt("hintText") == 0) // text elements (interactions, elements etc)
                HintsNotVisible();
        }
    }

    /// <summary>
    /// It won't show the hints
    /// </summary>
    private void HintsNotVisible()
    {
        GameCanvasManager.Instance.interactionText.enabled = false;
        //GameCanvasManager.Instance.elementText.enabled = false;
        for (int i = 0; i < hintTexts.Length; i++)
            hintTexts[i].enabled = false;
    }
}
