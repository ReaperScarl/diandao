﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// The Option Manager
/// </summary>
public class OptionsManager : MonoBehaviour {

    [Header("True, if the scene is the main menu")]
    public bool isMainMenu;
    [Space(15)]

    [Header("       Elements to Insert in main menu")]
    [Space(5)]
    [Header("The slider of the background")]
    public Slider backgroundVolume;
    [Header("The slider of the SoundEffect")]
    public Slider soundEffectVolume;
    [Header("The hint texts toggle")]
    public Toggle hint;
    [Header("The glowy things toggle")]
    public Toggle glowy;
    [Header("The visible interface Toggle")]
    public Toggle visibleInterface;

    [Space(15)]

    [Header("       Elements to Insert in game menu")]
    [Header("The level sound manager for the scene")]
    public LevelSoundManager lvlSoundManager;

    /// <summary>
    /// Called when the Option Menu is enabled
    /// It will set the elements from the player Prefs
    /// </summary>
    private void OnEnable()
    {
        //Debug.Log("Enabled Options");
        backgroundVolume.value = PlayerPrefs.GetFloat("backgroundVolume");
        soundEffectVolume.value = PlayerPrefs.GetFloat("soundEffectVolume");
        if (isMainMenu)
        {
            hint.isOn=(PlayerPrefs.GetInt("hintText")==0)?false:true;
            glowy.isOn = (PlayerPrefs.GetInt("glowyThings") == 0) ? false : true;
            visibleInterface.isOn = (PlayerPrefs.GetInt("visibleInterface") == 0) ? false : true;
        }
    }

    /// <summary>
    /// Called when the background slider is changed
    /// </summary>
    public void OnChangedBackgroundVolume()
    {
        PlayerPrefs.SetFloat("backgroundVolume", backgroundVolume.value);
        if (isMainMenu)
        {
            PlayerPrefs.SetFloat("backgroundVolume", backgroundVolume.value);
            GameObject[] musics = GameObject.FindGameObjectsWithTag("music");
            // Check all the elements in the game that are background music
            for (int i = 0; i < musics.Length; i++)
            {
                if (musics[i])
                {
                    AudioSource[] audios = musics[i].GetComponents<AudioSource>();
                    for (int j = 0; j < audios.Length; j++) // check if there are more audiosources and if there are, change just the ones active
                        if (audios[j].enabled)
                            audios[j].volume = backgroundVolume.value;
                }
            }
        }
        else
            if(lvlSoundManager)
                lvlSoundManager.ChangeVolume(backgroundVolume.value);        
    }

    /// <summary>
    /// Called when the sound effect slider is changed
    /// </summary>
    public void OnChangedSoundEffectdVolume()
    {
        PlayerPrefs.SetFloat("soundEffectVolume", soundEffectVolume.value);
        if (SoundEffectManager.Instance)
            SoundEffectManager.Instance.ChangeVolume(soundEffectVolume.value);
        GameObject[] otherSoundEffects = GameObject.FindGameObjectsWithTag("soundEffect");
        for(int i=0; i<otherSoundEffects.Length; i++)
        {
            AudioSource [] sources= otherSoundEffects[i].GetComponents<AudioSource>();
            foreach(AudioSource source in sources)
            {
                if (source.GetComponent<AdaptiveSoundEffect>() == null)
                    source.volume = soundEffectVolume.value;
                else
                    source.GetComponent<AdaptiveSoundEffect>().ChangedVolume(soundEffectVolume.value);
            }
        }
    }

    /// <summary>
    /// Called when the hint text toggle is changed
    /// </summary>
    public void OnChangedHintText()
    {
        PlayerPrefs.SetInt("hintText", hint.isOn?1:0);
    }

    /// <summary>
    /// Called when the glowy things toggle is changed
    /// </summary>
    public void OnChangedGlowyThings()
    {
        PlayerPrefs.SetInt("glowyThings", glowy.isOn ? 1 : 0);
    }

    /// <summary>
    /// Called when the interface toggle is changed
    /// </summary>
    public void OnChangedInterface()
    {
        PlayerPrefs.SetInt("visibleInterface", visibleInterface.isOn ? 1 : 0);
    }
}
