﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script manages the option elements of default when starting the game
/// This method is inside the Main menu and should be called before the Starting of the music
/// </summary>
public class StartingOptionsScript : MonoBehaviour {

	void Start () {
       // Debug.Log(PlayerPrefs.GetInt("FirstTime"));
        if(PlayerPrefs.GetInt("FirstTime")==0)// first time entering this game
        {
            // The player prefs doesn't save bool, so we use the int
            PlayerPrefs.SetInt("FirstTime", 1);

            PlayerPrefs.SetInt("hintText", 1);
            PlayerPrefs.SetInt("visibleInterface", 1);
            PlayerPrefs.SetInt("glowyThings", 1);

            PlayerPrefs.SetFloat("backgroundVolume", 1);
            PlayerPrefs.SetFloat("soundEffectVolume", 1);
        }
	}
}
