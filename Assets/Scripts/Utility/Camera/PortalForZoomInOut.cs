﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the script portal responsible for the zoom in/out of the camera.
/// When activate, it will zoom out or in the camera with the new cameraSize in timeToScale seconds.
/// When activated, it will become active again after timeToScale Seconds
/// </summary>
public class PortalForZoomInOut : _PortalScript {

    [Header("The element with the camera Following (The camera)")]
    public CameraFollowing cam;

    [Range (3,20)]
    [Header("The new scale the camera")]
    public float cameraSize= 5 ;
    
    [Range(0,10)]
    [Header("The time to scale the camera (from the previous size)")]
    public float timeToScale = 1;

    private bool isActive = true;
    private WaitForSeconds delay;

    private void Start()
    {
        delay = new WaitForSeconds(timeToScale);
    }

    /// <summary>
    /// Overrided method that is called when the portal is passed
    /// </summary>
    protected override void Activate()
    {
        if (isActive)
        {
            if (Mathf.Abs(cam.getSizeCamera() - cameraSize) > 0.01f)
            {
                isActive = false;
                cam.ChangeScale(cameraSize, timeToScale);
                StartCoroutine(WaitDeactivated());
            }           
        }
    }

    /// <summary>
    /// It just waits untile the time is passed and reactives the portal
    /// </summary>
    /// <returns></returns>
    IEnumerator WaitDeactivated()
    {
        yield return delay;
        isActive = true;
    }
}
