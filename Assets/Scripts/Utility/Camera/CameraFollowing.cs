﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// This class is responsible for the movement of the camera, according to the position
/// of the player in the environment
/// </summary>
public class CameraFollowing : MonoBehaviour {
   
    public Renderer target;             //the player
    [Header("The area where the player is located inside the camera")]
    public Rect playerZone;               //where the player moves
    [Space(10)]
    [Header("The position of the camera")]
    public Vector3 smoothPos;           

    [Space(10)]
    [Header("    Elements of the camera to setup")]

    [Range(0,20)]

    [Header("Offset from the edge. This will space the rect from the edges of the camera vertically")]
    public float offsetFromTheHorizontalEdge = 2;
    
    [Header("Height of the player. This will augment the space of the rect")]
    public float heightOfPlayer = 2;

    [Range(0, 20)]
    [Header("Height of jump. This will augment the space of the rect")]
    public float HeightOFPlayerJump = 2;

    [Header("Center of the rect trasform in the witdh")]
    [Range(0,1)]
    public float centerOfthePartsInTheCamera = 1/3;



    [Range(1,9)]
    [Header("How many parts from 1 to 10 of the camera should be used to center the player.")]
    public int partsInTheCamera=4;

    [Header("The starter Rect for the camera")]
    public bool playerGravityUp;


    private float[] OffsetHeights = { 0, 0.3f, 0.5f, 0.7f, 1 };

    // This variable is equal to: W= aspectRatio* StartingSize *2
    private float totalWitdhOfCamera;

    // This variable is equal to: H = StartingSize *2
    //private float totalHeightOfCamera;

    // ---- variables for the changes
    private float oldCameraSize;
    private Rect oldRect;
    private float oldSize;

    private float newCameraSize;
    private Rect newRect;
    private float newSize;
    private int moduleForHeight = 7;

    private bool isChangingTheSmooth=false;

    protected Camera _camera;           //main camera
    protected Vector3 _currentVelocity; //velocity of the camera
    private Rigidbody2D targetRb2d;

    public void Start()
    {
        targetRb2d = target.GetComponent<Rigidbody2D>();
        _currentVelocity = targetRb2d.velocity;
 
        _camera =GetComponent<Camera>();

        if (!_camera.orthographic)
        {
            Debug.LogError("deadzone script require an orthographic camera!");
            Destroy(this);
        }
        // New part
        InializeRect();
        //Now the position of the camera is the already center of the camera itself at the start.
        // The player now should be inside the rect when starting the scene.
        smoothPos = transform.position;
    }

    public void LateUpdate()
    {
        float localX = target.transform.position.x - transform.position.x;
        float localY = target.transform.position.y - transform.position.y;

        if (!isChangingTheSmooth)
        {
            if (localX < playerZone.xMin)
            {
                smoothPos.x += localX - playerZone.xMin;
            }
            else if (localX > playerZone.xMax)
            {
                smoothPos.x += localX - playerZone.xMax;
            }
            if (localY < playerZone.yMin)
            {
                smoothPos.y += localY - playerZone.yMin;
            }
            else if (localY > playerZone.yMax)
            {
                smoothPos.y += localY - playerZone.yMax;
            }
        }
        transform.position = Vector3.SmoothDamp(transform.position, smoothPos, ref _currentVelocity, 0.001f);
    }

    /// <summary>
    /// This method is called by the player controller when there is a change of gravity.
    /// This changes the rect based on the gravity given as input
    /// </summary>
    /// <param name="gravityUp"> is the gravity upwards</param>
    public void ChangeRect(bool gravityUp)
    {
        if (gravityUp != playerGravityUp)
        {
           // Debug.Log("Inside change rect");
            if (gravityUp)
            {
                newRect.y = newSize -  (newRect.height+heightOfPlayer) ;
            }
            else
            {
                newRect.y = -(newSize) + heightOfPlayer;
            }  

            playerGravityUp = gravityUp;
            oldRect = playerZone;
            oldSize = _camera.orthographicSize;

            StartCoroutine( InterpolateRect(0.1f*newSize,false));
        }   
    }

    /// <summary>
    /// Method called by the portal with a change of size for the camera
    /// </summary>
    /// <param name="camSize"> the new size of the camera</param>
    /// <param name="timeToChange"> time for the transition</param>
    public void ChangeScale(float camSize, float timeToChange)
    {
        oldRect = playerZone;
        oldSize = _camera.orthographicSize;
        newSize = camSize;

        totalWitdhOfCamera = _camera.aspect * newSize * 2;

        // for both of the rects
        newRect.height = heightOfPlayer + HeightOFPlayerJump + OffsetHeights[(int)newSize/moduleForHeight]*newSize;
        newRect.width = totalWitdhOfCamera / 10 * partsInTheCamera;
        newRect.x = - totalWitdhOfCamera*centerOfthePartsInTheCamera;

        // Depends on the gravity
        if (playerGravityUp)
            newRect.y = newSize - (newRect.height + offsetFromTheHorizontalEdge);
        else
            newRect.y = -(newSize) + offsetFromTheHorizontalEdge;

        StartCoroutine(InterpolateRect(timeToChange,true));
    }

    /// <summary>
    /// This method is called to get the size of the camera used by the camera following
    /// </summary>
    /// <returns> the size of the camera</returns>
    public float getSizeCamera()
    {
        //Debug.Log(" get size: " + _camera.orthographicSize);
        return _camera.orthographicSize;
    }

    /// <summary>
    /// This method is called by the MoveCameraPortal when the player has to be followed again
    /// </summary>
    /// <param name="time"> the time is seconds to reach the player on the center</param>
    public void ReactivateTheFollowing(float time)
    {
        StartCoroutine( ResetOnThePlayer(time));
    }

    public void ChangeTheHeight(bool isManualChanging, float value, float time)
    {
        oldRect.height = newRect.height;

        if (isManualChanging)
            newRect.height = value;
        else
            newRect.height = heightOfPlayer + HeightOFPlayerJump + OffsetHeights[(int)newSize / moduleForHeight] * newSize;

        if (playerGravityUp)
            newRect.y = newSize - (newRect.height + offsetFromTheHorizontalEdge);
        else
            newRect.y = -(newSize) + offsetFromTheHorizontalEdge;

        StartCoroutine(InterpolateRect(time, false));
    }


    /// <summary>
    /// This IEnumerator will interpolate the rect transform "Player Zone" 
    /// proportionally with the size and the aspect of the Camera.
    /// This is called when there is a difference of scale or a difference in gravity
    /// </summary>
    /// <param name="time"> the time needed to pass from the old rect to the new one</param>
    private IEnumerator InterpolateRect(float time, bool isScaleChanging)
    {
        float currentTime = 0;
        StartCoroutine(PutPlayerToCenter(time));

        while (currentTime<=1)
        {
           // Debug.Log("inside the interpolate rect "+ currentTime );
            currentTime += Time.deltaTime/time;
            if (!_camera)
                _camera = GetComponent<Camera>();
            if (isScaleChanging)
                _camera.orthographicSize= Mathf.Lerp(oldSize, newSize, currentTime);
            playerZone.height = Mathf.Lerp(oldRect.height, newRect.height, currentTime);
            playerZone.width = Mathf.Lerp(oldRect.width, newRect.width, currentTime);
            playerZone.x = Mathf.Lerp(oldRect.x, newRect.x, currentTime);
            playerZone.y = Mathf.Lerp(oldRect.y, newRect.y, currentTime);
            yield return null;
        }
    }


    /// <summary>
    /// This is used to put the player in the center of the rect transform while a change in the rect is made.
    /// This will also disable momentarely the update checks to put the player in the position wanted.
    /// </summary>
    /// <param name="time"> takes the same parameter of the "InterpolateRect" IEnumerator</param>
    private IEnumerator PutPlayerToCenter(float time)
    {
        isChangingTheSmooth = true;
        float currentTime = 0;

        Vector3 pointToMove= new Vector3(0,0,transform.position.z);

        while (currentTime <= 1)
        {
            //Debug.Log("inside the center rect " + currentTime);
            currentTime += Time.deltaTime / time;
           
            if (!_camera)
                _camera = GetComponent<Camera>();

            if (playerGravityUp)
                pointToMove.y = target.transform.position.y   // the center of the player
                               + (-_camera.orthographicSize + offsetFromTheHorizontalEdge);

            else
                pointToMove.y = target.transform.position.y  // The center of the player
                            - (-_camera.orthographicSize + offsetFromTheHorizontalEdge);

            pointToMove.x = target.transform.position.x;

            smoothPos.x = Mathf.Lerp(smoothPos.x, pointToMove.x, currentTime);
            smoothPos.y = Mathf.Lerp(smoothPos.y, pointToMove.y, currentTime);

            yield return null;
        }
        isChangingTheSmooth = false;
       // Debug.Log("point to move: " + pointToMove);
    }

    /// <summary>
    /// This is used to put the player in the center of the rect transform while a change in the rect is made.
    /// This will also disable momentarely the update checks to put the player in the position wanted.
    /// </summary>
    /// <param name="time"> the time to reset</param>
    private IEnumerator ResetOnThePlayer(float time)
    {
        this.enabled = false;
        float currentTime = 0;
        Vector3 startingPosition = transform.position;
        Vector3 pointToMove = new Vector3(0, 0, transform.position.z);

        while (currentTime <= 1)
        {
            //Debug.Log("inside the center rect " + currentTime);
            currentTime += Time.deltaTime / time;
            if (!_camera)
                _camera = GetComponent<Camera>();

            if (playerGravityUp)
                pointToMove.y = target.transform.position.y   // the center of the player
                               + (-_camera.orthographicSize + heightOfPlayer + HeightOFPlayerJump);

            else
                pointToMove.y = target.transform.position.y  // The center of the player
                            - (-_camera.orthographicSize + heightOfPlayer + HeightOFPlayerJump);

            pointToMove.x = target.transform.position.x;

            transform.position = Vector3.Lerp(startingPosition, pointToMove, currentTime);
            yield return null;
            
        }
        smoothPos = transform.position;
        this.enabled = true;
        // Debug.Log("point to move: " + pointToMove);
    }


    //------   elements for the editor showing

    /// <summary>
    /// Inizialize the rect at the start
    /// </summary>
    public void InializeRect()
    {
        //Debug.Log("size: " + _camera.orthographicSize);
        newSize = _camera.orthographicSize;
        oldSize = newSize;
        totalWitdhOfCamera = _camera.aspect * _camera.orthographicSize * 2;

       
        // for both of the rects
        newRect.height = heightOfPlayer + HeightOFPlayerJump + OffsetHeights[(int)newSize / moduleForHeight] * newSize;
        newRect.width = totalWitdhOfCamera / 10 * partsInTheCamera;
        newRect.x = -totalWitdhOfCamera*centerOfthePartsInTheCamera;

        // Depends on the gravity
        if(playerGravityUp)
            newRect.y = newSize - (newRect.height + offsetFromTheHorizontalEdge);
        else
            newRect.y= -(newSize) + offsetFromTheHorizontalEdge;
        playerZone = newRect;
    }


    public bool notShow=true;
}

#if UNITY_EDITOR

[CustomEditor(typeof(CameraFollowing))]
/// <summary>
/// This is the current playerZone. Computational heavy so just activate for test purposes
/// </summary>
public class DeadZonEditor : Editor
{
    CameraFollowing cam;
    private void Awake()
    {
        cam = target as CameraFollowing;
        cam.Start();
    }

    public void OnSceneGUI() 
    {
        if(!cam.notShow)
            cam.InializeRect();
        Vector3[] vert =
        {
            cam.transform.position + new Vector3(cam.playerZone.xMin, cam.playerZone.yMin, 0),
            cam.transform.position + new Vector3(cam.playerZone.xMax, cam.playerZone.yMin, 0),
            cam.transform.position + new Vector3(cam.playerZone.xMax, cam.playerZone.yMax, 0),
            cam.transform.position + new Vector3(cam.playerZone.xMin, cam.playerZone.yMax, 0)
        };

        Color transp = new Color(0, 0, 0, 0);
        
        Handles.DrawSolidRectangleWithOutline(vert, transp, Color.red); 
    } 
}
#endif
