﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Portal that will change the height of the rect transform of the camera
/// </summary>
public class ChangeCameraHeightPortal : _PortalScript {

    [Header("true, if the height below will become the height of the rect transform")]
    public bool isHeightToChangeManually = true;

    [Header("The value of the height of the rect transform")]
    [Range(2,20)]
    public float manualHeight = 3;

    [Header("The time to change the rect transform")]
    public float timeToChange = 1.5f;

    [Header("the camera following of the scene")]
    public CameraFollowing camFoll;
	
	protected override void Activate()
    {
        camFoll.ChangeTheHeight(isHeightToChangeManually, manualHeight,timeToChange);
    }
}
