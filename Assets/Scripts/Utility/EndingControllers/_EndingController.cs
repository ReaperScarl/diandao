﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This is checked out when the PlayVideoScript Finishes
/// </summary>
public abstract class _EndingController : MonoBehaviour {



    /// <summary>
    /// This method is called by the PlayVideoScript.
    /// It is called to do stuff as:
    /// - ReEnable the scripts
    /// - Move and activate Characters
    /// - Move to another scene after the video in finished.
    /// </summary>
    public abstract void VideoFinished();


}
