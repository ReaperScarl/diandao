﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/// <summary>
/// This class will be responsible for the following things:
/// - Move the Player to new position
/// - Make appear the father
/// - Re Enable the scripts
/// </summary>
public class Lvl1AfterRopeCutscene : _EndingController
{
    [Header("Game object of the Father")]
    public GameObject father;

    [Header("The coordinates of the position of the character after the cutscene is finished")]
    public Vector3 playerNewPosition;
    [Header("GameObject of the player")]
    public GameObject player;

    [Header("Object to make disappear")]
    public GameObject[] toMakeDisappear;

    [Header("Object to make appear")]
    public GameObject[] toMakeAppear;

    private EnableController enController;

    // Use this for initialization
    void Start()
    {
        enController = player.GetComponent<EnableController>();
    }

    /// <summary>
    /// Method called to make the changes needed
    /// </summary>
    public override void VideoFinished()
    {
        for (int i = 0; i < toMakeDisappear.Length; i++)
            toMakeDisappear[i].SetActive(false);

        for (int i = 0; i < toMakeAppear.Length; i++)
            toMakeAppear[i].SetActive(true);


        InGameMenuManager gameMenu = GameObject.Find("InGameMenu").GetComponent<InGameMenuManager>();
        if(gameMenu)
        {
            gameMenu.Fade(0.5f);
            gameMenu.Resume();
        }
        father.SetActive(true);
        player.transform.position = playerNewPosition;
       
        enController.EnableAll();
        this.gameObject.SetActive(false);
    }

}

