﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Script used after the puddle cutscene is played
/// </summary>
public class CutsceneOfPuddleEnding : _EndingController {

	private PuddleInteraction doorInfo;

	private void Start()
	{
		doorInfo = GetComponent<PuddleInteraction>();
	}


	public override void VideoFinished()
	{
        InGameMenuManager gMM = GameObject.Find("InGameMenu").GetComponent<InGameMenuManager>();
        gMM.KeepBlack();
		SceneManager.LoadScene(doorInfo.levelToGo);
        gMM.Resume();
    }
}
