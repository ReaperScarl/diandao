﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// This class is responsible to change the level after the cutscene is finished
/// </summary>
public class CutsceneBetweenSceneEnding : _EndingController {

    [Header("The in game menu manager")]
    public InGameMenuManager gMM;

    private DoorLevelInteractionScript doorInfo;

    private void Start()
    {
        doorInfo = GetComponent<DoorLevelInteractionScript>();
    }

    /// <summary>
    /// Overrided method. This will load the new scene.
    /// </summary>
    public override void VideoFinished()
    {
        gMM.KeepBlack();
        SceneManager.LoadScene(doorInfo.levelToGo);
        gMM.Resume();
    }
}
