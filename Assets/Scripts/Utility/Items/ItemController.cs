﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This scripts checks when the item is in range of the player and calls the methods of the player
/// to enable the grabbing
/// </summary>
[RequireComponent(typeof(CircleCollider2D))]
public class ItemController : MonoBehaviour
{

    [Header("The range of grab from the center of the element")]
    [Range(1,20)]
    public float rangeOfGrabbing = 3;

    [Header("Info of the element")]
    public ItemID ID;
    [Header("The description that is seen after \"press x to grab \" " )]
    public string description;
    public GameObject graphicElement;
    [Header("the icon to put in the interface")]
    public Sprite interfaceElement;
    [Space(10)]
    [Header("The clip to play when the element is grabbed")]
    public AudioClip onGrabSoundEffect;

    private ItemInfo info;

    private CircleCollider2D coll;

    // Use this for initialization
    void Start()
    {
        coll = GetComponent<CircleCollider2D>();
        coll.radius = rangeOfGrabbing;
        info = new ItemInfo(ID, description, graphicElement, interfaceElement);
    }


    public ItemInfo GetInfo()
    {
        return info;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Add this element to the player controller so that it can be called
        if (collision.transform.GetComponent<PlayerController>())
            collision.transform.GetComponent<PlayerController>().AddToGrab(this);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //Delete this element from the player controller so that it can't be called outside
        if (collision.transform.GetComponent<PlayerController>())
            collision.transform.GetComponent<PlayerController>().ExitFromGrabRange();
    }

    private void OnDestroy()
    {
        if (onGrabSoundEffect)
            SoundEffectManager.Instance.PlayClip(onGrabSoundEffect);
    }


}
