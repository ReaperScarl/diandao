﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that stores the information about the item
/// </summary>
public class ItemInfo {

    public ItemID ID;
    public string description;
    [Header("prefab that has to be instantiated when the element is thrown/dropped- if possible")]
    public GameObject graphicElement;
    [Header("Sprite to be added in the interface")]
    public Sprite interfaceElement;


    public ItemInfo(ItemID id, string desc, GameObject gElem, Sprite iElem)
    {
        ID = id;
        description = desc;
        graphicElement = gElem;
        interfaceElement = iElem;
    }
}

public enum ItemID
{
    Nothing, // ID never used for real elements
    FatherDocument,
    AlienDocument,
    Rock,
	Key,
    Other,
    

}