﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using UnityEngine.UI;

/// <summary>
/// This script manages the door that moves the player in a different level
/// </summary>
public class DoorLevelInteractionScript : _Interactions {

    [Header("This is the level where the door will lead/load")]
    [Range(0, 50)]
    public int levelToGo;

    public bool isLocked = false;

    [Space (20)]
    [Header("True only if there is a cutscene to play before to go in the next scene")]
    public bool hasCutsceneFirst = false;

    [Header("cutscene that you want to play before pass the scene")]
    public VideoClip cutscene;

    [Header("What audio clip should be played when opened?")]
    public AudioClip doorOpeningClip;

    private RawImage image;
    private VideoPlayer videoP;
    private PlayVideoScript videoS;

    protected override void Start()
    {
        base.Start();
        if (hasCutsceneFirst)
        {
            image = GameCanvasManager.Instance.cutscene.GetComponent<RawImage>();
            videoP = image.GetComponent<VideoPlayer>();
            videoS = GameCanvasManager.Instance.cutscene;
        } 
    }

    /// <summary>
    /// This method overrides the method action.
    /// When it is called, it loads the level scene number "levelToGo"
    /// </summary>
    public override void Action()
    {
        if (isLocked)
        {
            return;
        }

        if (doorOpeningClip)
            SoundEffectManager.Instance.PlayClip(doorOpeningClip);

		if (!hasCutsceneFirst)
        {
			SceneManager.LoadScene (levelToGo);
		}
        else
        {
            if (levelToGo == 0) // to disable the resume game
            {
                PlayerPrefs.SetInt("Current Scene Index", 0);
            }

            image.gameObject.SetActive(true);
           
            videoP.clip = cutscene;
            videoS.StartVideo(this.transform);
            this.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    /// <summary>
    /// This method unlocks the door and the trigger collider of the door
    /// </summary>
    public void Unlock()
    {
        coll.enabled = true;
        isLocked = false;
    }
}
