﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that manages the Door interaction. It's just a test but it shows what it does.
/// To show the functionality, it will open and close a door.
/// </summary>
public class DoorInteractionScript : _Interactions {

    [Header("If true, The door can't be opened")]
    public bool isLocked = false;
    [Space(10)]

    [Header("          Elements to set up")]

    [Space(10)]
    [Header("The gameObject with the sprites opened and closed")]
    public GameObject OpenedSprite;
    public GameObject ClosedSprite;

    [Space(10)]

    [Header("The audio clip when the door is opened")]
    public AudioClip doorOpenedClip;
    [Header("The audio clip when the door is closed")]
    public AudioClip doorClosedClip;
    [Header ("The audio clip when the door is locked")]
    public AudioClip doorLockedClip;



    private BoxCollider2D doorCollider;
    private bool isDoorClosed = true;


    new void Start()
    {
        base.Start();
        doorCollider = GetComponent<BoxCollider2D>();
    }

    /// <summary>
    /// Override of the method action.
    /// It opens and closes a door.
    /// </summary>
    public override void Action()
    {
        if (isLocked)
        {
            if (doorLockedClip)
                SoundEffectManager.Instance.PlayClip(doorLockedClip);
            return;
        }
            
        if (isDoorClosed)
        {
            Open(true);
            isDoorClosed = false;
            if (doorOpenedClip)
                SoundEffectManager.Instance.PlayClip(doorOpenedClip);

        }
        else
        {
            Open(false);
            isDoorClosed = true;
            if (doorClosedClip)
                SoundEffectManager.Instance.PlayClip(doorClosedClip);
        }
    }
    
    /// <summary>
    ///     The Entry will be open or closed
    /// </summary>
    /// <param name="isOpen"> true if the door is to open</param>    
    private void Open(bool isOpen)
    {
        OpenedSprite.SetActive(isOpen);
        ClosedSprite.SetActive(!isOpen);
        doorCollider.enabled = !isOpen;
    }
    
    /// <summary>
    /// Method called by the reset from A Door Reset Script when there is a restart from a checkpoint
    /// </summary>
    /// <param name="isDoorClosed"> true if The door is closed</param>
    /// <param name="isDoorLocked">true if the door is locked</param>
    public void ResetDoor(bool isDoorClosed, bool isDoorLocked)
    {
        Open(!isDoorClosed);
        this.isDoorClosed = isDoorClosed;
        isLocked = isDoorLocked;
    }

    /// <summary>
    /// GetThe variable DoorIsClosed
    /// </summary>
    /// <returns> the doorIsClosed bool variable</returns>
    public bool GetDoorIsClosed()
    {
        return isDoorClosed;
    } 
}
