﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is responsible for the holes in the puzzle
/// It has to : 
/// - as any interactions, give the interaction to drop the rock
/// - check if this is a right hole or not
/// - check when it is filled or not
/// </summary>
public class HoleController : _Interactions {

    [Header("It is a correct hole for the solution?")]
    public bool isRightHole;
    [Header ("the director of the puzzle")]
    public HoleDirector director;
    [Space(5)]
    [Header("    Sound effects")]
    [Space(5)]
    [Header("Sound when the hole is filled")]
    public AudioClip holeFilledClip;
    [Header("Sound when the hole is emptied")]
    public AudioClip holeEmptiedClip;

    private PlayerController player;
    private bool isHoleFilled = false;

    private RockController rockController;

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (!player)
            player = collision.GetComponent<PlayerController>();
        base.OnTriggerEnter2D(collision);
        
    }

    /// <summary>
    /// This method has to call the Drop rock of the player
    /// </summary>
    public override void Action()
    {
        if (!isHoleFilled)
        {
            GameObject rock= player.DropRock();
            if (rock)
                rock = Instantiate(rock, this.transform.position, Quaternion.identity, this.transform);
            else
                return;
            isHoleFilled = true;
            coll.enabled = false;
            if (holeFilledClip)
                SoundEffectManager.Instance.PlayClip(holeFilledClip);

            // Debug.Log("rock " + rock);
            rockController = rock.GetComponent<RockController>();
            rockController.DeactivateTheRock();
            if (director)
                director.AddRock(this.isRightHole);
        }
    }

    /// <summary>
    /// Method that should be called from the reset of the holes
    /// </summary>
    public void ResetHole()
    {
        if (transform.childCount > 0)
            Destroy(transform.GetChild(0).gameObject);
        isHoleFilled = false;
        coll.enabled = true;
    }
    
    /// <summary>
    /// Method called by the Hole director to free the rocks
    /// </summary>
    public void GiveBackTheRock()
    {
        if (transform.childCount > 0)
        {
            if (holeEmptiedClip)
                SoundEffectManager.Instance.PlayClip(holeEmptiedClip);
            rockController.ReactivateTheRock();
            transform.DetachChildren();
            isHoleFilled = false;
            coll.enabled = true;
        }     
    }
}
