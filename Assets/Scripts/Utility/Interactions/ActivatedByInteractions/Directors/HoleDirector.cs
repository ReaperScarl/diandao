﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This is the Director of the holes.
/// It will know which ones are the right ones the number of holes to filled etc
/// </summary>
public class HoleDirector : MonoBehaviour {

    [Header("Number of holes of the puzzle")]
    public int numberOfHoles;

    [Header("The action to do when the puzzle is solved")]
    public _Action actionToDo;

    [Header("The holes for the puzzle")]
    public HoleController[] holes;

    private int numberOfHolesFilled=0;
    private int numberOfHolesFilledCorrectly=0;

    /// <summary>
    /// This method is called by the hole controller to let know to the director that a hole has been filled.
    /// If the number of holes filled or the number of the holes filled correctly is equals to the amount of holes,
    /// it will check if the amount is correct or it has to free some rocks
    /// </summary>
    /// <param name="isRightHole">if the caller hole is a right hole or not</param>    
    public void AddRock(bool isRightHole)
    {
        if (isRightHole)
            numberOfHolesFilledCorrectly++;
        numberOfHolesFilled++;
        if (numberOfHolesFilled == numberOfHoles || numberOfHolesFilledCorrectly == numberOfHoles)
            AllHolesAreFilled();
    }

    /// <summary>
    /// This method is called when the amount of holes said is filled. 
    /// It will do an action when the number is right or
    /// It will free the rocks from the wrong holes
    /// </summary>
    private void AllHolesAreFilled()
    {
        if (numberOfHolesFilledCorrectly == numberOfHoles)
        {
            if (actionToDo) // the action to open the door or whatever
                actionToDo.DoAnAction();
            Debug.Log("Puzzle completed");
        }
        else
        {
            for(int i=0; i<holes.Length; i++)
            {
                if (!holes[i].isRightHole)
                    holes[i].GiveBackTheRock();
            }
            numberOfHolesFilled = numberOfHolesFilledCorrectly;
        }
    }

    /// <summary>
    /// The reset for the director.
    /// It will reset the number of holes filled correctly and not
    /// </summary>
    public void Reset()
    {
        numberOfHolesFilled = 0;
        numberOfHolesFilledCorrectly = 0;
    }
}
