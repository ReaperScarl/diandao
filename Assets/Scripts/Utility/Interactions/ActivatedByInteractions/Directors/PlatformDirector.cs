﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manages a set of platforms.
/// It activates and deactivates the platforms when called
/// </summary>
public class PlatformDirector : MonoBehaviour {

    public Transform[] platforms;

    /// <summary>
    /// This method is called by a button to activate the platforms
    /// </summary>
    public void Activate()
    {
        for(int i=0; i<platforms.Length; i++)
            platforms[i].GetComponent<PlatformMovementController>().StartMoving();
    }


    /// <summary>
    /// This method is called by a button to deactivate the platforms
    /// </summary>
    public void Deactivate()
    {
        for(int i=0; i<platforms.Length; i++)
            platforms[i].GetComponent<PlatformMovementController>().StopMoving();
    }
}
