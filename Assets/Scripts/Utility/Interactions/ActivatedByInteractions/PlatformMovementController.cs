﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class controlls the movement of the single platform
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class PlatformMovementController : MonoBehaviour {

    [Header("The speed of the platform")]
    [Range(0,10)]
    public float speedOfMovement=3;

    [Header ("The distance that will reach the platform")]
    [Range(-15, 30)]
    public float distance = 3;

    [Header("The clip to play when the platform is on")]
    public AudioClip platformActiveClip;

    private bool isMoving;
    private float startingPosition;
    private Rigidbody2D rigidBody;
    private bool goingUp=false;
    private WaitForSeconds delay;
    private AudioSource source;

	void Start () {
        startingPosition = transform.position.y;
        rigidBody = GetComponent<Rigidbody2D>();
        delay = new WaitForSeconds(0.1f);
        source = GetComponent<AudioSource>();
    }
	
    /// <summary>
    /// This method is called by the outside from a button director.
    /// This starts the movement of the platform.
    /// </summary>
    public void StartMoving()
    {
        isMoving = true;
        StartCoroutine(Movement());
        if(platformActiveClip)
            StartCoroutine(StartSound());
    }

    /// <summary>
    /// This method is called by the outside from a button director.
    /// It is used to stop the movement of the platform.
    /// </summary>
    public void StopMoving()
    {
        isMoving = false;
        rigidBody.velocity = Vector3.zero;
    }

    /// <summary>
    /// Enumerator that will move the platform in the time.
    /// </summary>
    /// <returns></returns>
    IEnumerator Movement()
    {
        // Debug.Log("Velocity  "+rigidBody.velocity);
        if (startingPosition == transform.position.y) // first time it is activated
            goingUp = (distance > 0)? true : false ;

        if (distance > 0) // going up
        {
            while (isMoving)
            {
                if (goingUp)
                {
                    rigidBody.velocity = new Vector3(0, speedOfMovement, 0);

                    while (this.transform.position.y < (startingPosition + distance))
                    {
                        yield return null;
                    }
                }
                goingUp = false;
                rigidBody.velocity= Vector3.zero;
                yield return delay;
                rigidBody.velocity = new Vector3(0, -speedOfMovement, 0);

                while (this.transform.position.y > startingPosition)
                {
                    yield return null;
                }
                goingUp = true;
            }
        }
        else // going down
        {
            while (isMoving)
            {
                if (!goingUp)
                {
                    rigidBody.velocity = new Vector3(0, -speedOfMovement, 0);

                    while (this.transform.position.y > (startingPosition + distance))
                    {
                        yield return null;
                    }
                }
                   
                goingUp = true;
                rigidBody.velocity = new Vector3(0, speedOfMovement, 0);

                while (this.transform.position.y < startingPosition)
                {
                    yield return null;
                }
                goingUp = false;
            }
        }
    }


    /// <summary>
    /// This ienumerator will check the sound effect of the elevator.
    /// It should play in loop so a sound effect manager is not usable
    /// </summary>
    /// <returns></returns>
    IEnumerator StartSound()
    {
        source.clip = platformActiveClip;
        source.loop = true;
        source.Play();
        while (isMoving)
            yield return delay;
        source.Stop();
    }

    /// <summary>
    /// Pause the sound effect
    /// </summary>
    public void PauseSound()
    {
        source.Pause();
    }

    /// <summary>
    /// resume the sound effect
    /// </summary>
    public void ResumeSound()
    {
        source.UnPause();
    }

}
