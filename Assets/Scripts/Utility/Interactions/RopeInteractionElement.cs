﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manages the activation of the rope ability.
/// It will deactivated itself after that.
/// </summary>
public class RopeInteractionElement : _Interactions {

    [Header("Set of texts to make the appear after the rope is taken")]
    public GameObject[] textToMakeAppear;
    [Space(10)]
    [Header("Set of texts to make the Disappear after the rope is taken")]
    public GameObject[] textToMakeDisappear;
    [Space(10)]
    [Header("Sound Effect")]
    [Header("rope sound effect clip")]
    public AudioClip ropeEffectCLip;

    private PlayerController player;

    /// <summary>
    /// This overrides the action from Interactions.
    /// It will activate the rope ability
    /// </summary>
    public override void Action()
    {
        player.transform.GetComponent<RopePlayer>().enabled = true;
        player.DeleteInteraction();
        this.GetComponent<SpriteRenderer>().enabled = false;
        this.GetComponent<CircleCollider2D>().enabled=false;
        if (ropeEffectCLip)
            SoundEffectManager.Instance.PlayClip(ropeEffectCLip);

        for (int i = 0; i < textToMakeAppear.Length; i++)
            textToMakeAppear[i].SetActive(true);
        for (int i = 0; i < textToMakeDisappear.Length; i++)
            textToMakeDisappear[i].SetActive(false);

        this.enabled = false;
    }

    /// <summary>
    /// This will save the player when it enters the range
    /// </summary>
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);

        if (!player)
            player = collision.GetComponent<PlayerController>();
    }
}
