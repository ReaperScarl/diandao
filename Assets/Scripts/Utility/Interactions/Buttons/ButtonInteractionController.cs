﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class that manages the button interaction with platforms.
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public class ButtonInteractionController : _Interactions {

    [Header("The director for the platform")]
    public GameObject platformDirectorObject;

    [Header("The sprite when the button is Up")]
    public Sprite upButton;

    [Header("The sprite when the button is Down")]
    public Sprite downButton;

    [Header("The clip played when the button is pressed")]
    public AudioClip buttonPressedClip;

    
    private bool isButtonActivated=false;
    private SpriteRenderer sRenderer;

    protected override void Start()
    {
        base.Start();
        sRenderer = GetComponent<SpriteRenderer>();
        GlowForButtonHelper spriteHelper = GetComponent<GlowForButtonHelper>();
        if (spriteHelper)
        {
            if (!PlayerPrefs.HasKey("glowyThings") || PlayerPrefs.GetInt("glowyThings") == 1) //glowy active
            {
                upButton = spriteHelper.glowUpButton;
                downButton = spriteHelper.glowDownButton;
            }
            else
            {
                upButton = spriteHelper.nGUpButton;
                downButton = spriteHelper.nGDownButton;
            }
            sRenderer.sprite = upButton;
        }
    }


    /// <summary>
    /// Override of the method Action.
    /// This Activates and deactivates the moving platform.
    /// </summary>
    public override void Action()
    {
        if (buttonPressedClip)
            SoundEffectManager.Instance.PlayClip(buttonPressedClip);
        if (isButtonActivated)
        {
            sRenderer.sprite = upButton;
            platformDirectorObject.GetComponent<PlatformDirector>().Deactivate();
            isButtonActivated = false;
        }
        else
        {
            sRenderer.sprite = downButton;
            isButtonActivated = true;
            platformDirectorObject.GetComponent<PlatformDirector>().Activate();
        }
    }
}
