﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script that manages the call of the pipe mechanism to open the pipe
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public class ButtonPipeController : _Interactions {

    [Range(0, 10)]
    public float timeToReactivate = 2;

    [Header("The sprite when the button is Up")]
    public Sprite upButton;

    [Header("The sprite when the button is Down")]
    public Sprite downButton;


    [Header ("The pipe controller that should be called from this button")]
    public GameObject pipeController;

    [Header("The clip played when the button is pressed")]
    public AudioClip buttonPressedClip;

    private bool isButtonActivated = false;
    private PipeMechanism pipe;
    private SpriteRenderer sRenderer;
    private bool activated = true;

    // Use this for initialization
    new void Start () {
        base.Start();
        pipe = pipeController.GetComponent<PipeMechanism>();
        sRenderer = GetComponent<SpriteRenderer>();
        GlowForButtonHelper spriteHelper = GetComponent<GlowForButtonHelper>();
        if (spriteHelper)
        {
            if (!PlayerPrefs.HasKey("glowyThings") || PlayerPrefs.GetInt("glowyThings") == 1) //glowy active
            {
                upButton = spriteHelper.glowUpButton;
                downButton = spriteHelper.glowDownButton;
            }
            else
            {
                upButton = spriteHelper.nGUpButton;
                downButton = spriteHelper.nGDownButton;
            }
            sRenderer.sprite = upButton;
        }
    }


    /// <summary>
    /// Override of the method Action.
    /// This Activates The pipe
    /// </summary>
    public override void Action()
    {
        if(activated)
            if (!isButtonActivated)
            {
                if (buttonPressedClip)
                    SoundEffectManager.Instance.PlayClip(buttonPressedClip);
                sRenderer.sprite = downButton;
                pipe.ActivatingPipe();
                isButtonActivated = true;
                StartCoroutine(ButtonReactiving());
            }
    }

    /// <summary>
    /// An enumerator to delay the reactivation of the button
    /// </summary>
    /// <returns></returns>
    IEnumerator ButtonReactiving()
    {
        yield return new WaitForSeconds(timeToReactivate);
        sRenderer.sprite = upButton;
        isButtonActivated = false;
    }

    /// <summary>
    /// This is called when the player is on the junkyard
    /// it is used to deactivate the button
    /// </summary>
    public void DeactivateThePipe()
    {
        activated = false;
        coll.enabled = false;
    }

}
