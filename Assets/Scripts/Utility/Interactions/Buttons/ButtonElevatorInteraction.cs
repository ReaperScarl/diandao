﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that manages the button interaction of the elevator
/// </summary>
public class ButtonElevatorInteraction : _Interactions {

    [Header("Time to wait before pressing again the button")]
    public float timeToWait;

    [Header("The sprite when the button is Up")]
    public Sprite upButton;

    [Header("The sprite when the button is Down")]
    public Sprite downButton;

    [Header("The elevator itself")]
    public GameObject elevator;

    [Header("The clip played when the button is pressed")]
    public AudioClip buttonPressedClip;

    public bool isButtonActivated = false;
    private SpriteRenderer sRenderer;
    private ElevatorMovement elevatorM;

    protected override void Start()
    {
        base.Start();
        sRenderer = GetComponent<SpriteRenderer>();
        elevatorM = elevator.GetComponent<ElevatorMovement>();

        GlowForButtonHelper spriteHelper = GetComponent<GlowForButtonHelper>();
        if (spriteHelper)
        {
            if (!PlayerPrefs.HasKey("glowyThings") || PlayerPrefs.GetInt("glowyThings") == 1) //glowy active
            {
                upButton = spriteHelper.glowUpButton;
                downButton = spriteHelper.glowDownButton;
            }
            else
            {
                upButton = spriteHelper.nGUpButton;
                downButton = spriteHelper.nGDownButton;
            }
            sRenderer.sprite = upButton;
        }
    }

    /// <summary>
    /// Override of the method Action.
    /// This Activates and deactivates the moving platform.
    /// </summary>
    public override void Action()
    {
        if (!isButtonActivated)
        {
            sRenderer.sprite = downButton;
            elevatorM.ButtonCall(this);
            isButtonActivated = true;
            if (buttonPressedClip)
                SoundEffectManager.Instance.PlayClip(buttonPressedClip);
        }
    }

    /// <summary>
    /// This method reactivates the button
    /// It's called by the elevator
    /// </summary>
    /// <returns></returns>
    public void ButtonReactiving()
    {
        sRenderer.sprite = upButton;
        isButtonActivated = false;
    }
}
