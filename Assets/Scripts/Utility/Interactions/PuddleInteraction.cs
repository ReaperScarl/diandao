﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

/// <summary>
/// Puddle interaction.
/// With the key, the scene for level3 should be loaded.
/// Without the key, it will ask you to take the key.
/// </summary>
public class PuddleInteraction : _Interactions {

	// ------	Public Variables	------
	[Space(5)]
	[Header("Level to go")]
	[Range(1, 50)]
	public int levelToGo;

	[Header("CutScene to play")]
	public VideoClip cutScene;

	[Header("RawImage to play the video")]
	public RawImage image;


    // ------	Private Variables	------
    private GameObject player;

    /// <summary>
    /// Action interaction with the puddle.
    /// </summary>
    public override void Action()
	{
		// Get player
		// GameObject player = GameObject.FindWithTag("player");
		InventoryLvl2 inventory = player.GetComponent<InventoryLvl2> ();

		if (inventory.IsItemInInventory (ItemID.Key)) 
		{
			image.gameObject.SetActive (true);
			image.GetComponent<VideoPlayer> ().clip = cutScene;
            image.GetComponent<PlayVideoScript> ().StartVideo (this.transform);
		}		
	}

	protected override void OnTriggerEnter2D (Collider2D collision)
	{
		base.OnTriggerEnter2D (collision);

		if (player == null)
			player = collision.gameObject;
	}
}
