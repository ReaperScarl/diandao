﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This is an abstract class for the interactions elements.
/// This parent has a public Method that is called by the player when interacting
/// </summary>
[RequireComponent(typeof(CircleCollider2D))]
public abstract class _Interactions : MonoBehaviour {


    //protected Text actionText;

    [Header("The range of interaction from the center of the element")]
    [Range(1, 50)]
    public float rangeOfInteraction = 2;
    [Space(10)]

    [Header("The part after \"press x to \" to write in the interaction text")]
    public string messageToSayWhenInteracting;

    protected CircleCollider2D coll;

    protected virtual void Start()
    {
        coll = GetComponent<CircleCollider2D>();
        coll.radius = rangeOfInteraction;
    }

    /// <summary>
    /// Action function that is called by the player for the interaction.
    /// </summary>
    public abstract void Action();


    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        // Add this element to the player controller so that it can be called
        if(collision.transform.GetComponent<PlayerController>())
            collision.transform.GetComponent<PlayerController>().AddInteraction(this);
    }

    protected virtual void OnTriggerExit2D(Collider2D collision)
    {
        //Delete this element from the player controller so that it can't be called outside
        if (collision.transform.GetComponent<PlayerController>())
            collision.transform.GetComponent<PlayerController>().DeleteInteraction();
    }

}
