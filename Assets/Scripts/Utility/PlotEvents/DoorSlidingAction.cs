﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script manages the call to the opening of the door in the lvl 3
/// </summary>
public class DoorSlidingAction : _Action {

    [Header ("The door to open to pass the puzzle")]
    public DoorMovementScript doorToOpen;


    public override void DoAnAction()
    {
        doorToOpen.StartMoving();
    }
}
