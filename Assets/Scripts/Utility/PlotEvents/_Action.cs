﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class will manage the action for its children.
/// </summary>
public abstract class _Action : MonoBehaviour {


    /// <summary>
    /// This method when called should do something in the chidren
    /// </summary>
    public abstract void DoAnAction();
}
