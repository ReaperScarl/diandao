﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This will start a dialogue made without interaction with the player 
/// </summary>
public class DialogueAction : _Action {

    [Header("The strings of the dialogue ")]
    public string[] stringsOfDialogue;

    [Header("The Time per string of the dialogue ")]
    public float[] timePerStringDialogue;

    private Text dialogueText;
    private DelayDisDialogueScript disappearScript;
    private LetterForLetterWord wordWriter;

    private bool isTalking = false;
    private WaitForSeconds delay = new WaitForSeconds(0.1f);

    private void Start()
    {
        dialogueText = GameCanvasManager.Instance.DialogueText;
        disappearScript = dialogueText.GetComponent<DelayDisDialogueScript>();
        wordWriter = dialogueText.GetComponent<LetterForLetterWord>();
    }

    /// <summary>
    /// Overrided Method. Starts the dialogue when called
    /// </summary>
    public override void DoAnAction()
    {
        StartCoroutine(StartTheDialogue(stringsOfDialogue, timePerStringDialogue));
    }


    protected IEnumerator StartTheDialogue(string[] dialogueStrings, float[] timePerString)
    {
        isTalking = true;
        disappearScript.ActivateDialogueElements();
        float timeForNext;
        for (int i = 0; i < dialogueStrings.Length &&isTalking; i++)
        {
            //dialogueText.text = dialogueStrings[i];
            wordWriter.StartWrite(dialogueStrings[i]);
            timeForNext = Time.time + timePerString[i];
            while (timeForNext > Time.time)
            {
                if (!isTalking)
                    break;
                yield return delay;
            }
        }
        disappearScript.Disappear();
        isTalking = false;
    }


    public void Reset()
    {
        isTalking = false;
        wordWriter.Stop();
    }
}
