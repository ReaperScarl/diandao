﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This class will open the door that is locked by plot
/// </summary>
public class DoorOpeningScript : _Action {



	// Use this for initialization
	void Start () {
		
	}


    public override void DoAnAction()
    {
        GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
    }
}
