﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the script that manages the action made by the father 
/// when Xia gives to him the first document
/// </summary>
public class FatherElevatorCallAction : _Action {

    [Header("The pipe to open")]
    public OpeningElevatorPipe pipeToOpen;

    [Header("time To wait before open the Pipe")]
    [Range(0,10)]
    public float timeToWait = 3;

    [Header("The elevator to call")]
    public ElevatorMovement elevator;


    private WaitForSeconds delay;

    private void Start()
    {
        delay = new WaitForSeconds(timeToWait);   
    }
    /// <summary>
    /// Override method to do an action.
    /// It will:
    /// - Open the pipe
    /// - Call the elevator
    /// </summary>
    public override void DoAnAction()
    {
        StartCoroutine(WaitBeforeOpenThePipe());
        elevator.FatherCall();
    }



    IEnumerator WaitBeforeOpenThePipe()
    {
        yield return delay;
        pipeToOpen.ActivatingPipe();
    }
}
