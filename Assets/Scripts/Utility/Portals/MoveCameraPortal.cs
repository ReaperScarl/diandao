﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Test
/// The camera portal to have a specific view in specific rooms
/// </summary>
public class MoveCameraPortal : _PortalScript {


    [Header("The element with the camera Following (The camera)")]
    public CameraFollowing cam;

    [Range(0, 10)]
    [Header("The time to move the camera (from the previous size)")]
    public float timeToMove = 2;

    [Header("The point in which the camera has to go")]
    public Transform pointToGo;

    [Header("true if the portal is going to move the camera, false to reactivate the camera following")]
    public bool isGoingInside = true;

    [Header("the idex in the array of this portal")]
    [Range(0, 20)]
    public int portalID;

    [Header("The director that is managing this portal")]
    public PortalToMoveCameraDirector director;


    private bool isMoving = false;
    
    protected override void Activate()
    {
  
        if (isGoingInside)
        {
            cam.enabled = false;
            StartCoroutine(MoveTheCamera());
            director.isFree = false;
        }
        else
        {
            if (!director.isFree)
            {
                director.isFree = true;
                cam.ReactivateTheFollowing(timeToMove);
                director.FreeTheCamera();
            }
            
        }
    }

    IEnumerator MoveTheCamera()
    {
        director.PortalActivation(portalID);
        float currentTime=0;
        Vector3 camPosition = cam.transform.position;
        isMoving = true;
        while (currentTime <= 1)
        {
            cam.transform.position=Vector3.Lerp(camPosition,pointToGo.position,currentTime);
            currentTime += Time.deltaTime / timeToMove;
            
            yield return null;
            if (!isMoving)
                break;
        }
        director.PortalDeactivation(portalID);
        isMoving = false;
    }

    /// <summary>
    /// Called by the director to stop the movement of this camera
    /// </summary>
    public void Stop()
    {
        isMoving = false;
    }

}
