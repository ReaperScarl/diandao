﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

/// <summary>
/// This Script manages to activate the cutscene given when the portal is passed
/// </summary>
public class CutscenePortalScript : _PortalScript{

    [Header("cutscene that you want to play when the player pass the portal")]
    public VideoClip cutscene;

    [Header("Where you want to place the cutscene")]
    public RawImage image;

    private VideoPlayer videoP;
    private PlayVideoScript videoS;

    private EnableController controller;

	// Use this for initialization
	void Start () {
        videoP = image.GetComponent<VideoPlayer>();
        videoS = image.GetComponent<PlayVideoScript>();
	}


    /// <summary>
    /// This will use the cutscene and put it in the Image
    /// </summary>
    protected override void Activate()
    {
        image.gameObject.SetActive(true);

        videoP.clip = cutscene;

        videoS.StartVideo(this.transform);
       
    }

}
