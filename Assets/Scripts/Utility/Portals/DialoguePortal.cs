﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the portal that when activated will make talk the character
/// </summary>
[RequireComponent(typeof(DialogueAction))]
public class DialoguePortal : _PortalScript {

    public DialogueAction dialogue;

    private bool isActive = true;


    protected override void Activate()
    {
        if (isActive)
        {
            isActive = false;
            dialogue.DoAnAction();
        }
    }

    public void Reset()
    {
        isActive = true;
    }
}
