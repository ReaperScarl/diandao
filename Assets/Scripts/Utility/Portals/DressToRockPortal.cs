﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script will manage the change from dress mechanic to rock mechanic
/// </summary>
public class DressToRockPortal : _PortalScript {

    [Header("The rock animator controller of Xia")]
    public RuntimeAnimatorController rockController;

    private PlayerController player;
    private Animator animator;
    private bool isFirstTime = true;

    protected override void OnTriggerExit2D(Collider2D collision)
    {
        player = collision.GetComponent<PlayerController>();
        animator = collision.GetComponent<Animator>();
        base.OnTriggerExit2D(collision);
       
    }

    protected override void Activate()
    {
        if (isFirstTime)
        {
            isFirstTime = false;
            GameCanvasManager.Instance.rockImage.enabled = true;
            GameCanvasManager.Instance.numberOfRockText.enabled = true;
            GameCanvasManager.Instance.numberOfRockText.text = ""+player.numberOfRocks;
            player.isDressActive = false;
            animator.runtimeAnimatorController = rockController;
        }
    }

    public void Reset(RuntimeAnimatorController dressController)
    {
        if (!isFirstTime)
        {
            animator.runtimeAnimatorController = dressController;
            GameCanvasManager.Instance.rockImage.enabled = false;
            GameCanvasManager.Instance.numberOfRockText.enabled = false;
            GameCanvasManager.Instance.numberOfRockText.text = "";
            isFirstTime = true;
        }
       
    }
}
