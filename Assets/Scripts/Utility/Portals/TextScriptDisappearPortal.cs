﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Portal that makes disappear the elements when the portal is passed
/// </summary>
public class TextScriptDisappearPortal : _PortalScript {

    public Text[] textsToMakeDisappear; 

    /// <summary>
    /// Overrided the Activate to make disappear the elements
    /// </summary>
    protected override void Activate()
    {
        for (int i = 0; i < textsToMakeDisappear.Length; i++)
            if(textsToMakeDisappear[i].gameObject.activeSelf)
                textsToMakeDisappear[i].GetComponent<DelayedDisappearScript>().Disappear();
    }
}
