﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// portal to stop resume the sound effect of the platforms
/// </summary>
public class PortalToStopStartSounds : _PortalScript {

    [Header("The platforms to Stop & play")]
    public PlatformMovementController [] platforms;
    [Header("True if it will be paused, false to reactivate it")]
    public bool isPausing;

    protected override void Activate()
    {
        if (isPausing)
            foreach (PlatformMovementController platform in platforms)
                platform.PauseSound();
        else
            foreach (PlatformMovementController platform in platforms)
                platform.ResumeSound();
    }
}
