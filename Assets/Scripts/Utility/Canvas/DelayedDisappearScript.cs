﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class made for make desappear a transform (for text, images etc) after a delay decided by the designer.
/// </summary>
public class DelayedDisappearScript : MonoBehaviour {

    [Header("Delay from the last modification")]
    public float secToDis=1f;

    private static bool hasTodisappear = false;
    private WaitForSeconds delay;

    void Start()
    {
        delay = new WaitForSeconds(secToDis/2);

    }

    /// <summary>
    ///     Called if the text has to disappear
    /// </summary>
    public void Disappear()
    {
        hasTodisappear = false;
        StartCoroutine(WaitToDiseppear());
    }

    protected virtual IEnumerator WaitToDiseppear()
    {
        yield return delay;
        hasTodisappear = true;
        yield return delay ;
        if (hasTodisappear)
        {
            GetComponent<Text>().text="";
        }    
    }
}
