﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateGearScript : MonoBehaviour {

    public float rotationSpeed=3;

    private bool rotating;


	// Use this for initialization
	void Start () {
		
	}
	
	
    public void Rotate(bool shouldRotate)
    {
        rotating = shouldRotate;
        if (shouldRotate)
            StartCoroutine(Rotate());
    }


    private IEnumerator Rotate()
    {
        while (rotating)
        {
            transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z- rotationSpeed);
            yield return null;
        }
    }
}
