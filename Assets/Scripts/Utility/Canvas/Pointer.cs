﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// script that is responsible for the change of the pointer
/// </summary>
public class Pointer : MonoBehaviour {

    public Texture2D pointer;

	void Start () {
        Cursor.SetCursor(pointer, Vector2.zero,CursorMode.Auto);
	}

}
