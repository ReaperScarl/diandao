﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Script that manages the graphic inventory.
/// </summary>
public class InventoryGUIManager : MonoBehaviour {

    // public Text rockText;

    [Header("Keep to 'nothing' if the corresponding sprite is not inserted")]
    [Header("(Also remember that the SIZE has to be")]
    [Header (" the same of the NUMBER OF SPRITES (children))")]
    [Space(10)]
    public ItemID[] IDs;

    public InventoryController playerInventory;

    private int numberOfImages;

    private int index;

    private void Start()
    {
        numberOfImages=transform.childCount;
        index = numberOfImages - 1;
        for (int i = index; i >= 0; i--)
            if (transform.GetChild(i).GetComponent<Image>().sprite == null)
            {

                index = i;
                break;
            }
    }

    /// <summary>
    /// Method called by the inventory controller to add a Image
    /// It will add at the index position the image and will move the index
    /// </summary>
    /// <param name="itemInfo"> Item of the object grabbed</param>
    /// <param name="spriteToInsert"> sprite to insert</param>
    public void AddItem(ItemID itemInfo, Sprite spriteToInsert)
    {
        if (itemInfo!= ItemID.Rock)
        {
            if (index >= 0)
            {
                Image image = transform.GetChild(index).GetComponent<Image>();
                Color col = image.color;
                col.a = 255;
                image.color = col;
                image.sprite = spriteToInsert;
                IDs[index] = itemInfo;
                index--;
                if (PlayerPrefs.GetInt("visibleInterface") == 0) //the images should not be visible
                {
                    image.enabled = false;
                }
            }
            else
            {
                Debug.Log("There are not spaces left!");
            }
        }
    }

    /// <summary>
    ///  Method called when an element has to be deleted from somewhere
    /// </summary>
    /// <param name="itemID"> the ID of the element to delete</param>
    public void TakeAnItem(ItemID itemID)
    {
        int indexToDelete=-1;

        for(int i=numberOfImages-1; i >= 0; i--)
        {
            if (itemID == IDs[i])
            {
                indexToDelete = i;
                break;
            }       
        }

        if (indexToDelete == -1)
        {
            Debug.Log("Item not found");
            return;
        }

        Image imageToChange = transform.GetChild(indexToDelete).GetComponent<Image>();
        
        if (indexToDelete == 0) // the item to delete is the last one
        {
            IDs[indexToDelete] = ItemID.Nothing;
            imageToChange.sprite = null;
            Color col = imageToChange.color;
            col.a = 0;
            imageToChange.color = col;
            return;
        }

        Image nextImage = transform.GetChild(indexToDelete - 1).GetComponent<Image>();
    
        for(int i=indexToDelete; i>0; i--)
        {
            if (nextImage.sprite != null) // next image has to be changed
            {
                IDs[indexToDelete] = IDs[indexToDelete - 1];
                imageToChange.sprite = nextImage.sprite;
                imageToChange = nextImage;
                indexToDelete--;
                nextImage= transform.GetChild(indexToDelete - 1).GetComponent<Image>();
            }
            else
            {
                IDs[indexToDelete] = ItemID.Nothing;
                imageToChange.sprite = null;
                Color col = imageToChange.color;
                col.a = 0;
                imageToChange.color = col;
                index = indexToDelete;
                return;
            }     
        }
    }

    /// <summary>
    /// This will put the inventory text all to not visible
    /// </summary>
    public void InventoryNotVisible()
    {
        for(int i=0; i<numberOfImages; i++)
        {
            transform.GetChild(i).GetComponent<Image>().enabled = false;
        }
    }


    /// <summary>
    /// Takes all the images and put them in an array
    /// </summary>
    /// <returns> the set of sprite at that moment</returns>
    public Sprite[] GetImages()
    {
        Sprite[] sprites = new Sprite[numberOfImages];
        for(int i=numberOfImages-1; i>=0; i--)
        {
            Image image = transform.GetChild(i).GetComponent<Image>();
            if (image.sprite)
                sprites[i] = image.sprite;
        }
        return sprites;
    }

    /// <summary>
    /// Method called to have the right images after a respawn
    /// </summary>
    /// <param name="images"> the sprites to be restored</param>
    /// <param name="oldIDs"></param>
    public void ResetInventory(Sprite [] sprites, ItemID [] oldIDs)
    {
        for(int i= numberOfImages-1; i>=0; i--)
        {
            Image image= transform.GetChild(i).GetComponent<Image>();
            if (sprites[i] == null & image.sprite != null) // delete the elements not yet got and increase the index
            {
                index++;
                if (playerInventory)
                    playerInventory.Drop(IDs[i]);
            }
            image.sprite = sprites[i];
            IDs[i] = oldIDs[i];
            if (sprites[i] == null)
            {
                Color col = image.color;
                col.a = 0;
                image.color = col;
            }
        }
    }

}
