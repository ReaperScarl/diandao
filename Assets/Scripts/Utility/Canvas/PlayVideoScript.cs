﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

/// <summary>
/// This is a script for the Raw Image that contains the playerVideo
/// This manages the play of the video.
/// </summary>
public class PlayVideoScript : MonoBehaviour {

    [Header("the In game Menu manager")]
    public InGameMenuManager gameMenu;
    [Header("The key to skip the cutscene")]
    public KeyCode skipKey = KeyCode.Escape;
    [Header("OSTs of the level")]
    public LevelSoundManager lSoundManager;

    private Text escapeText;
    private VideoPlayer videoP;
    private RawImage image;
    private EnableController enC;
    private bool isSkipping=false;
    //To not let to skip when the video is not even on
    private bool isActivated = false;

    //Audio
    private AudioSource audioSource;

    // Use this for initialization
    void Start () {
        videoP=GetComponent<VideoPlayer>();
        image=GetComponent<RawImage>();
        audioSource = GetComponent<AudioSource>();
        escapeText = GameCanvasManager.Instance.escapeText;
        escapeText.gameObject.SetActive(false);
	}

    /// <summary>
    /// This method is called to start a video
    /// </summary>
    /// <param name="transform"> the element that should have the ending controller</param>
    public void StartVideo(Transform transform)
    {
        gameMenu.PauseTheMenu();
        escapeText.gameObject.SetActive(true);
        StartCoroutine(PlayVideoUntilEnd(transform));
    }

    private void Update()
    {
        if(isActivated)
            if (Input.GetKeyDown(skipKey))
                isSkipping = true;
    }

    public IEnumerator PlayVideoUntilEnd(Transform transform)
    {

        //Set Audio Output to AudioSource
        videoP.audioOutputMode = VideoAudioOutputMode.AudioSource;

        //Assign the Audio from Video to AudioSource to be played
        videoP.EnableAudioTrack(0, true);
        videoP.SetTargetAudioSource(0, audioSource);

        videoP.Prepare();

        while (!videoP.isPrepared)
            yield return null;

        image.texture = videoP.texture;
        isActivated = true;
        lSoundManager.PauseSoundtrack(); // music pause
        // Play video
        videoP.Play();
        //Play Sound
        audioSource.Play();

        image.enabled = true;
        while (videoP.isPlaying&&!isSkipping)
            yield return null;

        image.enabled=false;
        this.gameObject.SetActive(false);
        escapeText.gameObject.SetActive(false);
        if (transform.GetComponent<_EndingController>())
            transform.GetComponent<_EndingController>().VideoFinished();
        gameMenu.ResumeTheMenu();
        isActivated = false;
        isSkipping = false;
        lSoundManager.ResumeSoundtrack(); // music resume
    }
}
