﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This is the script responsible of the game canvas.
/// It has a static global accessor to be used without reference.
/// </summary>
public class GameCanvasManager : MonoBehaviour {

    /// <summary>
    /// The Global accessor of the canvas manager
    /// </summary>
    public static GameCanvasManager Instance;
    [Header("the element text, the one used by the elements")]
    public Text elementText;
    [Header("The text used by the player")]
    public Text interactionText;

    [Header("the inventory shown in the screen")]
    public InventoryGUIManager inventoryGUI;

    [Header("the text with the number of the rock (when active)")]
    public Text numberOfRockText;

    [Header("rock Image icon")]
    public Image rockImage;

    [Header("The cutscene script")]
    public PlayVideoScript cutscene;
    
    [Header("the dialogue background")]
    public Image dialogueBackGround;
    [Header("the dialogue gear")]
    public Image dialogueGear;
    [Header("the dialogue text")]
    public Text DialogueText;

    [Header("the text used in the cutscene")]
    public Text escapeText;

    void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);
        else
            Instance = this;
    }
}
