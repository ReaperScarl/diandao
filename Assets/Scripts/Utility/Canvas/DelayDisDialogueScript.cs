﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script is a implementation of the delayed disappear
/// This will manage also the graphic parts different from the text itself
/// </summary>
public class DelayDisDialogueScript : DelayedDisappearScript {

    public void ActivateDialogueElements()
    {
        GameCanvasManager.Instance.dialogueBackGround.enabled = true;
        GameCanvasManager.Instance.dialogueGear.enabled = true;
    }

    protected override IEnumerator WaitToDiseppear()
    {
        yield return base.WaitToDiseppear();
        GameCanvasManager.Instance.dialogueBackGround.enabled = false;
        GameCanvasManager.Instance.dialogueGear.enabled = false;
    }
}
