﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// This is the script that will write one letter per letter the string in the dialogue
/// </summary>
public class LetterForLetterWord : MonoBehaviour {

    [Header("The time after each letter will be written ")]
    public float delayBetweenLetters = 0.05f;

    private RotateGearScript gear;
    private Text text;
    private WaitForSeconds delay;
    private bool isStopping;


	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
        delay = new WaitForSeconds(delayBetweenLetters);
        gear = GameCanvasManager.Instance.dialogueGear.GetComponent<RotateGearScript>();
	}

    /// <summary>
    /// This will start to write  letter for letter the string given
    /// </summary>
    /// <param name="stringToWrite">the string to write letter per letter</param>
    public void StartWrite(string stringToWrite)
    {
        StartCoroutine(WriteLetterForLetter(stringToWrite));
    }

    private IEnumerator WriteLetterForLetter(string stringToWrite)
    {
        isStopping = false;
        text.text = "";
        gear.Rotate(true);
        char[] chars = stringToWrite.ToCharArray();
        for(int i=0; i<stringToWrite.Length&!isStopping; i++)
        {
            text.text += chars[i];
            yield return delay;
        }
        gear.Rotate(false);
        isStopping = false;
    }


    public void Stop()
    {
        if(!isStopping)
            isStopping = true;
    }
}
