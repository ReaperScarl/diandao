﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectVolumeController : MonoBehaviour {

	// Use this for initialization
	void Start () {

        if (this.tag == "soundEffect")
        {
            float volume = PlayerPrefs.GetFloat("soundEffectVolume");
            AudioSource[] audios = GetComponents<AudioSource>();
            for (int i = 0; i < audios.Length; i++)
            {
                audios[i].volume = volume;
            }
        }

	}
	
}
