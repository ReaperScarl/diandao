﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script manages the movement of the Door in lvl 3
/// It will open going down or up, depending on the distance said
/// </summary>
public class DoorMovementScript : MonoBehaviour {

    [Header("The distane between the starting point and the last one")]
    [Range(-15, 15)]
    public float distanceToReach=-2;

    [Header ("The speed of the door to move")]
    public float speed=3;

    [Header("The sound when the door is moving")]
    public AudioClip doorMovementClip;

    private Rigidbody2D rigidBody;
    private float startingPosition;
    private bool goingUp = false;
    private bool isMoving = false;

    void Start()
    {
        startingPosition = transform.position.y;
        rigidBody = GetComponent<Rigidbody2D>();
        goingUp = (distanceToReach > 0) ? true : false;
    }


    /// <summary>
    /// This method is called from the outside by an actions et.
    /// This starts the movement of the door.
    /// </summary>
    public void StartMoving()
    {
        isMoving = true;
        StartCoroutine(Movement());
        SoundEffectManager.Instance.PlayClip(doorMovementClip);
    }


    /// <summary>
    /// This method is called from the outside by a reset function.
    /// It is used to stop the movement of the door.
    /// </summary>
    public void ResetDoor()
    {
        isMoving = false;
        rigidBody.velocity = Vector3.zero;
        transform.position = new Vector2(transform.position.x, startingPosition);
    }


    /// <summary>
    /// Enumerator that will move the platform in the time.
    /// </summary>
    /// <returns></returns>
    IEnumerator Movement()
    { 
        if (goingUp) // going up
        {
            rigidBody.velocity = new Vector3(0, speed, 0);
            while (this.transform.position.y < (startingPosition + distanceToReach) && isMoving)
                yield return null;  
        }
        else // going down
        {
            rigidBody.velocity = new Vector3(0, -speed, 0);
            while (this.transform.position.y > (startingPosition + distanceToReach) && isMoving)
                yield return null;
        }
        rigidBody.velocity = Vector3.zero;
    }
}
