﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script manages the change of fixed camera.
/// when 2 portals are working togheter, the last one will stop
/// </summary>
public class PortalToMoveCameraDirector : MonoBehaviour {

    [Header("The portals to manage")]
    public MoveCameraPortal[] portals;

    [Header("The camera is free")]
    public bool isFree = true;


    private bool[] activePortals;
    

    private void Start()
    {
        activePortals = new bool[portals.Length];
    }

    /// <summary>
    /// This method is used by a move camera portal to let know the director that someone else is moving
    /// </summary>
    /// <param name="portalIndex"></param>
    public void PortalActivation(int portalIndex)
    {
        activePortals[portalIndex] = true;
        for(int i=0; i<activePortals.Length; i++)
        {
            if(activePortals[i]&& i!=portalIndex) // if it's active some other portals
            {
                portals[i].Stop();
            }
        }
    }

    /// <summary>
    /// This method is called by the not inside portals, so they can stop the portals moving
    /// </summary>
    public void FreeTheCamera()
    {
        for (int i = 0; i < activePortals.Length; i++)
        {
            portals[i].Stop();
        }
    }



    /// <summary>
    /// Called by the portal to let it know that it finished
    /// </summary>
    /// <param name="portalIndex"> the number of the index of the portal</param>
    public void PortalDeactivation(int portalIndex)
    {
        activePortals[portalIndex] = false;
    }

}
