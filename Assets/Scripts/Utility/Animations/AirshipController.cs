﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the controller of the single airship. 
/// </summary>
[RequireComponent (typeof(Rigidbody2D))]
public class AirshipController : MonoBehaviour {


    

    [Header("The distance after it will to disappear scaling down ")]
    public float distanceBeforeDisappearing = 4;

    [Header("the speed of the airship")]
    [Range(1,10)]
    public float speed=5;



    //    - - - -    private variables

    private WaitForSeconds speedDelay = new WaitForSeconds(0.5f);

    private Vector2 destination;
    private bool isdestinationRight;
    private Rigidbody2D rb2d;
    private int numberOfOrder;
    private AirshipDirector director;

    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.bodyType = RigidbodyType2D.Kinematic;
    }

    /// <summary>
    /// This will call the start to move the airship
    /// </summary>
    /// <param name="destination"> the position to start from where spawn it</param>
    public void StartAirship(Vector2 destination, float startingScale, int index, AirshipDirector director)
    {
        this.destination = destination;
        isdestinationRight = (destination.x > 0) ? true : false;

        Vector2 scale = new Vector2(startingScale, startingScale);

        if (!isdestinationRight)
           scale.x= startingScale * -1;

        transform.localScale = scale;

        numberOfOrder = index;
        this.director = director;
        StartCoroutine(MoveAirship());
    }

    
    /// <summary>
    /// This will start to movement of the airship 
    /// </summary>
    private IEnumerator MoveAirship()
    {
        if(!rb2d)
            rb2d = GetComponent<Rigidbody2D>();
        if (isdestinationRight)
            rb2d.velocity = new Vector2(speed*transform.localScale.y, 0);
        else
            rb2d.velocity = new Vector2(-speed * transform.localScale.y, 0);

      //  Debug.Log("destination: "+ destination.x+" position "+transform.position.x);
        while (Mathf.Abs(destination.x - transform.position.x)>2)
        {
            //Debug.Log("move airship while");
            yield return speedDelay;
        }
      //  Debug.Log("move airship end of num");
        StartCoroutine(DisappearAirship());
    }

    /// <summary>
    /// This method is called to make disappear the ship
    /// </summary>
    private IEnumerator DisappearAirship()
    {
        float heightToReach = transform.position.y + 20;
        rb2d.velocity = new Vector2(rb2d.velocity.x, distanceBeforeDisappearing);
        Vector2 scale = new Vector2(transform.localScale.x, transform.localScale.y);
       
        while ((heightToReach - transform.position.y) > 2)
        {
            scale.x +=(isdestinationRight)? -0.001f: +0.001f;
            scale.y += -0.001f;
            transform.localScale = scale;
            yield return null;
        }
     //   Debug.Log("disappear airship before zero");
        rb2d.velocity = Vector2.zero;
      //  Debug.Log("disappear airship end of num");
        director.OneAirshipIsGone(numberOfOrder);
    }
}
