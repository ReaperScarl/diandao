﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script should manage the speed of an animation changing it's multiplier parameter
/// </summary>
[RequireComponent(typeof(Animator))]
public class AnimationSpeedController : MonoBehaviour {

    [Range(0,5)]
    public float speedValue = 1;

    private Animator anim;


    private void OnEnable()
    {
        anim = GetComponent<Animator>();
        anim.SetFloat("animSpeed", speedValue);
    }

}
