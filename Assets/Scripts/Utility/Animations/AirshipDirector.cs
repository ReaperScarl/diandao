﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the director that will control the airship to fly
/// </summary>
public class AirshipDirector : MonoBehaviour {

    [Header("the airship prefab")]
    public GameObject airshipPrefab;

    [Header("Time to spawn between ships")]
    public float timeToSpawn = 1;


    [Header("Maximum scale that the airships will have")]
    public float maxScale = 0.3f;

    [Header("Miminum scale that the airships will have")]
    public float minScale = 0.01f;

    [Header("Number of maximum ships moving at once")]
    public int numberOfAirshipsAlive;



    [Space(10)]
    
    [Header("Area of spawn to setup")]
    [Space(5)]
    [Header ("The trasform with the position from where the airship can be spawn going to the right")]
    public Transform maxLeftPos;

    [Header("The trasform with the position from where the airship can be spawn going to the left")]
    public Transform maxRightPos;
    
    private WaitForSeconds reactionTime;

    private GameObject[] airships; 


	void Start ()
    {
        reactionTime = new WaitForSeconds(timeToSpawn);
        StartCoroutine(CallTheFirstsShips());
    }


    private IEnumerator CallTheFirstsShips()
    {
        airships = new GameObject[numberOfAirshipsAlive];
        for(int i=0; i< numberOfAirshipsAlive; i++)
        {
            airships[i] = Instantiate(airshipPrefab, transform);
            airships[i].name = airships[i].name +" "+ i;
            StartAnAirship(i);
            yield return reactionTime;
        }
    }

    private void StartAnAirship(int index)
    {

        float directionChoice = Random.Range(0f, 1);
      //  Debug.Log("dirchoice: " + directionChoice);

        float sizeChoice = Random.Range(0f, 1);
      //  Debug.Log("sizechoice: " + sizeChoice);


        Vector2 destination;
      
        if (directionChoice < 0.5f)
        {
            destination = new Vector2(maxLeftPos.position.x, maxLeftPos.position.y);
            airships[index].transform.position = new Vector2(maxRightPos.position.x, maxRightPos.position.y + (5 - (10 * directionChoice)));
        }
        else
        {
            destination = new Vector2(maxRightPos.position.x, maxRightPos.position.y);
            airships[index].transform.position = new Vector2(maxLeftPos.position.x, maxLeftPos.position.y + (5 - (8 * directionChoice)));
        }

        float scale = (sizeChoice * maxScale < minScale) ? minScale : sizeChoice * maxScale;
        

        airships[index].GetComponent<AirshipController>().StartAirship(destination,scale,index,this);
    }
   

    /// <summary>
    /// This method is called when an airship has finished its journey
    /// </summary>
    /// <param name="index"></param>
    public void OneAirshipIsGone(int index)
    {
        Debug.Log("A ship is returned: "+ airships[index].name);
        StartAnAirship(index);
    }
}
