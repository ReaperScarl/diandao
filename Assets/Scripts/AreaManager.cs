﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This class will manage the single area change.
/// </summary>

public class AreaManager : MonoBehaviour {

    [Header("The indixes according to the director of the areas that needs to be visible in this area. These elements need to be in order")]
    public int[] areas;

    [Header("The director that needs to be called")]
    public MapDirector director;

    public void CallTheChange()
    {
        director.ChangeVisibility(areas);
    }
}
