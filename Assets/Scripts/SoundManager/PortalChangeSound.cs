﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class calls
/// ChangingSound method to change
/// the listened sound track
/// by the player
/// </summary>
public class PortalChangeSound : _PortalScript {

    [Header("Sound Manager of the level")]
    public GameObject soundManager;

    [Header("The index of the sound track that is listened by player in soundtracks array")]
    public int runningOSTIndex;

    [Header("The index of the sound track that is not listened by player in soundtracks array")]
    public int notRunningOSTIndex;

    public bool isActive = true;

    /// <summary>
    /// This method calls the ChangingSound
    /// method, in order to fade out the running
    /// sound track and to fade in the not running one
    /// </summary>
    protected override void Activate()
    {
        if (isActive)
        {
            isActive = false;
            soundManager.GetComponent<LevelSoundManager>().ChangingSound(runningOSTIndex, notRunningOSTIndex);
        }
    }
}
