﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is responsible for SoundManager on Main Menu.
/// This script should be attached to SoundManager.
/// Play a clip for once, and then play another clip for loop.
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class MainMenuSoundManager : MonoBehaviour {

	// ------	Public Variables	------
	[Header("Background Music For Once")]
	public AudioClip acOnce;

	[Header("Background Music For Loop")]
	public AudioClip acLoop;

	[Header("Audio Source For Once")]
	public AudioSource audioSource;

	[Header("Audio Source For Loop")]
	public AudioSource audioSource2;

	[Header("Seconds to start the second Track After the first one")]
	public float seconds = 4.0f;


	// ------	Private Variables	------
	private float acOnceLength;	// Clip length of the background music for once

	// Use this for initialization
	void Start () 
	{
		audioSource.clip = acOnce;
		float backgroundVolume = 1.0f;
		if(PlayerPrefs.HasKey("backgroundVolume"))
			backgroundVolume = PlayerPrefs.GetFloat ("backgroundVolume");
		audioSource.volume = backgroundVolume;
		audioSource2.volume = backgroundVolume;
		audioSource.loop = false;

		acOnceLength = acOnce.length;
        audioSource.Play();
        Invoke("PlayASLoop", seconds);
    }

	/// <summary>
	/// Play asLoop
	/// Create this function to called by the Invoke() function
	/// </summary>
	void PlayASLoop()
	{
		audioSource2.clip = acLoop;
		audioSource2.loop = true;

		audioSource2.Play ();
	}

}
