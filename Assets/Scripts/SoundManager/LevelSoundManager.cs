﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manages the 
/// sound tracks inside the level
/// It provides attributes to insert
/// various sound tracks and
/// to check if they should be
/// heared by player or not
/// </summary>
public class LevelSoundManager : MonoBehaviour {

    [Header("All the clips of the current level")]
    public AudioClip[] soundTracks;

    [Header("All the AS of the current level")]
    public AudioSource[] audioSources;

    [Header("i-th element says if AS-i should be heared or not")]
    public bool[] stActives;

    [Header("How faster the ost fades")]
    [Range(0.1F, 10F)]
    public float fadeTime;

    private float generalVolume;

    void Start()
    {
        generalVolume = PlayerPrefs.GetFloat("backgroundVolume");

        for (int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i].clip = soundTracks[i];
            if (stActives[i])
            {
                audioSources[i].volume = generalVolume;
            }
            else audioSources[i].volume = 0;
        }

        PlayAsLoop(audioSources);
    }

    /// <summary>
    /// This method receives the
    /// set of sound tracks and play
    /// them in loop
    /// </summary>
    /// <param name="audioSource"></param>
    void PlayAsLoop(AudioSource[] audioSource)
    {
        for (int i = 0; i<audioSource.Length; i++)
        {
            audioSource[i].loop = true;
            audioSource[i].Play();
        }
    }

    /// <summary>
    /// This method changes
    /// the volume of the sound track(s)
    /// that player is listening
    /// </summary>
    /// <param name="volume"> The new volume of the actives tracks </param>
    public void ChangeVolume(float volume)
    {
        generalVolume = volume;

        for (int i = 0; i < audioSources.Length; i++)
        {
            if (stActives[i])
            {
                audioSources[i].volume = generalVolume;
            }
        }
    }

    /// <summary>
    /// This method is called by a portal
    /// in order to start a coroutine to
    /// control two (set of) sound tracks
    /// </summary>
    /// <param name="activeAS"> the active track to fade out </param>
    /// <param name="toActiveAS"> the inactive track to fade in </param>
    public void ChangingSound(int activeAS, int toActiveAS)
    {
        StartCoroutine(Change(activeAS, toActiveAS));
    }

    /// <summary>
    /// This method works in the
    /// following way:
    /// </summary>
    /// <param name="activeAS"> It is (are) the sound 
    /// track(s) to fade out. Must be put to false 
    /// in the active set </param>
    /// <param name="toActiveAS"> It is (are) 
    /// the sound track(s) to fade in.
    /// Must be put to true in the active set </param>
    /// <returns></returns>
    private IEnumerator Change(int activeAS, int toActiveAS)
    {
        while (audioSources[activeAS].volume > 0)
        {
            audioSources[activeAS].volume -= generalVolume * Time.deltaTime / fadeTime;
            audioSources[toActiveAS].volume += generalVolume * Time.deltaTime / fadeTime;
            yield return new WaitForEndOfFrame();
        }
        stActives[activeAS] = false;
        stActives[toActiveAS] = true;
    }

    /// <summary>
    /// This method checks the active
    /// soundtracks and pause them
    /// </summary>
    public void PauseSoundtrack()
    {
        for (int i=0; i<stActives.Length; i++)
        {
            if (stActives[i])
            {
                audioSources[i].Pause();
            }
        }
    }

    /// <summary>
    /// This method checks the active 
    /// soundtracks and resume them
    /// </summary>
    public void ResumeSoundtrack()
    {
        for (int i = 0; i < stActives.Length; i++)
        {
            if (stActives[i])
            {
                audioSources[i].UnPause();
            }
        }
    }
}
