﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// FSM that manages the boar behaviour.
/// This ai should have the following states:
/// - sleep
/// - chase & flee when hitted by the hive
/// - chase when awaken by the player
/// - kill when the player is in the range of death of the boar
/// </summary>
[RequireComponent(typeof(Animator))]
public class BoarAI : MonoBehaviour {

    [Header("Speed of the boar while chasing & fleeing")]
    public float runSpeed = 5;

    [Header("X position where the boar is headed  while fleeing")]
    public float XPosOfFlee;

    [Header(" the range whitin the boar will kill the player")]
    public float rangeOfDeath = 2f;

    [Header("Seconds between each update of the FSM")]
    public float timeOfReaction = 0.3f;

    [Space(10)]
    [Header("      Elements to set up")]

    [Header("Death Manager To Call when killing")]
    public DeathMechanic dM;
    [Space(10)]
    //------   Audio variables
    [Header("      Audio sources of the boar")]
    [Space(5)]
    [Header("audio Clip of the boar Sleeping")]
    public AudioClip sleepingGrunt;
    [Header("audio Clip of the boar attacking or fleeing")]
    public AudioClip attackEscapeClip;
    [Header("audio Clip of the boar moving")]
    public AudioClip movementClip;

    public AudioClip beeClip;

    [Header("how far the player has to be to not hear the boar")]
    public float rangeOfHearing = 15;


    private Transform playerTransform;
    private Transform target;
    private Rigidbody2D rb2D;
    private bool isFacingRight=false;
    private bool readyToMove=true;
    WaitForSeconds FSMUpdateDelay;
  
    private FSM fsm;

    // ------ audio variables
    private bool isSleeping=true;
    WaitForSeconds sleepingDelay;
    WaitForSeconds stepsDelay;
    WaitForSeconds beeDelay;


    // ------ Animator component
    private Animator anim;

    // Use this for initialization
    void Start() {
        playerTransform = GameObject.FindGameObjectWithTag("player").transform;
        FSMUpdateDelay = new WaitForSeconds(timeOfReaction);
        rb2D = GetComponent<Rigidbody2D>();
        StartCoroutine(SleepingSounds());
        sleepingDelay = new WaitForSeconds(sleepingGrunt.length+2f);
        stepsDelay = new WaitForSeconds(movementClip.length + 0.1f);
        beeDelay = new WaitForSeconds(beeClip.length);
        anim = GetComponent<Animator>();
    }

    /// <summary>
    ///  Function that updates the fsm
    /// </summary>
    IEnumerator BoarAwake()
    {
        isSleeping = false;
        anim.SetBool("isAwake", true);
        StartCoroutine(MovementSounds());
        while (readyToMove)
        {
            fsm.Update();
            yield return FSMUpdateDelay;
        }
    }

    /// <summary>
    /// This Method will make the boar chase the player
    /// </summary>
    private void Chase()
    {
        // See the target & Go towards the target
        if (transform.position.x-target.position.x>0.1f) // boar on the right
        {
            if (isFacingRight) // face the opposite direction
                Flip();
            rb2D.velocity = Vector2.left * runSpeed;
        }
        if(transform.position.x - target.position.x < 0.1f)// boar on the left
        {
            if (!isFacingRight) // face the opposite direction
                Flip();
            rb2D.velocity = Vector2.right * runSpeed;
        }
        anim.SetFloat("hSpeed", Mathf.Abs(rb2D.velocity.x));
    }

    /// <summary>
    /// this method flips the character while moving when it needs to go in the opposite direction
    /// </summary>
    void Flip()
    {
        isFacingRight = !isFacingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    /// <summary>
    /// Condition to pass to the Killing State
    /// </summary>
    /// <returns></returns>
    bool IsInDeathRange()
    {
        return (Vector2.Distance(target.position, transform.position) < rangeOfDeath);
    }

    /// <summary>
    /// This Method will make the boar kill the player
    /// </summary>
    private void Kill()
    {
        anim.SetBool("isAttacking", true);
        //Debug.Log("Die!");
        dM.PlayerDie();
        SoundEffectManager.Instance.PlayClip(attackEscapeClip);
        readyToMove = false;
    }

    /// <summary>
    /// This Method will make the boar flee the scene and go to a definite position
    /// </summary>
    private void Flee() 
    {
        if (transform.position.x - XPosOfFlee > 0.1f) // boar on the right
        {
            if (isFacingRight) // face the opposite direction
                Flip();
            rb2D.velocity = Vector2.left * runSpeed;
        }
        if (transform.position.x - XPosOfFlee < 0.1f)// boar on the left
        {
            if (!isFacingRight) // face the opposite direction
                Flip();
            rb2D.velocity = Vector2.right * runSpeed;
        }
        if (Mathf.Abs(transform.position.x - XPosOfFlee) < 1)
            Escaped();
    }

    /// <summary>
    /// When the boar will reach the  point, it will "escape".
    ///  It:
    ///  - closes the FSM
    ///  - Deactivates the colliders
    ///  - deactivates its renderer
    /// </summary>
    private void Escaped()
    {
        readyToMove = false;
        GetComponent<SpriteRenderer>().enabled = false;
        rb2D.bodyType = RigidbodyType2D.Static;
        Collider2D[] colliders = GetComponents<Collider2D>(); // takes off every collider
        for (int i = 0; i < colliders.Length; i++)
                colliders[i].enabled = false;
    }

    /// <summary>
    /// Starts the FSM when called by the Trigger
    /// </summary>
    void StartTheBoarFSM(bool playerActivation)
    {
        if (playerActivation) // chase & kill
        {
            // Defining states and link actions when enter/exit/stay for the boar
            FSMState chasing = new FSMState();
            chasing.enterActions = new FSMAction[] { Chase };
            chasing.stayActions = new FSMAction[] { Chase };
            FSMState killing = new FSMState();
            killing.enterActions = new FSMAction[] { Kill };

            //Defining transitions
            FSMTransition t1 = new FSMTransition(IsInDeathRange);
            //Link states with transitions
            chasing.AddTransition(t1, killing);

            //Setup a FSM at initial state
            fsm = new FSM(chasing);
        }
        else //flee
        {
            // Defining states and link actions when enter/exit/stay for the boar
            FSMState fleeing = new FSMState();
            fleeing.enterActions = new FSMAction[] { Flee };
            fleeing.stayActions = new FSMAction[] { Flee };
            //Setup a FSM at initial state
            fsm = new FSM(fleeing);
            StartCoroutine(BeeSounds());
        }
        StartCoroutine(BoarAwake());
    }

    /// <summary>
    /// The Boar will awake when the player comes in or the hive comes
    /// </summary>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "player") // trigger enters
        {

            target =collision.transform;
            StartTheBoarFSM(true); 
        }
        if (collision.tag == "hive") // hive is hitting
        {
            Collider2D[] colliders = GetComponents<Collider2D>();
            for (int i = 0; i < colliders.Length; i++)
                if (colliders[i].isTrigger)
                    colliders[i].enabled = false; 
            StartTheBoarFSM(false);
            anim.SetBool("isEscaping", true);
            SoundEffectManager.Instance.PlayClip(attackEscapeClip);
        }
    }

    /// <summary>
    /// Method called by the reset Script
    /// It has to:
    /// - clear the fsm
    /// - clear the target
    /// - re enable the colliders
    /// - put the dynamic again to the boar
    /// - re put the ready to the boar
    /// </summary>
    public void Reset()
    {
        target = null;
        fsm = null;
        GetComponent<SpriteRenderer>().enabled = true;
        Collider2D[] colliders = GetComponents<Collider2D>(); // takes off every collider
        for (int i = 0; i < colliders.Length; i++)
            colliders[i].enabled = true;
        readyToMove = true;
        if (isFacingRight)
            Flip();
        rb2D.bodyType = RigidbodyType2D.Dynamic;
        rb2D.velocity = Vector2.zero;
        isSleeping = true;
        anim.SetBool("isAttacking", false);
        anim.SetBool("isAwake", false);
        anim.SetBool("isEscaping", false);

        StartCoroutine(SleepingSounds());
    }

    //---- Audio methods

    /// <summary>
    /// This will check if the player is in range, and if it is, it will emit the sound
    /// </summary>
    IEnumerator SleepingSounds()
    {
        while (isSleeping)
        {
            //Debug.Log("Distance: " + Vector2.Distance(playerTransform.position, transform.position));
            if (Vector2.Distance(playerTransform.position, transform.position) < rangeOfHearing)
            {
                if (sleepingGrunt)
                    SoundEffectManager.Instance.PlayClip(sleepingGrunt);
                yield return sleepingDelay;
            }
            else
            {
                yield return FSMUpdateDelay;
            }
            
        }
    }

    /// <summary>
    /// This will make the sounds of the boar when moving
    /// </summary>
    IEnumerator MovementSounds()
    {
        while (readyToMove&&!isSleeping)
        {
            SoundEffectManager.Instance.PlayClip(movementClip);
            yield return stepsDelay;
        }
    }



    /// <summary>
    /// This will make the sounds of the boar when moving
    /// </summary>
    IEnumerator BeeSounds()
    {
        while (readyToMove&& !isSleeping)
        {
            SoundEffectManager.Instance.PlayClip(beeClip);
            yield return beeDelay;
        }
    }

}
