﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manages the settings
/// of the movement of the butterfly
/// </summary>

[RequireComponent(typeof(Animator))]
public class ButterflyMovement : MonoBehaviour {

    [Header("Speed of the butterfly")]
    public float buttSpeed;
    [Header("Speed of the sine movement")]
    [Range(1F, 15F)]
    public float frequency;  
    [Header("Size of the sine movement")]
    [Range(0F, 1F)]
    public float magnitude;   
    [Header("Position where the butterfly disappear")]
    public float finalPos;
    [Header("Position to be reached before moving")]
    public Vector2 highPos;

    //the axis along where the sinusoidal path is given
    private Vector3 axis;
    //position to use for the part where the butterfly lifts in the air
    private Vector3 entireHighPos;

    private Animator anim;
    
    //amount of "time" to add to time variable
    public float epsilon = 0.0167F;
    //starting value for the sinusoidal
    public float startingTime = 5.045372F;

    public bool isActive = true;

    void Start()
    { 
        axis = transform.up;
        entireHighPos.x = transform.position.x + highPos.x;
        entireHighPos.y = transform.position.y + highPos.y;
        entireHighPos.z = 0;
        anim = GetComponent<Animator>();
    }

    /// <summary>
    /// This method calls the coroutine
    /// to start the movement of the butterfly
    /// </summary>
    public void Activation()
    {
        StartCoroutine(Movement());
    }

    /// <summary>
    /// This method applies the sinusoidal movement
    /// Once the batterfly reaches the end pint,
    /// the butterfly is disabled
    /// </summary>
    /// <returns></returns>
    private IEnumerator Movement()
    {
        float time = startingTime;
        isActive = true;
        anim.SetBool("isMoving", isActive); //animator speed part
        while (transform.position != entireHighPos && isActive)
        {
            transform.position = Vector2.MoveTowards(transform.position, entireHighPos, buttSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        while (transform.position.x <= finalPos && isActive)
        {
            transform.position += transform.right * Time.deltaTime * buttSpeed;
            if(Time.deltaTime != 0)
            {
                transform.position = transform.position + axis * Mathf.Sin(time * frequency) * magnitude;
                time = time + epsilon;
            }
            yield return new WaitForEndOfFrame();
        }
        if (isActive)
        {
            anim.SetBool("isMoving", false);
            gameObject.SetActive(false);
        }
    }
}