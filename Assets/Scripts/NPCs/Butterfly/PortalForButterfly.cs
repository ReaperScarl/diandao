﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is used to trigger
/// in a certain set position
/// the movement of the butterfly 
/// </summary>
public class PortalForButterfly : _PortalScript {

    //the butterfly itself
    public GameObject butterfly;

    public bool isActive = true;

    /// <summary>
    /// This method is called by the trigger when 
    /// the main character reaches the portal whose 
    /// script is attached. Once the butterfly is
    /// disabled, this gameobject is disabled too
    /// </summary>
    protected override void Activate()
    {
        if (isActive)
        {
            butterfly.GetComponent<ButterflyMovement>().Activation();
        }
        isActive = false;
    }
}
