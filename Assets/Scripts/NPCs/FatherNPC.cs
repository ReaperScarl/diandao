﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Script that manages the dialogues of the father
/// </summary>
public class FatherNPC : _TalkNPC {

    [Header("The strings of the dialogue when the player hasn't taken the first document yet")]
    public string[] firstDocNotTakenDialogue;

    public float[] timePerStringFirstDialogue;

    [Space(10)]
    [Header("The strings of the dialogue when the player has taken the first document")]
    public string[] firstDocTakenDialogues;

    public float[] timePerStringSecondDialogue;
    [Space(10)]

    [Header("The strings of the dialogue when the player hasn't taken the second document yet")]
    public string[] afterFirstDocTakenDialogue;

    public float[] timePerStringThirdDialogue;
    [Space(10)]

    [Header("The strings of the dialogue when the player has taken the second document")]
    public string[] secondDocTakenDialogue;

    public float[] timePerStringFourthDialogue;
    [Space(10)]

    private _Action FatherCallAction;

    [Header("The door to unlock")]
    public GameObject labDoor;

    //"The inventory element inside the game canvas"
    private InventoryGUIManager inventoryManager;

    private InventoryController inventory;

    /// <summary>
    /// 4 values:
    ///    - 0 first dialogue not taken the document yet
    ///    - 1 Taken the first document but not the second - first time
    ///    - 2 Taken the first document but not the second - the next ones
    ///    - 3 Taken the second document
    /// </summary>    
    private int indexOfDialogue;

	// Use this for initialization
	override protected void Start () {

        FatherCallAction = GetComponent<_Action>();
        inventoryManager = GameCanvasManager.Instance.inventoryGUI;
        base.Start();

        indexOfDialogue = 0;
	}
	
	

    /// <summary>
    /// Ovverride of the method from the abstract class
    /// </summary>
    /// <param name="player"> it is the player</param>
    public override void Talk(GameObject player)
    {
        if (hasTalked)
            return;
        if (!inventory)
            inventory=player.GetComponent<InventoryLvl1>();

        if (indexOfDialogue == 1 && !inventory.IsItemInInventory(ItemID.AlienDocument)) // first taken - next time
            indexOfDialogue = 2;

        if (indexOfDialogue < 1 && inventory.IsItemInInventory(ItemID.FatherDocument)) // first taken - first time
        {
            indexOfDialogue = 1;
            FatherCallAction.DoAnAction();
        }
           

        if(indexOfDialogue <= 2 && inventory.IsItemInInventory(ItemID.AlienDocument)) // second taken
            indexOfDialogue = 3;

        //Debug.Log("index " + indexOfDialogue);

        switch (indexOfDialogue)
        {
            case 0:     // not taken
                StartCoroutine(StartTheDialogue(firstDocNotTakenDialogue, timePerStringFirstDialogue));
               break;

            case 1:        // first taken - first time

                StartCoroutine(StartTheDialogue(firstDocTakenDialogues, timePerStringSecondDialogue));
                inventoryManager.TakeAnItem(ItemID.FatherDocument);
                break;

            case 2:         // first taken - next times
                StartCoroutine(StartTheDialogue(afterFirstDocTakenDialogue, timePerStringThirdDialogue));
                break;

            case 3:         // second taken
                StartCoroutine(StartTheDialogue(secondDocTakenDialogue, timePerStringFourthDialogue));
                // maybe at the end it could finish and go to a cutscene
                labDoor.GetComponent<DoorLevelInteractionScript>().Unlock();
                inventoryManager.TakeAnItem(ItemID.FatherDocument);
                break;
        }
    }

}
