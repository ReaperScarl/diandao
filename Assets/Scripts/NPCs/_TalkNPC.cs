﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// This is an abstract class for the Talk elements.
/// This parent has a public Method that is called by the player when interacting
/// </summary>
[RequireComponent(typeof(CircleCollider2D))]

public abstract class _TalkNPC : MonoBehaviour {

    [Header("The range of interaction from the center of the element")]
    [Range(1, 20)]
    public float rangeOfInteraction = 2;



    protected Text dialogueText;
    private CircleCollider2D coll;
    private DelayDisDialogueScript disappearScript;
    private LetterForLetterWord wordWriter;
    protected bool hasTalked=false;

    protected virtual void Start()
    {
        dialogueText = GameCanvasManager.Instance.DialogueText;
        coll = GetComponent<CircleCollider2D>();
        coll.radius = rangeOfInteraction;
        wordWriter = dialogueText.GetComponent<LetterForLetterWord>();
        disappearScript= dialogueText.GetComponent<DelayDisDialogueScript>();
    }

    /// <summary>
    /// Function that is called by the player to talk.
    /// </summary>
    public abstract void Talk(GameObject player);

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Add this element to the player controller so that it can be called
        if (collision.transform.GetComponent<PlayerController>())
            collision.transform.GetComponent<PlayerController>().AddToTalk(this.gameObject);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        //Delete this element from the player controller so that it can't be called outside
        if (collision.transform.GetComponent<PlayerController>())
            collision.transform.GetComponent<PlayerController>().ExitFromTalkRange();
    }

                        //--- Probably to move on the dialogue text
    /// <summary>
    /// This is a method called by everyone of the children To start a dialogue when activated.
    /// </summary>
    /// <param name="dialogueStrings"> strings of the dialogue</param>
    /// <param name="timePerString">time to wait for every dialogue</param>
    /// <returns></returns>
    protected IEnumerator StartTheDialogue(string[] dialogueStrings, float[] timePerString)
    {
        hasTalked = true;
        disappearScript.ActivateDialogueElements();
        for (int i = 0; i < dialogueStrings.Length; i++)
        {
            //dialogueText.text = dialogueStrings[i];
            wordWriter.StartWrite(dialogueStrings[i]);
            yield return new WaitForSeconds(timePerString[i]);
        }
        disappearScript.Disappear();
        hasTalked = false;
    }
}
