﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that manages the talk for the talk mechanic scene
/// </summary>
public class TalkMechanicScene : _TalkNPC {

    [Header("The strings of the dialogue when the player hasn't taken the first document yet")]
    public string[] dialogueStrings;

    public float[] timePerString;

    public DoorLevelInteractionScript door;

    private bool alreadyTalked=false;

    /// <summary>
    /// Overridden method
    /// </summary>
    /// <param name="player"></param>
    public override void Talk(GameObject player)
    {
        if (hasTalked)
            return;

        if (!alreadyTalked)
        {
            door.isLocked = false;
            alreadyTalked = true;
        }
            StartCoroutine(StartTheDialogue(dialogueStrings, timePerString));
    }
}
