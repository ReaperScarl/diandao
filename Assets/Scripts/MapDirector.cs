﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This class will manage the showing areas of the level
/// </summary>
public class MapDirector : MonoBehaviour {

    [Header("These area the areas to put to visible or invisible")]
    public GameObject[] areas;

    [Header("The areas that are visible at the start")]
    public int[] startingAreasVisible;

	// Use this for initialization
	void Start () {

        int index = 0;
        int i;
        for(i=0; i<areas.Length && index<startingAreasVisible.Length; i++)
        {
            if (i == startingAreasVisible[index])
            {
                areas[i].SetActive(true);
                Debug.Log("areas " + i + ": " + areas[i].activeSelf);
                index++;
            }
            else
            {
                areas[i].SetActive(false);
                Debug.Log("areas " + i + ": " + areas[i].activeSelf);
            }
                
        }

        for(; i<areas.Length; i++)
        {
            areas[i].SetActive(false);
            Debug.Log("areas " + i + ": " + areas[i].activeSelf);
        }
	}
	
    /// <summary>
    /// This will change the map disabling the areas that are not active.
    /// </summary>
    /// <param name="newAreas"></param>
    public void ChangeVisibility(int [] newAreas)
    {
        int index = 0;
        int i;
        for ( i = 0; i < areas.Length && index < newAreas.Length; i++)
        {
            
            if (i == newAreas[index])
            {
                areas[i].SetActive(true);
                //Debug.Log("areas "+i+": "+areas[i].activeSelf);
                index++;
            }
            else
                areas[i].SetActive(false);
        }
        for (; i < areas.Length; i++)
        {
            areas[i].SetActive(false);
            // Debug.Log("areas " + i + ": " + areas[i].activeSelf);
        }
    }
}
