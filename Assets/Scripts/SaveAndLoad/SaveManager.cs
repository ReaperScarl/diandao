﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is responsible for saving the scene.
/// This script should be attached to the SaveManager in the scene which should be saved.
/// </summary>
public class SaveManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		SaveAndLoad.SaveGame ();
	}
}
