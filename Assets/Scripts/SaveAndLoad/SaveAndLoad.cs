﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// This class is repsonsible for saving and loading game.
/// </summary>
public class SaveAndLoad : MonoBehaviour {

	private static Dictionary<int, string> SceneToLevel = new Dictionary<int, string>()
	{
		{0, "MainMenu"},
		{1, "Level1A"},
		{2, "Level1A"},
		{3, "Level2"},
		{4, "Level3"}
	};

	public static void SaveGame(){
		int currentSceneIndex = SceneManager.GetActiveScene ().buildIndex;
		PlayerPrefs.SetInt ("Current Scene Index", currentSceneIndex);

		// Debug.Log("Game Saved!");
	}

	public static void ResumeGame(){
		int currentSceneIndex;
		if (PlayerPrefs.HasKey ("Current Scene Index"))
			currentSceneIndex = PlayerPrefs.GetInt ("Current Scene Index");
		else
			currentSceneIndex = 1;

		// Debug.Log ("Resume Game");

		if (SceneToLevel.ContainsKey (currentSceneIndex))
			SceneManager.LoadScene (SceneToLevel [currentSceneIndex]);
		else
			SceneManager.LoadScene ("Level1A");
		}
}
