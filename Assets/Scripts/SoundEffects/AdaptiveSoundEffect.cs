﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class will manage the sound effects that need to change based on the distance from the player
/// </summary>
public class AdaptiveSoundEffect : MonoBehaviour {

    [Header("The player transform")]
    public Transform player;

    [Header("The distance from where it starts to play something (going inside)")]
    public float minVolDistance;

    [Header("The distance from where it starts to play at max Volume. Must be lower than minVolumeDistance. ")]
    public float maxVolDistance;

    [Header("The volume that this sound should have. If it is too strong, put it to <1")]
    [Range(0,1)]
    public float relativeVolume = 1;


    private WaitForSeconds wait = new WaitForSeconds(0.1f);
    private AudioSource audioToPlay;
    private Vector3 position;
    private float generalVolume;

	// Use this for initialization
	void Start () {
        audioToPlay= this.GetComponent<AudioSource>();
        StartCoroutine(AdaptVolume());
        position = this.transform.position;
        generalVolume = PlayerPrefs.GetFloat("soundEffectVolume");
	}
	
	/// <summary>
    ///  Changes the volume based on the movement of the player
    /// </summary>
    IEnumerator AdaptVolume()
    {
        while (this)
        {
            float distance = Vector2.Distance(position, player.transform.position);
            ChangeVolume(distance);
            yield return wait;
        }
    }

    /// <summary>
    /// This will be used when a change of volume is made in game
    /// </summary>
    /// <param name="value"> the new general volume</param>
    public void ChangedVolume(float value)
    {
        generalVolume = value;
        float distance = Vector2.Distance(position, player.transform.position);
        ChangeVolume(distance);
    }

    /// <summary>
    /// this will change the volume when called
    /// </summary>
    /// <param name="distance"> the distance between the player and the object</param>
    private void ChangeVolume(float distance)
    {
        if (distance > minVolDistance)
            audioToPlay.volume = 0;
        else
        {
            if (distance <= minVolDistance && distance > maxVolDistance)
                audioToPlay.volume = CalculateTheLinDistance(distance) * generalVolume * relativeVolume;
            else
                audioToPlay.volume = 1 * generalVolume * relativeVolume;
        }
    }

    /// <summary>
    /// calculated the distance
    /// </summary>
    /// <param name="distance"> distance between the player and the object of the sound effect</param>
    /// <returns></returns>
    private float CalculateTheLinDistance(float distance)
    {
        return (minVolDistance - distance) / (minVolDistance - maxVolDistance);
    }
}
