﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEffect : MonoBehaviour {

	public AudioClip bees;
	public AudioClip crow;
	public AudioClip bird;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnButtonBees(){
		SoundEffectManager.Instance.PlayClip (bees);
	}

	public void OnButtonCrow(){
		SoundEffectManager.Instance.PlayClip (crow);
	}

	public void OnButtonBird(){
		SoundEffectManager.Instance.PlayClip (bird);
	}
}
