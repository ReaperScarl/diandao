﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSlideEffect : SlideEffect {

	// ------	Public Variables	------
	public float rotatingSpeed = 20.0f;

	// Use this for initialization
	protected virtual void Start () {
		base.Start ();
	}
	
	protected override void Update(){
		base.Update ();

		if (Mathf.Abs (rb2d.velocity.x) > 0.1f)
			transform.Rotate(new Vector3(0, 0, -rotatingSpeed * Time.deltaTime * Mathf.Sign (rb2d.velocity.x)));
	}
}
