﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootStepsEffect : MonoBehaviour {

    public AudioClip[] footSteps;

    private CharacterMovement cM;

    private bool activation = true;

    /// <summary>
    /// This method is called when the player
    /// enters the collider held by the gameobject whose 
    /// script is attached. It sends a set of
    /// sound effect used to update the footsteps 
    /// of the player
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.tag == "player")
        {
            if (!cM)
                cM = collision.GetComponent<CharacterMovement>();
            cM.StartingSoundEffect(footSteps);
        }   
    }

    /// <summary>
    /// This method is called when the player
    /// stays inside the collider held by the gameobject whose 
    /// script is attached. It avoids to call
    /// again the method to play footsteps sound effect
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "player" && activation)
        {
            if (!cM)
                cM = collision.GetComponent<CharacterMovement>();
            activation = false;
            cM.StartingSoundEffect(footSteps);
        }
    }

    /// <summary>
    /// This method is called when the player
    /// eexits the collider held by the gameobject whose 
    /// script is attached. It makes available to
    /// call again the method to play footsteps 
    /// sound effect
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        activation = true;
    }
}
