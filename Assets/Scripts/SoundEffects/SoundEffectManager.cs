﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is responsible for sound effect manager.
/// This script should be attached to sound effect manager.
/// </summary>
public class SoundEffectManager : MonoBehaviour {

	// Global Singleton Accessor;
	public static SoundEffectManager Instance;

	private List<AudioSource> pool = new List<AudioSource> ();
	private float volume;

	void Awake()
	{
		if (Instance != null)
			Destroy (gameObject);
		else
			Instance = this;

		DontDestroyOnLoad (Instance);
	}

	// Use this for initialization
	void Start () 
	{
		volume = PlayerPrefs.GetFloat ("soundEffectVolume");
	}

	/// <summary>
	/// Play the clip.
	/// This function is called by any other scripts needed to play a sound effect.
	/// </summary>
	/// <param name="ac">Audio clip to play</param>
	public void PlayClip(AudioClip ac, float pitch=1)
	{
		StartCoroutine (PlayAndReturn (ac, pitch));
	}

	/// <summary>
	/// Changes the volume by other scripts.
	/// </summary>
	/// <param name="_volume">Volume.</param>
	public void ChangeVolume(float _volume)
	{
		volume = _volume;

		foreach (AudioSource aus in GetComponents<AudioSource>()) 
		{
			if (aus.enabled == true)
				aus.volume = volume;
		}
	}

	/// <summary>
	/// Get an audio from the pool, play the clip, and return to the pool.
	/// </summary>
	/// <param name="ac">Audio clip to play</param>
	private IEnumerator PlayAndReturn(AudioClip ac, float pitch)
	{
		AudioSource audioSource = GetAS ();
		audioSource.pitch = pitch;
		audioSource.PlayOneShot (ac, volume);

		yield return new WaitForSeconds (ac.length);

		ReturnAS (audioSource);
	}

	/// <summary>
	/// Get an audio source from the pool.
	/// If there is no available source, just create one.
	/// </summary>
	/// <returns>Audio Source</returns>
	private AudioSource GetAS()
	{
		if (pool.Count != 0) 
		{
			AudioSource audioSource = pool [0];
			audioSource.enabled = true;
			pool.RemoveAt (0);
			return audioSource;
		} 
		else 
		{
			return gameObject.AddComponent<AudioSource> ();
		}
	}

	/// <summary>
	/// Return the audio source to the pool.
	/// </summary>
	/// <param name="audioSource">Audio source</param>
	private void ReturnAS(AudioSource audioSource)
	{
		audioSource.enabled = false;
		pool.Add (audioSource);
	}

}
