﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpEffect : MonoBehaviour {

	// ------	Public Variables	------
	public AudioClip jumpClip;

	// ------	Private Variables	------
	private CharacterJump cj;

	/// <summary>
	/// Set corresponding clip when enter the trigger
	/// </summary>
	public void OnTriggerEnter2D(Collider2D collider){
		if (collider.tag == "player") {
			cj = collider.GetComponent<CharacterJump> ();
			cj.SetClip (jumpClip);
		}
	}
}
