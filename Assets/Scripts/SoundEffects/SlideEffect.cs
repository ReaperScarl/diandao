﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The script is responsible for slide sound effect on movable objects.
/// </summary>
[RequireComponent(typeof(Rigidbody2D), typeof(AudioSource))]
public class SlideEffect : MonoBehaviour {

	// ------	Public Variables	------
	public AudioClip slideClip;
	public AudioSource audioSource;

	// ------	Required Component	------
	protected Rigidbody2D rb2d;
	private bool isPlaying;

	// Use this for initialization
	protected virtual void Start () {
		audioSource.loop = true;
		audioSource.clip = slideClip;

		rb2d = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		if (Mathf.Abs (rb2d.velocity.x) > 0.1f && !audioSource.isPlaying)
			audioSource.Play ();

		if (Mathf.Abs (rb2d.velocity.x) < 0.1f && audioSource.isPlaying)
			audioSource.Stop ();
	}
}
