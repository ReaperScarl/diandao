﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manages the rotation of the carriage and its interaction
/// </summary>
public class CarriageInteraction : _Interactions {

    //how much faster the carriage should be to reach the final rotation
    public float timeOfRotation;

    //the carriage itself
    public GameObject carriage;

    //the final rotation reachable by the carriage
    public float finalRot;

    public bool isActive = true;
    [Space(10)]
    [Header("The clip player when the player interacts with the carriage")]
    public AudioClip onInteractionSoundEffect;

    public float forceToAdd = 5;

    private Rigidbody2D rb2d;


    protected override void Start()
    {
        rb2d=carriage.GetComponent<Rigidbody2D>();
        rb2d.velocity = Vector3.zero;
        base.Start();
    }



    /// <summary>
    /// Override of the method Action.
    /// This turn down the carriage to let the character goes on
    /// </summary>
    public override void Action()
    {
        if (isActive)
        { 
            StartCoroutine(Rotation());
            isActive = false;
            coll.enabled = false;
        }
    }

    /// <summary>
    /// This method applies the rotation 
    /// to the carriage
    /// </summary>
    /// <returns></returns>
    private IEnumerator Rotation()
    {
        rb2d.bodyType = RigidbodyType2D.Dynamic;
        float timeToRotate = 0;
        float initialRot = carriage.transform.rotation.eulerAngles.z;
        float tempRotation;

        float epsilon1 = 0;
        float epsilon2 = 254F; //value if we want to reach -105

        if(Math.Abs(finalRot) > 105 && finalRot < 0)
        {
            epsilon2 = epsilon2 - (105 - finalRot);
        }

        while(Mathf.Abs(carriage.transform.rotation.eulerAngles.z - 360F) -Math.Abs(finalRot) >= epsilon2)
        {
            rb2d.AddForce(new Vector2(forceToAdd, 0F));
            yield return null;
        }

        while (Mathf.Abs(carriage.transform.rotation.eulerAngles.z - 360F) - Math.Abs(finalRot) < epsilon1)
        {
            rb2d.AddForce(new Vector2(forceToAdd, 0F));
            yield return null;
        }
        if (onInteractionSoundEffect)
            SoundEffectManager.Instance.PlayClip(onInteractionSoundEffect);
        rb2d.bodyType = RigidbodyType2D.Static;
    }
}
