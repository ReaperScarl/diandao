﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manages the rotation 
/// of the dead tree
/// </summary>
public class DeadTreeMovement : MonoBehaviour {

    [Header("How much force should be added to rotate the dead tree")]
    [Range(40F, 150F)]
    public float forceToAdd;

    [Header("Until which degrees the force should be applied")]
    public float rotationBorder;

    [Header("The clip to play when the tree is falling")]
    public AudioClip fallingClip;
    [Header("The clip to play when the tree finishes the fall")]
    public AudioClip endOfFallClip;

    private Rigidbody2D rb2d;

    public bool toActiveFall;

    public bool toActiveEndFall;
    
    [Header("This is the gameObject that stops the dead tree from keeping falling")]
    public GameObject endOfFallObject;

	// Use this for initialization
	void Start () {
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        toActiveFall = true;
        toActiveEndFall = true;
	}

    /// <summary>
    /// This method is called when
    /// the player collides with the
    /// dead tree
    /// </summary>
    /// <param name="collider"></param>
    public void OnCollisionEnter2D(Collision2D collider)
    {
        if (collider.gameObject.tag == "player" && toActiveFall)
        {
            toActiveFall = false;
            StartCoroutine(ActivatingDeadTree());
        }
        if (collider.gameObject == endOfFallObject && toActiveEndFall)
        {
            toActiveEndFall = false;
            SoundEffectManager.Instance.PlayClip(endOfFallClip);
            rb2d.bodyType = RigidbodyType2D.Static;
        }
    }

    /// <summary>
    /// This method adds a force to rotate
    /// the dead tree
    /// </summary>
    /// <returns></returns>
    private IEnumerator ActivatingDeadTree()
    {
        float borderValue = -330F;
        if(rotationBorder < 0 && rotationBorder > -10F)
        {
            borderValue = borderValue + rotationBorder;
        }
        if (rotationBorder < 0 && rotationBorder < -10F)
        {
            borderValue = borderValue + Mathf.Abs(rotationBorder);
        }

        SoundEffectManager.Instance.PlayClip(fallingClip);
        while(Mathf.Abs(rotationBorder) - gameObject.transform.rotation.eulerAngles.z > 0 ||
            Mathf.Abs(rotationBorder) - gameObject.transform.rotation.eulerAngles.z < borderValue)
        {
            rb2d.AddForce(new Vector2(forceToAdd, 0F));
            yield return new WaitForEndOfFrame();
        }
    }
}
