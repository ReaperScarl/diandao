﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorMovement : MonoBehaviour {

    [Header("Speed of the elevator")]
    [Range(1F, 10F)]
    public float movementElevatorSpeed;

    [Header("Distance of the elevator")]
    public float distanceElevator;

    private AudioSource audioSource;

    private bool goingUp;

    private ButtonElevatorInteraction controller;
    private CharacterMovement cM;
    private RopePlayer rP;

    private float startingPositionElevator;
    
    private Rigidbody2D rb2d;

	// Use this for initialization
	void Start () {
        startingPositionElevator = transform.position.y;
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        goingUp = true;
        audioSource = gameObject.GetComponent<AudioSource>();
        cM = GameObject.FindGameObjectWithTag("player").GetComponent<CharacterMovement>();
        rP = GameObject.FindGameObjectWithTag("player").GetComponent<RopePlayer>();
    }

    /// <summary>
    /// This method is called after
    /// pressing the button
    /// </summary>
    public void ButtonCall(ButtonElevatorInteraction script)
    {
        controller = script;
        cM.movementEnabled = false;
        cM.Stop();
        rP.activated = false;
        cM.GetComponent<EnableController>().DisableAll();
        cM.transform.SetParent(this.transform);
        cM.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
        StartingMovement();
    }

    /// <summary>
    /// This method is called to start the
    /// movement procedure
    /// </summary>
    private void StartingMovement()
    {
        StartCoroutine(Movement());
    } 

    /// <summary>
    /// This method stops the elevator
    /// </summary>
    public void StopMoving()
    {
        cM.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        cM.transform.SetParent(null);
        rb2d.velocity = Vector3.zero;
        cM.movementEnabled = true;
        rP.activated = true;
        cM.GetComponent<EnableController>().EnableAll();
        if (controller)
            controller.ButtonReactiving();
    }

    /// <summary>
    /// This elevator activate the
    /// movement of the elevator
    /// </summary>
    /// <returns></returns>
    private IEnumerator Movement()
    {
        audioSource.loop = true;
        audioSource.Play();
        if (goingUp)
        {
            rb2d.velocity = new Vector3(0, movementElevatorSpeed, 0);

            while (transform.position.y < (startingPositionElevator + distanceElevator))
            {
                yield return null;
            }

            StopMoving();
            goingUp = false;
        }
        else // going down
        {
            rb2d.velocity = new Vector3(0, -movementElevatorSpeed, 0);

            while (transform.position.y > (startingPositionElevator))
            {
                yield return null;
            }

            StopMoving();
            goingUp = true;
        }
        audioSource.Stop();
    }

    /// <summary>
    /// This method is called when
    /// the player has spoken with the
    /// father in a certain moment of the game
    /// </summary>
    public void FatherCall()
    {
        StartingMovement();
    }
	
}
