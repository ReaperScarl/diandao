﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is used to open the door of the 
/// elevator pipe
/// </summary>
public class OpeningElevatorPipe : MonoBehaviour {

    [Header("Speed of the gates to open/close")]
    [Range(1.0f, 10.0f)]
    public float speedGate;

    [Header("End position of the left door of the elevator gate")]
    public Vector3 endOfLGate;

    [Header("End position of the right door of the elevator gate")]
    public Vector3 endOfRGate;

    [Header("Left door of the elevator gate")]
    public GameObject lGate;

    [Header("Right door of the elevator gate")]
    public GameObject rGate;

    [Header("The sound effect to be played when the gate is opening")]
    public AudioClip openingGateClip;

    private bool isActive;

    void Start()
    {
        isActive = false;
    }

    /// <summary>
    /// This method is called by the father
    /// It start the process to open the pipe,
    /// if not already called
    /// </summary>
    public void ActivatingPipe()
    {
        if (!isActive)
        {
            isActive = true;
            StartCoroutine(OpeningPipe());
        }
    }

    /// <summary>
    /// This method manges the opening of the 
    /// elevator gate/pipe
    /// </summary>
    /// <returns></returns>
    private IEnumerator OpeningPipe()
    {
        SoundEffectManager.Instance.PlayClip(openingGateClip);
        while (lGate.transform.position.x != endOfLGate.x && rGate.transform.position.x != endOfRGate.x)
        {
            lGate.transform.position = Vector3.MoveTowards
                (lGate.transform.position, endOfLGate, speedGate * Time.deltaTime);
            rGate.transform.position = Vector3.MoveTowards
            (rGate.transform.position, endOfRGate, speedGate * Time.deltaTime);
            yield return null;
        }
    }
}
