﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Platform that is weight sensed (Controlled by the button).
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class WeightSensePlatform : MonoBehaviour {

	// ------	Public Variables	------
	public float moveDistance = 5f;
	public float moveSpeed = 5f;

    [Header ("The part that will appear only when the plaform passes the 2 meters")]
    public GameObject scaffold;

	public bool isButtonPressed = false;

    public AudioClip platformSoundEffect;
   
    // ------	Private Variables	------

    private bool isAwake;
    private WaitForSeconds delay;
    private AudioSource audioS;

    // ------	Required Component
    private Rigidbody2D rb2d;
	private float vel;
	private float startY;
	private float endY;

	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();

		vel = 0f;
		startY = transform.position.y;
		endY = startY - moveDistance;
        delay = new WaitForSeconds(0.1f);
        audioS = GetComponent < AudioSource>();
        audioS.clip = platformSoundEffect;
	}

    /// <summary>
    /// The IEnumerator will move the platform.
    /// This will also check for the distance between the scaffold and the platform to activate and deactivate it
    /// </summary>

    IEnumerator MoveThePlatform()
    {
        while (isAwake)
        {
            if(!audioS.isPlaying)
                StartSoundEffect();
            if (isButtonPressed)
            {
                if (transform.position.y > endY)
                    vel = -moveSpeed;
                else
                {
                    vel = 0f;
                    StopSoundEffect();
                }
                    
            }
            else
            {
                if (transform.position.y < startY)
                    vel = moveSpeed;
                else
                {
                    vel = 0f;
                    StopSoundEffect();
                }       
            }

            if (Mathf.Abs(transform.position.y - startY) > 2.5 && !scaffold.activeSelf)
                scaffold.SetActive(true);
            if(Vector2.Distance(transform.position,scaffold.transform.position ) < 2.5 && scaffold.activeSelf)
                scaffold.SetActive(false);

            rb2d.velocity = Vector2.up * vel;
            yield return delay;
        }
        StartSoundEffect();
        while (!isAwake&& transform.position.y < startY)
        {
            rb2d.velocity = Vector2.up * moveSpeed;
            yield return delay;
            if (Vector2.Distance(transform.position, scaffold.transform.position) < 2.5 && scaffold.activeSelf)
                scaffold.SetActive(false);
        }
        rb2d.velocity = Vector2.up * 0;
        StopSoundEffect();
    }

    /// <summary>
    /// This is called by the button.
    /// It wakes up the platform
    /// </summary>
    public void WakeUp()
    {
        isAwake = true;
        StartCoroutine(MoveThePlatform());
    }

    /// <summary>
    /// This is called by the button.
    /// It makes to stop the platform
    /// </summary>
    public void Sleep()
    {
        isAwake = false;
    }

    /// <summary>
    /// This will start the sound effect of the platform
    /// </summary>
     private void StartSoundEffect()
    {
        audioS.Play();
    }

    /// <summary>
    /// This will stop the sound effect of the platform
    /// </summary>
    private void StopSoundEffect()
    {
        audioS.Stop();
    }



}
