﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Weight sensor button.
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public class WeightSensorButton : _Interactions {

	// ------	Public Variables	------
	public Sprite glowButton;
    public Sprite noGlowButton;
	public WeightSensePlatform platform;
    [Header("The sensor where it should be the mass")]
    public Collider2D weightSensorCollider;
	

	// ------	Private Variables	------
	public bool isPress;
    private bool isSomethingInRange=false;
    private GameObject rock;
    private PlayerController controller;
    private WaitForSeconds delay;

    // ------	Required Components	------
    private SpriteRenderer sr;

	// Use this for initialization
	protected override void Start () {
		base.Start ();

		sr = GetComponent<SpriteRenderer> ();
        delay = new WaitForSeconds(0.1f);
		isPress = false;
        if (!PlayerPrefs.HasKey("glowyThings") || PlayerPrefs.GetInt("glowyThings") == 1) //glowy active
            sr.sprite = glowButton;
        else
            sr.sprite = noGlowButton;
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if(!controller)
            controller = collision.GetComponent<PlayerController>();
        base.OnTriggerEnter2D(collision);
        //Debug.Log("On enter");
        isSomethingInRange = true;
        platform.WakeUp();
        StartCoroutine(SenseWeightOnButton());
    }

    /// <summary>
    /// Instantiate a rock in the button's position and set the body type static.
    /// </summary>
    public override void Action () {
        if (!rock)
        {
            rock = Instantiate(controller.DropRock(), transform.position+new Vector3(0,+0.4f,0), Quaternion.identity);
            rock.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
        }
	}

    protected override void OnTriggerExit2D(Collider2D collision)
    {
        
        //Check if there is still something in there
        isSomethingInRange = AreCollidersInRange();
        if (!isSomethingInRange)
            platform.Sleep();
        base.OnTriggerExit2D(collision);

    }

    /// <summary>
    /// Sense whether there is an object with mass on.
    /// </summary>
    private void SenseWeight(){
		Collider2D[] colliders = Physics2D.OverlapCircleAll (transform.position,0.35f); // the 0.2 is to change accordingly with the scale of the button
		foreach (Collider2D collider in colliders) {
			if (collider.GetComponent<Rigidbody2D> () && weightSensorCollider.IsTouching(collider)) {
                if (!isPress) {
                    PressOn();
				}
				return;
			}
		}

		if (isPress) {
            PressOff();
		}
	}

    /// <summary>
    /// This ienumerator will start when something is on the collider. 
    /// It will stop when there is nothing on the sensor anymore
    /// </summary>
    IEnumerator SenseWeightOnButton()
    {
        while (isSomethingInRange)
        {
            SenseWeight();
            yield return delay;
        }
        PressOff();
    }

    /// <summary>
    /// it presses to one the button and call and change the platform to true
    /// </summary>
    private void PressOn()
    {
        isPress = true;
        platform.isButtonPressed = true;
    }

    //it presses to one the button and call and change the platform to false
    private void PressOff()
    {
        isPress = false;
        platform.isButtonPressed = false;
    }

    private bool AreCollidersInRange()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.2f); // the 0.2 is to change accordingly with the scale of the button
        foreach (Collider2D collider in colliders)
        {
            if (collider.GetComponent<Rigidbody2D>() && weightSensorCollider.IsTouching(collider))
            {
                return true;
            }
        }
        return false;
    }
    
}
