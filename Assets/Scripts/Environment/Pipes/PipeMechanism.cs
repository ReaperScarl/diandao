﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeMechanism : MonoBehaviour {

    [Header("Speed of the gates to open/close")]
    [Range(1.0f, 10.0f)]
    public float speedGate;

    [Header("Time while sucking air")]
    [Range(1.0f, 15.0f)]
    public float timeSucking;       //how much time the air should be sucked

    [Header("Time for the mechanism to open and/or close")]
    [Range(1.0f, 10.0f)]
    public float timeOpenClos;      //how much time is given to the gate to open/close

    public GameObject lGate;        //the left part of the pipe gate

    public GameObject rGate;        //the right part of the pipe gate

    public GameObject[] suckingZone;//the zone where the air is sucked

    public GameObject endOfLGate;

    public GameObject endOfRGate;

    public GameObject startOfLGate;

    public GameObject startOfRGate;

    [Header("The sound effect to be played when the gate is opening")]
    public AudioClip openingClosingGateClip;

    [Header("The sound effect to be played when the air is sucking")]
    public AudioClip fanActivatingClip;

    private AudioSource fanActivatingAS;

    // Use this for initialization
    void Start () {
        fanActivatingAS = gameObject.GetComponent<AudioSource>();
        fanActivatingAS.clip = fanActivatingClip;
    }

    /// <summary>
    /// This method is called by an interactible object,
    /// whose task is to activate the pipe
    /// </summary>
    public void ActivatingPipe()
    {
        StartCoroutine(SuckAir());
    }

    private IEnumerator SuckAir()
    {
        StartCoroutine(OpeningGate());                                  //opening the pipe

        for(int i = 0; i < suckingZone.Length; i++)
            suckingZone[i].GetComponent<Collider2D>().enabled = true;

        fanActivatingAS.Play();
        yield return new WaitForSeconds(timeSucking);    //sucking

        for (int i = 0; i < suckingZone.Length; i++)
            suckingZone[i].GetComponent<Collider2D>().enabled = false;  //finish sucking

        StartCoroutine(ClosingGate());
        fanActivatingAS.Stop();
        yield return new WaitForSeconds(timeOpenClos);                  //closing the pipe

    }

    private IEnumerator OpeningGate()
    {
        SoundEffectManager.Instance.PlayClip(openingClosingGateClip);
        while (lGate.transform.position != endOfLGate.transform.position &&
                rGate.transform.position != endOfRGate.transform.position)
        {
            lGate.transform.position = Vector2.MoveTowards
                (lGate.transform.position, endOfLGate.transform.position, speedGate * Time.deltaTime);
            rGate.transform.position = Vector2.MoveTowards
            (rGate.transform.position, endOfRGate.transform.position, speedGate * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator ClosingGate()
    {
        SoundEffectManager.Instance.PlayClip(openingClosingGateClip);
        while (lGate.transform.position != startOfLGate.transform.position &&
                rGate.transform.position != startOfRGate.transform.position)
        {
            lGate.transform.position = Vector2.MoveTowards
                (lGate.transform.position, startOfLGate.transform.position, speedGate * Time.deltaTime);
            rGate.transform.position = Vector2.MoveTowards
                (rGate.transform.position, startOfRGate.transform.position, speedGate * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    // Update is called once per frame
    void Update () {

	}
}
