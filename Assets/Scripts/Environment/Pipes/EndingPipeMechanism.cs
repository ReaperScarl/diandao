﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The script responsible for the autoclosure of the pipe
/// </summary>
public class EndingPipeMechanism : MonoBehaviour {

    [Header("Speed of the final gate")]
    [Range(1.0f, 10.0f)]
    public float gateSpeed;

    public GameObject closingRGatePos;

    public GameObject closingLGatePos;

    public GameObject lEndGate;

    public GameObject rEndGate;

    [Header("The clip to be played when the gate is closing")]
    public AudioClip closingGateClip;

	// Use this for initialization
	void Start () {
		
	}

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag=="player" && collision.transform.position.y < gameObject.transform.position.y)
            StartCoroutine(ClosingPipe());
    }

    private IEnumerator ClosingPipe()
    {
        SoundEffectManager.Instance.PlayClip(closingGateClip);
        while (lEndGate.transform.position != closingLGatePos.transform.position &&
                rEndGate.transform.position != closingRGatePos.transform.position)
        {
            lEndGate.transform.position = Vector2.MoveTowards
                (lEndGate.transform.position, closingLGatePos.transform.position, gateSpeed * Time.deltaTime);
            rEndGate.transform.position = Vector2.MoveTowards
                (rEndGate.transform.position, closingRGatePos.transform.position, gateSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }
}
