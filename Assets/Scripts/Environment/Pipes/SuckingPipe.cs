﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class manages the speed given to Xia
/// when she is sucked by the pipe. This velocity is
/// given only when the mechanism is activated
/// </summary>
public class SuckingPipe : MonoBehaviour {

    [Header("Speed of the sucked air")]
    [Range(10.0f, 100.0f)]
    public float speed;            

    [Header("Xia")]
    public GameObject player;

    private bool hasDress;           //if Xia has dresses on ornot

    private Rigidbody2D rbXia;       //Xia's rigidbody

    private float tempVelY;          //I need it to store the original x-velocity of Xia

    [Header("How much times the speed of the sucked air should be multiplied")]
    [Range(10.0f, 100.0f)]
    public float forceSuckXMultiplier;

    [Header("If the player is in the Y zone of the air-sucking")]
    public bool isInTriggerY;

    [Header("If the player is in the X zone of the air-sucking")]
    public bool isInTriggerX;

    // Use this for initialization
    void Start ()
    {
        rbXia = player.GetComponent<Rigidbody2D>();
        tempVelY = rbXia.velocity.y;
    }
	
	// Update is called once per frame
	void Update ()
    {

	}

    /// <summary>
    /// when Xia enters in the collider, she is sucked
    /// </summary>
    /// <param name="collision">the collider triggering the trigger</param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        hasDress = player.GetComponent<PlayerController>().hasTheDress; 

        if (collision.tag == "player" && !hasDress &&  isInTriggerY) 
        {
            rbXia.velocity = new Vector2(rbXia.velocity.x, speed);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "player" && !hasDress && isInTriggerX)
        {   
            rbXia.AddForce(transform.right * speed * forceSuckXMultiplier);
        }
    }

    /// <summary>
    /// when the collider is disabled/ the player goes out of the collider, initial velocity is restored
    /// </summary>
    /// <param name="collision">the collider triggering the trigger</param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (isInTriggerY)
        {
            rbXia.velocity = new Vector2(rbXia.velocity.x, tempVelY);
        }
    }
}
