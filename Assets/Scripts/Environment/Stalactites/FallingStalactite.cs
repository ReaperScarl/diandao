﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manages the fall of 
/// the stalacatite
/// </summary>
public class FallingStalactite : MonoBehaviour {

    [Header("How much is the delay before falling")]
    [Range(0F, 10F)]
    public float delay;

    [Header("How faster the stalactite will fall")]
    [Range(1F, 20F)]
    public float speedOfFalling;

    [Header("y-point of the final position")]
    public float endOfTheFall;

    [Header("Death Manager")]
    public DeathMechanic deathManager;

    public bool isActive;

    [Header("Clip to be played when the player dies")]
    public AudioClip deathClip;

    [Header("Clip to be played when the stalactite breaks from the ceiling")]
    public AudioClip breakStalactiteClip;

    [Header("Clip to be played when the stalactite ends the fall")]
    public AudioClip endOfFallClip;

    [Header("If true, the player will die when the element is touched; otherwise, false")]
    private bool isDeadly;

    //If true, the clip will not be played again
    private bool isAlreadyPlayed;

    private Rigidbody2D rb2d;

    /// <summary>
    /// This method is called by an external one
    /// in a portal. It makes start the fall of 
    /// the stalactite which holds this script
    /// </summary>
    public void StartingFall()
    {
        StartCoroutine(Falling());
    }

    /// <summary>
    /// This method has the duty
    /// to control the fall of the 
    /// stalactite
    /// </summary>
    private IEnumerator Falling()
    {
        isAlreadyPlayed = false;
        isActive = true;
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        isDeadly = true;
        yield return new WaitForSeconds(delay);
        SoundEffectManager.Instance.PlayClip(breakStalactiteClip);
        while ((gameObject.transform.position.y > endOfTheFall) && isActive)
        {
            rb2d.velocity = new Vector2(0, -speedOfFalling);
            yield return new WaitForEndOfFrame();
        }
        isDeadly = false;
        if (!isAlreadyPlayed)
        {
            SoundEffectManager.Instance.PlayClip(endOfFallClip);
        }
        rb2d.bodyType = RigidbodyType2D.Static;
    }

    /// <summary>
    /// This method is called when the player touches
    /// the stalactite. If it's falling, the player dies
    /// </summary>
    /// <param name="collision"> the element that touched</param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "player" && isDeadly && !deathManager.isDead)
        {
            deathManager.PlayerDie();
            SoundEffectManager.Instance.PlayClip(endOfFallClip);
            isAlreadyPlayed = true;
            SoundEffectManager.Instance.PlayClip(deathClip);
        }
    }

}
