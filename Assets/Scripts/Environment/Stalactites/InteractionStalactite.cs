﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class manages the interaction
/// activity of the player with the stalactite
/// </summary>
public class InteractionStalactite : _Interactions
{
    [Header("sprite of the stalactite when it becames not interactible")]
    public Sprite NotIntStalsprite;

    [Header("sprite of the stalactite when it is interactible")]
    public Sprite IntStalsprite;

    //variable to control the start of interaction 
    private bool isActive = true;

    //the rigid body of the game Object
    private Rigidbody2D rb2d;


    private SpriteRenderer sRenderer;

    protected override void Start()
    {
        base.Start();
        
            rb2d = GetComponent<Rigidbody2D>();
        sRenderer = GetComponent<SpriteRenderer>();
        if (!PlayerPrefs.HasKey("glowyThings") || PlayerPrefs.GetInt("glowyThings") == 1) //glowy active
            sRenderer.sprite = IntStalsprite;
        else
            sRenderer.sprite = NotIntStalsprite;
    }

    /// <summary>
    /// This method makes start the fall
    /// of the stalactite
    /// </summary>
    public override void Action()
    {
        if (isActive)
        {
            gameObject.GetComponent<FallingStalactite>().StartingFall();
            isActive = false;
            base.coll.enabled = false;
            sRenderer.sprite = NotIntStalsprite;
        }
       
    }

    /// <summary>
    /// This is method is called to reset the interaction
    /// </summary>
    public void Reset()
    {
        if (!isActive)
        {
            isActive = true;
            base.coll.enabled = true;
            rb2d.bodyType = RigidbodyType2D.Kinematic;
            rb2d.velocity = Vector2.zero;
            if (!PlayerPrefs.HasKey("glowyThings") || PlayerPrefs.GetInt("glowyThings") == 1) //glowy active
                sRenderer.sprite = IntStalsprite;
        }
    }
}
