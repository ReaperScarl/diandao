﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manages death event 
/// when player touches a certain part 
/// of the stalactite
/// </summary>
public class StalactitePeak : MonoBehaviour {

    [Header("Death Manager")]
    public DeathMechanic deathManager;

    [Header("Clip to be played when the player dies")]
    public AudioClip deathClip;

    /// <summary>
    /// Trigger when player touches the tip 
    /// of the stalactite
    /// </summary>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "player" && !deathManager.isDead)
        {
            deathManager.PlayerDie();
            SoundEffectManager.Instance.PlayClip(deathClip);
        }
           
    }
}
