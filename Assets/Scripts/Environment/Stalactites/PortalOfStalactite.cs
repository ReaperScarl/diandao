﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class inherites the _PortalScript
/// script, in order to activate a script only
/// if a certain condition is triggered by
/// the player
/// </summary>
public class PortalOfStalactite : _PortalScript {

    public bool isActive;

    [Header("Set of stalactites")]
    public GameObject[] stalactites;

    /// <summary>
    /// This method makes start each stalactite
    /// to fall down. When the for clause is complete,
    /// the behaviour of the script is disabled
    /// </summary>
    protected override void Activate()
    {
        if (isActive)
        {
            isActive = false;
            for (int i=0; i < stalactites.Length; i++)
            {
                stalactites[i].GetComponent<FallingStalactite>().StartingFall();
            }
        }
    }
}
