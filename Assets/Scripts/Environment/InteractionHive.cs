﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manages the interaction
/// between player and hive
/// </summary>
public class InteractionHive : _Interactions {

    //variable to control the start of interaction 
    public bool isActive = true;

    [Header("The clip to play when the player interacts with the hive")]
    public AudioClip hiveClip;

    //the rigid body of the game Object
    private Rigidbody2D rb2d;

    /// <summary>
    /// This metod makes start the fall
    /// of the hive
    /// </summary>
    public override void Action()
    {
        if (isActive)
        {
            SoundEffectManager.Instance.PlayClip(hiveClip);
            HiveFalling();
        }
        isActive = false;
        base.coll.enabled = false;
    }

    /// <summary>
    /// This method gives a certain velocity
    /// to the rigidbody of the hive
    /// to make it falls
    /// </summary>
    /// <returns></returns>
    private void HiveFalling()
    {
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        rb2d.bodyType = RigidbodyType2D.Dynamic;
    }

}
