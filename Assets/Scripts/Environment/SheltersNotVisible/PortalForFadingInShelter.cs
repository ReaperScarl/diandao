﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class is used to trigger
/// the fading-in of the shelter of the second level
/// </summary>
public class PortalForFadingInShelter : _PortalScript {

    [Header("How faster the fading should be")]
    public float fadingTime;

    [Header("Where you want to place the cutscene")]
    public GameObject outsideImage;

    private float targetAlpha;

    // Use this for initialization
    void Start()
    {
        targetAlpha = 1F;
    }

    /// <summary>
    /// This method starts the procedure
    /// of fading
    /// </summary>
    protected override void Activate()
    {
        StartCoroutine(FadingOutside());
    }

    /// <summary>
    /// This method fades in the outside image
    /// of the shelter
    /// </summary>
    /// <returns></returns>
    private IEnumerator FadingOutside()
    {
        Color curColor = outsideImage.GetComponent<SpriteRenderer>().material.color;
        float alphaDiff = Mathf.Abs(curColor.a - targetAlpha);
        //Debug.Log(alphaDiff);
        while (alphaDiff <= 1.0F && alphaDiff > -2F)
        {
            alphaDiff -= Time.deltaTime * fadingTime;
            curColor.a = Mathf.Lerp(curColor.a, targetAlpha, fadingTime * Time.deltaTime);
            outsideImage.GetComponent<SpriteRenderer>().material.color = curColor;
            //Debug.Log(alphaDiff);
            yield return null;
        }
        targetAlpha = 1.0F;
    }

    protected override void OnTriggerExit2D(Collider2D collision)
    {
        base.OnTriggerExit2D(collision);
    }
}
