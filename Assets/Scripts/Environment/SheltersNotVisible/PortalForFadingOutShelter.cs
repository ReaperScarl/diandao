﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class is used to trigger
/// the fading-out of the shelter of the second level
/// </summary>
public class PortalForFadingOutShelter : _PortalScript {

    [Header("How faster the fading should be")]
    public float fadingTime;

    [Header("The gameobject representing the outside of the shelter")]
    public GameObject outsideImage;

    private float targetAlpha;

    // Use this for initialization
    void Start () {
        targetAlpha = 0.0f;
	}

    /// <summary>
    /// This method starts the procedure
    /// of fading
    /// </summary>
    protected override void Activate()
    {
        StartCoroutine(FadingOutside());
    }

    /// <summary>
    /// This method fades out the outside image
    /// of the shelter
    /// </summary>
    /// <returns></returns>
    private IEnumerator FadingOutside()
    {
        Color curColor = outsideImage.GetComponent<SpriteRenderer>().material.color;
        float alphaDiff = Mathf.Abs(curColor.a - targetAlpha);
        //Debug.Log(alphaDiff);
        while (alphaDiff > 0.0001f)
        {
            curColor.a = Mathf.Lerp(curColor.a, targetAlpha, fadingTime * Time.deltaTime);
            outsideImage.GetComponent<SpriteRenderer>().material.color = curColor;
            alphaDiff = Mathf.Abs(curColor.a - targetAlpha);
            //Debug.Log(alphaDiff);
            yield return null;
        }
        targetAlpha = 0.0f;
    }

    protected override void OnTriggerExit2D(Collider2D collision)
    {
        base.OnTriggerExit2D(collision);
    }
}
