﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The script is responsible for little tree mechanic.
/// This component should be attached to the little tree object.
/// </summary>
public class LittleTreeControl : MonoBehaviour {

	// ------	Public Variables	------
	[Header("Pulling speed up the breaking point")]
	public float speedUp = 10f;
	[Header("Pulling speed down the breaking point")]
	public float speedDown = 30f;
	[Header("Recovering speed up the breaking point")]
	public float speedBack = 5f;

	[Header("Angle of breaking point")]
	public float angle = 45f;

	[Header("The baffle. (Don't modified this)")]
	public GameObject baffle;

	// ------	Required Components	------
	private Rigidbody2D rb2d;

	// ------	Private Variables	------
	private float maxSpeed;
	private bool isPulling;
	private float isFacedRightFloat;
	private float curAngle;

	// Use this for initialization
	void Start () 
	{
		rb2d = baffle.GetComponent<Rigidbody2D> ();

		maxSpeed = speedUp;
		isPulling = false;
		isFacedRightFloat = transform.localScale.y > 0 ? 1f : -1f;
	}
	
	// Update is called once per frame
	void Update () 
	{
		CalCurrentAngle ();
		CheckMaxSpeed ();

		if (isPulling)
			rb2d.angularVelocity = -isFacedRightFloat * maxSpeed;
		
		if(!isPulling)
		{
			if (curAngle < angle)
				rb2d.angularVelocity = isFacedRightFloat * speedBack;
		}

		Debug.Log (baffle.transform.localRotation.eulerAngles.z);
	}

	/// <summary>
	/// Check the current angle and modify the speed.
	/// </summary>
	private void CheckMaxSpeed(){
		if (curAngle > angle)
			maxSpeed = speedDown;
		else
			maxSpeed = speedUp;
	}

	/// <summary>
	/// Calculate current angle.
	/// </summary>
	private void CalCurrentAngle(){
		curAngle = baffle.transform.localRotation.eulerAngles.z > 0 ?
			Mathf.Abs (baffle.transform.localRotation.eulerAngles.z) :
			Mathf.Abs (baffle.transform.localRotation.eulerAngles.z + 360f);
	}

	/// <summary>
	/// Change the pulling state. Used by RopePlayer.
	/// </summary>
	public void ChangeIsPulling(bool _isPulling){
		isPulling = _isPulling;
	}
}
