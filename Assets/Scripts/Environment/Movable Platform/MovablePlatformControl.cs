﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Movable platform control.
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class MovablePlatformControl : MonoBehaviour {

	// ------	Public Variables	------
	[Header("Normal pull speed")]
	public float speedDown = 50f;
	[Header("Maximun angle of the platform")]
	public float maxAngle = 90f;
	public float startingAngle = 90f;
	public bool isCounterClockWise = true;
	public AudioClip fallClip;

	[Space(5)]
	[Header("Pulling direction")]
	public bool leftAllow = false;
	public bool rightAllow = false;

	[Space(5)]
	[Header("The following fileds only for little tree")]
	[Header("Angle of breaking point if has")]
	public float angle = 0f;
	[Header("Pulling speed up the breaking point")]
	public float speedUp = 0f;
	[Header("Recovering speed up the breaking point")]
	public float speedBack = 0f;
	[Header("Crack clip if have")]
	public AudioClip crackClip;

	// ------	Required Components	------
	private Rigidbody2D rb2d;

	// ------	Private Variables	------
	private float aVel = 0f;
	private float curAngle;
	private State state;
	private Transform player;
	private float rangePlayerOn;
	private float CCWFloat;
	private bool clipIsPlaying;

	// ------	Public Enumerator	------
	public enum State : int{
		Idle,
		Captured,
		PullRight,
		PullLeft,
	}

	// Use this for initialization
	void Start () 
	{
		rb2d = GetComponent<Rigidbody2D> ();

		player = GameObject.FindGameObjectWithTag ("player").transform;
		rangePlayerOn = GetComponent<BoxCollider2D> ().bounds.size.y / 2;

		CCWFloat = isCounterClockWise ? 1f : -1f;
		clipIsPlaying = false;
	}

	// Update is called once per frame
	void Update () 
	{
		CalCurrentAngle ();
		CheckMaxSpeed ();
		CheckPlayerPosition ();

		rb2d.angularVelocity = aVel * CCWFloat;

		if (Mathf.Abs (rb2d.angularVelocity) > 1f && curAngle < maxAngle - 2f) {
			StartCoroutine (PlayClip (fallClip));
		}
	}

	/// <summary>
	/// Check the current angle and modify the velocity.
	/// </summary>
	private void CheckMaxSpeed(){
		switch (state) {
		case State.Idle:
			if (curAngle < angle && curAngle > 2f)
				aVel = Mathf.Sign (transform.localScale.y) * speedBack;
			else
				aVel = 0f;
			break;
		case State.Captured:
			aVel = 0f;
			break;
		case State.PullRight:
			if (!rightAllow) {
				aVel = 0f;
				break;
			}
			if (curAngle < angle)
				aVel = -speedUp;
			else if (curAngle >= angle && curAngle < maxAngle)
				aVel = -speedDown;
			else {
				aVel = 0f;
				rb2d.MoveRotation (startingAngle - (maxAngle - 1f) * CCWFloat);
			}
			break;
		case State.PullLeft:
			if (!leftAllow) {
				aVel = 0f;
				break;
			}
			if (curAngle < angle)
				aVel = speedUp;
			else if (curAngle >= angle && curAngle < maxAngle)
				aVel = speedDown;
			else {
				aVel = 0f;
				rb2d.MoveRotation (startingAngle + (maxAngle - 1f) * CCWFloat);
			}
			break;
		}
			
	}

	/// <summary>
	/// Calculate current angle and play clip.
	/// </summary>
	private void CalCurrentAngle(){
		float lastAngle = curAngle;
		if(transform.eulerAngles.z <= 180f)
			curAngle = Mathf.Abs(transform.eulerAngles.z - startingAngle);
		else
			curAngle = Mathf.Abs(transform.eulerAngles.z - 360f - startingAngle);

		if (curAngle > angle && lastAngle < angle && crackClip) {
			SoundEffectManager.Instance.PlayClip (crackClip);
		}
	}

	/// <summary>
	/// Check if the player is too closed to the platform.
	/// </summary>
	private void CheckPlayerPosition(){
		Collider2D[] colliders = Physics2D.OverlapCircleAll (transform.position, rangePlayerOn);
		foreach (Collider2D collider in colliders) {
			if (collider.tag == "player") {
				aVel = 0f;
				break;
			}
		}
	}

	/// <summary>
	/// Play fall clip.
	/// </summary>
	private IEnumerator PlayClip(AudioClip clip){
		if (clipIsPlaying)
			yield break;
		else {
			SoundEffectManager.Instance.PlayClip (clip);
			clipIsPlaying = true;
		}

		yield return new WaitForSeconds (clip.length);

		clipIsPlaying = false;
	}

	/// <summary>
	/// Changes the state.
	/// </summary>
	public void ChangeState(State _state){
		if (state != _state) {
			state = _state;

			rb2d.angularVelocity = 0f;
			rb2d.velocity = new Vector2 (0, 0);
		}
	}
		
}
