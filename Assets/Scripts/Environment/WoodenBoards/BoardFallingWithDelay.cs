﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manages the fall of
/// a wooden board
/// </summary>
public class BoardFallingWithDelay : MonoBehaviour {

    [Header("How faster the wooden board will fall")]
    [Range(1F, 20F)]
    public float speedOfFalling;

    [Header("How much is the delay before falling")]
    [Range(0F, 10F)]
    public float delay;

    //rigidbody 
    private Rigidbody2D rb2d;

    [Header("y-point of the final position")]
    [Range(-30F, 0F)]
    public float endOfTheFall;

    public bool isActive = true;

    [Header("Sound effect clip when the board breaks")]
    public AudioClip boardBreakSoundEffect;


    /// <summary>
    /// This method is called by an external one
    /// in a portal. It makes start the fall of 
    /// the board which holds this script
    /// </summary>
    public void StartingFall()
    {
        if (boardBreakSoundEffect)
            SoundEffectManager.Instance.PlayClip(boardBreakSoundEffect);
        StartCoroutine(Falling());
    }

    /// <summary>
    /// This method has the duty
    /// to control the fall of the board
    /// If the delay is 0, the fall is immediate
    /// Otherwise, is delayed of the amount
    /// specified
    /// </summary>
    /// <returns></returns>
    private IEnumerator Falling()
    {
        isActive = true;
        yield return new WaitForSeconds(delay);
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        while ( (gameObject.transform.position.y > endOfTheFall) && isActive)
        {
            rb2d.velocity = new Vector2(0, -speedOfFalling);
            yield return null;
        }

        if (isActive)
        {
            gameObject.SetActive(false);
        }
    }
}
