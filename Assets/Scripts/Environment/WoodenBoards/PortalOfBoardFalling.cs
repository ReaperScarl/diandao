﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class inherites the _PortalScript
/// script, in order to activate a script only
/// if a certain condition is triggered by
/// the player
/// </summary>
public class PortalOfBoardFalling : _PortalScript {

    //bool variable to control the interaction
    public bool isActive = true;

    //the array of all the wooden board to control
    public GameObject[] woodenBoard;

    /// <summary>
    /// This method makes start each wooden board 
    /// to fall. When the for clause is complete,
    /// the behaviour of the script is disabled
    /// </summary>
    protected override void Activate()
    {
        if (isActive)
        {
            isActive = false;
            for (int i = 0; i < woodenBoard.Length; i++)
            {
                woodenBoard[i].GetComponent<BoardFallingWithDelay>().StartingFall();
            }
        }
    }

}
